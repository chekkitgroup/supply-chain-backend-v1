import { Inject, Injectable } from '@nestjs/common';
import { SeedersService } from 'database/seeders/seeders.service';
import { successResponseSpec, successResponse } from 'utils/response';

@Injectable()
export class AppService {
  @Inject(SeedersService)
  private readonly seederService: SeedersService;
  welcomeHome(): successResponseSpec {
    return successResponse('Welcome to Chekkit Supply Chain System API');
  }
}
