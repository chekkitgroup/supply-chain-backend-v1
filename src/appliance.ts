import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerDocumentOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import {
  INestApplication,
  UnprocessableEntityException,
  ValidationError,
  ValidationPipe,
} from '@nestjs/common';

import { AllExceptionsFilter } from 'exception/http-exception.filter';

import * as compression from 'compression';

import * as morgan from 'morgan';
import { AppModule } from 'app.module';
import { useContainer } from 'class-validator';

import { PaginationMeta } from 'utils/types/pagination';

import { successResponseSpec } from 'utils/response';
import { WarehouseOutputDto } from 'api/warehouse/dtos/response.dto';
import {
  CompanyOutputDto,
  SubscriptionLogsOutputDto,
} from 'api/admin/dtos/response.dto';
import { AuthModule } from 'api/auth/auth.module';
import { AdminModule } from 'api/admin/admin.module';
import { ProductModule } from 'api/product/product.module';
import { WareHouseModule } from 'api/warehouse/warehouse.module';
import { AssetModule } from 'api/asset/asset.module';
import { HardwareModule } from 'api/hardware/hardware.module';
import { RfidModule } from 'api/rfid/rfid.module';
import { PlansModule } from 'api/plans/plans.module';
import { SubscriptionModule } from 'api/subscription/subscription.module';
import { UsersModule } from 'api/users/users.module';
import { SiteModule } from 'api/site/site.module';
import { UtilsModule } from 'utils/utils.module';
import { SurveyModule } from 'api/survey/survey.module';
import { FinanceModule } from 'api/finance/finance.module';
import {
  HardwareOrderOutputDto,
  HardwareOrdersDetailsOutputDto,
  HardwareOutputDto,
} from 'api/hardware/dtos/response.dto';
import { AccountLoginOutputDto } from 'api/auth/dto/response.dto';
import { SiteOutputDto } from 'api/site/dto/response.dto';
import { ProductOutputDto } from 'api/product/dto/response.dto';
import {
  AssetOutputDto,
  AssetTypeOutputDto,
  OnboardingRequestOutputoDto,
  PinOutputDto,
} from 'api/asset/dto/response.dto';
import { json, urlencoded } from 'express';
import { SurveyOutputDto } from 'api/survey/dto/response.dto';
import { AnalyticsModule } from 'api/analytics/analytics.module';
import {
  NumberOfDaysOutputDto,
  StockCountOutputDto,
  SurveyPerformanceOutputDto,
  WarehouseWithTopAssetsOutputDto,
} from 'api/analytics/dto/response.dto';

import * as expressBasicAuth from 'express-basic-auth';

import { configuration } from './config/index';
import { router } from 'bull-board';
import { UtilsService } from 'utils/utils.service';
import { NotificationModule } from 'api/notification/notification.module';
import { AppmoduleModule } from 'api/appmodule/appmodule.module';

const setupApplicance = (app: INestApplication) => {
  app.use(compression());

  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));

  app.enableCors();

  app.setGlobalPrefix('api/v1', { exclude: ['/'] });

  app.use(morgan('dev'));

  app.useGlobalFilters(new AllExceptionsFilter());

  app.use(
    ['/api/supply-chain', '/api/asset-management'],
    expressBasicAuth({
      users: {
        [configuration().queue.user]: configuration().queue.password,
      },
      challenge: true,
    }),
  );

  app.use(
    '/admin/queues',
    expressBasicAuth({
      users: {
        [configuration().queue.user]: configuration().queue.password,
      },
      challenge: true,
    }),
    router,
  );

  const utilsService = app.get(UtilsService);

  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors: ValidationError[] | ValidationError[][]) => {
        const validationErros = utilsService.transformErrors(errors);

        throw new UnprocessableEntityException(validationErros.flat());
      },
    }),
  );

  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  const swaggerOptionsII: SwaggerCustomOptions = {
    swaggerOptions: {
      docExpansion: 'none',
      persistAuthorization: true,
    },
  };

  const swaggerOptionsI: SwaggerCustomOptions = {
    swaggerOptions: {
      docExpansion: 'none',
      persistAuthorization: true,
    },
  };

  const options: SwaggerDocumentOptions = {
    extraModels: [
      successResponseSpec,
      PaginationMeta,
      CompanyOutputDto,
      SubscriptionLogsOutputDto,
      AccountLoginOutputDto,
      ProductOutputDto,
    ],
    include: [
      AuthModule,
      AdminModule,
      ProductModule,
      UsersModule,
      ProductModule,
      NotificationModule,
      AppmoduleModule,
    ],
    operationIdFactory: (_controllerKey: string, methodKey: string) =>
      methodKey,
  };

  const optionsII: SwaggerDocumentOptions = {
    extraModels: [
      successResponseSpec,
      PaginationMeta,
      CompanyOutputDto,
      SubscriptionLogsOutputDto,
      WarehouseOutputDto,
      HardwareOutputDto,
      SiteOutputDto,
      AssetOutputDto,
      SurveyOutputDto,
      PinOutputDto,
      HardwareOrderOutputDto,
      HardwareOrdersDetailsOutputDto,
      WarehouseWithTopAssetsOutputDto,
      NumberOfDaysOutputDto,
      StockCountOutputDto,
      AssetTypeOutputDto,
      SurveyPerformanceOutputDto,
      OnboardingRequestOutputoDto,
    ],
    include: [
      WareHouseModule,
      AssetModule,
      HardwareModule,
      RfidModule,
      PlansModule,
      SubscriptionModule,
      UtilsModule,
      SiteModule,
      SurveyModule,
      AnalyticsModule,
      FinanceModule,
    ],
    operationIdFactory: (_controllerKey: string, methodKey: string) =>
      methodKey,
  };

  const supplyChainDocsConfig = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('Chekkit Supply Chain API Service')
    .setDescription(
      'Chekkit Supply Chain Management is an api service for chekkit supply chain product',
    )
    .setVersion('1.0')
    .addTag('Chekkit Supply Chain Management')
    .addBearerAuth()
    .build();

  const assetManagementDocsConfig = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('Chekkit Asset Management API Service')
    .setDescription('Chekkit Asset Managementt')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const supplyChainDocs = SwaggerModule.createDocument(
    app,
    supplyChainDocsConfig,
    options,
  );

  const assetManagementDocs = SwaggerModule.createDocument(
    app,
    assetManagementDocsConfig,
    optionsII,
  );

  SwaggerModule.setup(
    'api/supply-chain',
    app,
    supplyChainDocs,
    swaggerOptionsI,
  );

  SwaggerModule.setup(
    'api/asset-management',
    app,
    assetManagementDocs,
    swaggerOptionsII,
  );
};

export default setupApplicance;
