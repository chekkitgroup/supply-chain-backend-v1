import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ThrottlerModule } from '@nestjs/throttler';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { BullModule } from '@nestjs/bull';
import { ScheduleModule } from '@nestjs/schedule';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from 'database/database.module';
import { SeedersModule } from 'database/seeders/seeders.module';

import { AuthModule } from 'api/auth/auth.module';
import { UsersModule } from 'api/users/users.module';
import { WareHouseModule } from 'api/warehouse/warehouse.module';
import { AssetModule } from 'api/asset/asset.module';
import { HardwareModule } from 'api/hardware/hardware.module';

import { RfidModule } from 'api/rfid/rfid.module';
import { SubscriptionModule } from 'api/subscription/subscription.module';
import { AdminModule } from 'api/admin/admin.module';
import { PlansModule } from 'api/plans/plans.module';
import { ValidationModule } from 'api/users/validation/validation.module';
import { ProductModule } from './api/product/product.module';
import { UtilsModule } from './utils/utils.module';
import { SiteModule } from './api/site/site.module';
import { SurveyModule } from './api/survey/survey.module';
import { AnalyticsModule } from './api/analytics/analytics.module';
import { FinanceController } from './api/finance/finance.controller';
import { FinanceModule } from './api/finance/finance.module';
import { NotificationModule } from './api/notification/notification.module';
import { OneSignalModule } from 'onesignal-api-client-nest';
import { AppmoduleModule } from 'api/appmodule/appmodule.module';

@Module({
  imports: [
    BullModule.forRootAsync({
      useFactory: async (configService: ConfigService) => ({
        prefix: 'SC',
        redis: {
          host: configService.get('REDIS_HOST'),
          port: +configService.get('REDIS_PORT'),
          keyPrefix: 'Supply-Chain',
        },
      }),
      inject: [ConfigService],
    }),

    ConfigModule.forRoot({
      isGlobal: true,
    }),

    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 15,
    }),

    ScheduleModule.forRoot(),

    EventEmitterModule.forRoot(),
    OneSignalModule.forRoot({
      appId: '99a8a8c7-fdaa-4a92-b16a-aaf8dde7cbe3',
      restApiKey: 'NDk0ZmYzMTctOGFhOC00YTZkLWE1YTktZDBiMzljM2VlNGYy',
    }),
    AuthModule,
    DatabaseModule,
    ValidationModule,
    SeedersModule,
    WareHouseModule,
    AssetModule,
    HardwareModule,
    RfidModule,
    PlansModule,
    SubscriptionModule,
    AdminModule,
    ProductModule,
    UsersModule,
    UtilsModule,
    SiteModule,
    SurveyModule,
    AnalyticsModule,
    FinanceModule,
    NotificationModule,
    AppmoduleModule,
  ],

  controllers: [AppController, FinanceController],

  providers: [AppService],
})
export class AppModule {}
