import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import setupApplicance from 'appliance';

import logger from 'utils/logger';
import { SeedersService } from 'database/seeders/seeders.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  setupApplicance(app);

  const port = process.env.PORT || 3000;

  await app.listen(port, async () => {
    logger.info(`App running on port ${port}`);
  });
}

bootstrap();
