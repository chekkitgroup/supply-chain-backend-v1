import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';

@Module({
  imports: [
    SequelizeModule.forRootAsync({
      imports: [ConfigModule],

      useFactory: (configService: ConfigService) => ({
        dialect: 'mysql',
        host: configService.get('DATABASE_HOST'),
        port: +configService.get('DATABASE_PORT'),
        username: configService.get('DATABASE_USER'),
        password: configService.get('DATABASE_PASSWORD'),
        database: configService.get('DATABASE_NAME'),

        logging: configService.get('NODE_ENV') !== 'production',

        dialectOptions: {
          supportBigNumbers: true,
          bigNumberStrings: true,
        },

        define: {
          charset: 'utf8mb4',
          collate: 'utf8mb4_unicode_ci',
          createdAt: 'created_at',
          updatedAt: 'updated_at',
          deletedAt: 'deleted_at',
          paranoid: true,
          underscored: true,
        },

        pool: {
          max: 5,
          min: 2,
          acquire: 30000,
          idle: 20000,
        },

        autoLoadModels: true,

        synchronize: Boolean(+configService.get('SYNC_DB')),

        sync: { alter: true },
      }),

      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}
