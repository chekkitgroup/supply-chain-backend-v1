import {
  ConflictException,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import Feature from 'api/plans/models/feature.model';

import { featureCategory, features, plans } from 'api/plans/seed/plans';
import Hardware from 'api/hardware/models/hardware.model';
import { hardWares } from 'api/hardware/seed/hardware';
import Role from 'api/users/models/role.model';
import { permissions, roles } from 'api/users/seed/role';
import logger from 'utils/logger';
import FeatureCategory from 'api/plans/models/featureCategory.model';
import Plan from 'api/plans/models/plan.model';
import Permission from 'api/users/models/permission.model';
import { products } from 'api/product/seed';
import Product from 'api/product/models/product.model';
import { Op } from 'sequelize';
import FeaturePlan from 'api/plans/models/featurePlan';
import AppModule from 'api/appmodule/models/appmodule.model';
import { appModules } from 'api/appmodule/seed';

@Injectable()
export class SeedersService {
  private readonly _plans = plans;

  private readonly _featureCategoryData = featureCategory;
  private readonly _appModuleData = appModules;
  private readonly _featureData = features;

  private _featureCategories: FeatureCategory[] = [];
  private _appModules: AppModule[] = [];

  private readonly _hardwares = hardWares;

  private readonly _roles = roles;

  private readonly _products = products;
  private readonly _permissions = permissions;

  constructor(
    @InjectModel(Plan) private readonly planRepository: typeof Plan,

    @InjectModel(FeatureCategory)
    private readonly featureCategoryRepository: typeof FeatureCategory,

    @InjectModel(AppModule)
    private readonly moduleRepository: typeof AppModule,

    @InjectModel(Feature)
    private readonly featureRepository: typeof Feature,

    @InjectModel(Hardware) private readonly hardwareRepository: typeof Hardware,

    @InjectModel(Role) private readonly roleRepository: typeof Role,

    @InjectModel(Permission)
    private readonly permissionRepository: typeof Permission,

    @InjectModel(Product)
    private readonly productRepository: typeof Product,

    @InjectModel(FeaturePlan)
    private readonly featurePlanRepository: typeof FeaturePlan,
  ) {}

  async insertFeatureCategories() {
    logger.info('start syncing feature categories');

    const catgorySeeds = this._featureCategoryData.map(
      async (featureCategory) => {
        const category = await this.featureCategoryRepository.findOne({
          where: {
            name: featureCategory.name,
          },
        });

        if (!category)
          return this.featureCategoryRepository.create(featureCategory);
      },
    );

    const seedData = await Promise.all(catgorySeeds);

    logger.info(
      `result from feature categotries syncing ${JSON.stringify(seedData)}`,
    );

    this._featureCategories = seedData;
  }

  async insertAppModules() {
    logger.info('start syncing app modules');

    const appMosuleSeeds = this._appModuleData.map(async (module) => {
      const moduleInDb = await this.moduleRepository.findOne({
        where: {
          name: module.name,
        },
      });

      if (!moduleInDb) return this.moduleRepository.create(module);
    });

    const seedData = await Promise.all(appMosuleSeeds);

    logger.info(`result from app module syncing ${JSON.stringify(seedData)}`);

    this._appModules = seedData;
  }

  async insertFeatures() {
    logger.info('start syncing features');

    const featureSeeds = this._featureData.map(async (feature) => {
      const existedFeature = await this.featureRepository.findOne({
        where: {
          name: feature.name,
        },
      });

      if (!existedFeature) {
        const featureDetail: Partial<Feature> = {
          name: feature.name,
          description: feature.description,
          code: feature.code,
        };

        const seededFeature = (await this.featureRepository.create(
          featureDetail,
        )) as any;

        const featureCategory = this._featureCategories.find(
          (featureCategory) => feature.name.includes(featureCategory?.name),
        );

        await seededFeature.setFeatureCategory(featureCategory);
      }
    });

    const resolvedFeatures = await Promise.all(featureSeeds);

    logger.info(
      `result from features syncing ${JSON.stringify(resolvedFeatures)}`,
    );
  }

  async insertPlans() {
    logger.info('start syncing plans');

    const planSeeds = this._plans.map(async (plan) => {
      const existedPlan = await this.planRepository.findOne({
        where: {
          type: plan.type,
        },
      });

      if (!existedPlan) {
        const planDetail: Partial<Plan> = {
          type: plan.type as any,
          price: plan.price,
          description: plan.description,
          numberOfYears: plan.numberOfYears,
        };

        const seededPlan = (await this.planRepository.create(
          planDetail,
        )) as any;

        const planFeatures = JSON.parse(plan.featuresList);

        planFeatures.forEach(async (planFeature) => {
          const featureDetails = features.find(
            ({ name }) => name === planFeature,
          );

          const feature = await this.featureRepository.findOne({
            where: { name: featureDetails.name, code: featureDetails.code },
          });

          await this.featurePlanRepository.create({
            planId: seededPlan.id,
            featureId: feature.id,
          });
        });
      }
    });

    await Promise.all(planSeeds);

    logger.info(`successfully sync plans`);
  }

  async insertHardwares() {
    logger.info('start seeding hardwares');

    const seededHardwares = await this.hardwareRepository.findAll();

    if (seededHardwares.length > 0) {
      throw new ConflictException('hardwares already seeded');
    }

    const hardwares = this._hardwares.map((hardware) => {
      return this.hardwareRepository.create(hardware);
    });

    const seedData = await Promise.all(hardwares);

    logger.info(`result from hardware seeding ${JSON.stringify(seedData)}`);
  }

  async insertRoles() {
    const permissions = await this.permissionRepository.findAll();

    if (!permissions.length)
      throw new UnprocessableEntityException('KIndly sync permissions');

    const seededRoles = await this.roleRepository.findAll();

    if (seededRoles.length > 0)
      throw new ConflictException('roles already seeded');

    logger.info('start seeding roles');

    await Promise.all(
      this._roles.map(async (role) => {
        const seededRole = (await this.roleRepository.create(role)) as any;

        if (role.permissions?.length) {
          const permissions = await this.permissionRepository.findAll({
            where: {
              identifier: {
                [Op.in]: role.permissions,
              },
            },
          });

          await seededRole.setPermissions(permissions);
        }
      }),
    );

    logger.info('roles seeded successfully');
  }

  async insertPermissions() {
    logger.info('start syncing permissions');

    const permissions = this._permissions.reduce((result, currentValue) => {
      const permissionDetails = [];

      currentValue.identifiers.forEach((identifier) => {
        const props = {
          group: currentValue.group,
          identifier,
        };

        permissionDetails.push(props);
      });

      return [...result, ...permissionDetails];
    }, []);

    const permissionSeeds = permissions.map(async (permission) => {
      const permissionIdentifier = await this.permissionRepository.findOne({
        where: {
          identifier: permission.identifier,
        },
      });

      if (!permissionIdentifier) {
        const permissionDetail: Partial<Permission> = {
          group: permission.group,
          identifier: permission.identifier,
        };

        return this.permissionRepository.create(permissionDetail);
      }
    });

    const seedData = await Promise.all(permissionSeeds);

    logger.info(`result from permissions syncing ${JSON.stringify(seedData)}`);
  }

  async insertProducts() {
    logger.info('start seeding products');

    const seededProducts = await this.productRepository.findAll();

    if (seededProducts.length > 0) {
      throw new ConflictException('products already seeded');
    }

    const seedData = await this.productRepository.bulkCreate(this._products);

    logger.info(`result from products seeding ${JSON.stringify(seedData)}`);
  }
}
