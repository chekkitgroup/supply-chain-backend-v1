import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import Feature from 'api/plans/models/feature.model';

import Hardware from 'api/hardware/models/hardware.model';
import Permission from 'api/users/models/permission.model';
import Role from 'api/users/models/role.model';
import RolePermission from 'api/users/models/rolePermission.model';

import { SeedersService } from './seeders.service';
import FeatureCategory from 'api/plans/models/featureCategory.model';
import Plan from 'api/plans/models/plan.model';
import Product from 'api/product/models/product.model';
import FeaturePlan from 'api/plans/models/featurePlan';
import AppModule from 'api/appmodule/models/appmodule.model';

@Module({
  imports: [
    SequelizeModule.forFeature([
      Plan,
      FeatureCategory,
      Feature,
      FeaturePlan,
      Hardware,
      Role,
      Permission,
      RolePermission,
      Product,
      AppModule,
    ]),
  ],
  providers: [SeedersService],
  exports: [SeedersService],
})
export class SeedersModule {}
