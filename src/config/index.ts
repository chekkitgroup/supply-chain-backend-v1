interface Configuration {
  admin: {
    name: string;
    email: string;
    password: string;
  };
  port: number;

  jwt: {
    secret: string;
    expiry: string;
  };

  queue: {
    user: string;
    password: string;
  };

  tagAndTrac: {
    email: string;
    password: string;
    clientId: string;
    apiKey: string;
    origin: string;
    url: string;
    trackerPrefix: string;
    interval: number;
  };

  redis: {
    host: string;
    port: number;
  };

  IsTest: () => boolean;

  IsDev: () => boolean;

  IsProd: () => boolean;
}

export const configuration = (): Configuration => ({
  admin: {
    name: process.env.CHEKKIT_ADMIN_NAME,
    email: process.env.CHEKKIT_ADMIN_EMAIL,
    password: process.env.CHEKKIT_ADMIN_PASSWORD,
  },

  jwt: {
    secret: process.env.JWT_SECRET,
    expiry: process.env.JWT_EXPIRY,
  },

  port: +process.env.PORT,

  queue: {
    user: process.env.QUEUE_USER,
    password: process.env.QUEUE_PASSWORD,
  },

  tagAndTrac: {
    apiKey: process.env.TAGANDTRAC_API_KEY,
    email: process.env.TAGANDTARC_EMAIL_ID,
    password: process.env.TAGANDTRAC_PASSWORD,
    clientId: process.env.TAGANDTRAC_CLIENT_ID,
    origin: process.env.TAGANDTARC_ORIGIN,
    url: process.env.TAGANDTRAC_URL,
    trackerPrefix: process.env.TRACKER_PREFIX,
    interval: +process.env.TRACKER_INTERVAL,
  },

  redis: {
    host: process.env.REDIS_HOST,
    port: +process.env.REDIS_PORT,
  },

  IsTest: () => process.env.NODE_ENV === 'test',

  IsDev: () => process.env.NODE_ENV === 'development',

  IsProd: () => process.env.NODE_ENV === 'production',
});
