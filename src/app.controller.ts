import { Controller, Get } from '@nestjs/common';

import { ApiTags } from '@nestjs/swagger';
import { successResponseSpec } from 'utils/response';
import { AppService } from './app.service';

@ApiTags('Home')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  home(): successResponseSpec {
    return this.appService.welcomeHome();
  }
}
