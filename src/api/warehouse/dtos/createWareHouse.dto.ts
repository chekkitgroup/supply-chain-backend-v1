import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

export class warehouseDto {
  @ApiProperty({ example: 'Angels and CO' })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ example: '10 Oladipupo' })
  @IsString()
  @IsNotEmpty()
  address: string;

  @ApiProperty({ example: 'Lagos' })
  @IsString()
  @IsOptional()
  state: string;

  @ApiProperty({ example: 'Nigeria' })
  @IsString()
  @IsNotEmpty()
  country: string;

  @ApiProperty({ example: 6.453 })
  @IsNumber()
  @IsNotEmpty()
  latitude: number;

  @ApiProperty({ example: 8.453 })
  @IsNumber()
  @IsNotEmpty()
  longitude: number;

  @ApiProperty({ example: '10 Oladipupo' })
  @IsString()
  @IsOptional()
  toAddress: string;

  @ApiProperty({ example: 'Lagos' })
  @IsString()
  @IsOptional()
  toState: string;

  @ApiProperty({ example: 'Nigeria' })
  @IsString()
  @IsOptional()
  toCountry: string;

  @ApiProperty({ example: 6.453 })
  @IsNumber()
  @IsOptional()
  toLatitude: number;

  @ApiProperty({ example: 8.453 })
  @IsNumber()
  @IsOptional()
  toLongitude: number;

  @ApiProperty({ example: '100' })
  @IsString()
  capacity: string;
}

export class CreateWarehouseDto {
  @ApiProperty({
    description: 'warehouse creation requests',
    type: [warehouseDto],
  })
  @ValidateNested({ each: true })
  @IsArray()
  @Type(() => warehouseDto)
  warehouses: [warehouseDto];
}
