import { getSchemaPath } from '@nestjs/swagger';
import Warehouse from '../models/warehouse.model';

export class TopAssetsDto {
  id: string;
  name: string;
  state: string;
  country: string;
  assetsCount: string;
  contribution: number;
}

export class WarehouseOutputDto {
  id: string;
  name: string;
  quantity: number;
  isAvailable: boolean;
  status: boolean;
  address: string;
  state: string;
  country: string;
  latitude: number;
  longitude: number;
  capacity: string;
  created_at: Date;
  deleted_at: Date;
  updated_at: Date;

  static FromWarehouseOutput(warehouse: Warehouse) {
    const output = new WarehouseOutputDto();

    const exposedKeys = [
      'id',
      'name',
      'quantity',
      'isAvailable',
      'status',
      'address',
      'state',
      'country',
      'latitude',
      'longitude',
      'capacity',
    ];

    exposedKeys.forEach((exposedKey) => {
      output[exposedKey] = warehouse[exposedKey];
    });

    return output;
  }
}

export const WarehouseOutputDtoSchema = getSchemaPath(WarehouseOutputDto);
