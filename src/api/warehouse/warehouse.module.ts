import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import WareHouse from './models/warehouse.model';
import { WareHouseService } from './warehouse.service';
import { WareHouseController } from './warehouse.controller';

import { UsersModule } from 'api/users/users.module';
import { AssetModule } from 'api/asset/asset.module';
import Project from './models/project.model';

@Module({
  imports: [
    SequelizeModule.forFeature([Project, WareHouse]),
    forwardRef(() => UsersModule),
    forwardRef(() => AssetModule),
  ],
  providers: [WareHouseService],
  exports: [WareHouseService],
  controllers: [WareHouseController],
})
export class WareHouseModule {}
