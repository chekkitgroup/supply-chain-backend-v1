import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { TagAndTracService } from 'api/asset/apps/tagAndTrac.service';
import { HTTPREQUESTMETHOD } from 'api/asset/apps/types';
import { AssetService } from 'api/asset/asset.service';
import Asset from 'api/asset/models/asset.model';
import { userAuthDetails } from 'api/auth/types';
import { CompanyService } from 'api/users/company/company.service';
import sequelize from 'sequelize';

import logger from 'utils/logger';
import { getOffSet, getPaginationMetaData } from 'utils/pagination';
import { PaginationMeta } from 'utils/types/pagination';

import { CreateWarehouseDto } from './dtos/createWareHouse.dto';
import Project from './models/project.model';
import Warehouse from './models/warehouse.model';

@Injectable()
export class WareHouseService {
  constructor(
    @InjectModel(Warehouse)
    private readonly warehouseRepository: typeof Warehouse,

    @InjectModel(Project)
    private readonly projectRepository: typeof Project,

    private readonly companyService: CompanyService,

    @Inject(forwardRef(() => AssetService))
    private readonly assetService: AssetService,

    private readonly tagAndTracService: TagAndTracService,
  ) {}

  async create(wareHouseDetails: CreateWarehouseDto, user: userAuthDetails) {
    logger.info('About retrieving company warehouses');

    const company = await this.companyService.findOne({ id: user.companyId });

    const projectId = await this.getProjectByCompanyId(user.companyId);

    if (!projectId) {
      const projectData = {
        projectName: `${company.name}`,
        shortName: `${company.name}`,
        sourceAddress: {
          location: wareHouseDetails.warehouses[0].address,
          city: wareHouseDetails.warehouses[0].state,
          state: wareHouseDetails.warehouses[0].state,
          country: wareHouseDetails.warehouses[0].country,
          zip: '+234',
        },
        destinationAddress: {
          location:
            wareHouseDetails.warehouses[0].toAddress ||
            wareHouseDetails.warehouses[0].address,
          city:
            wareHouseDetails.warehouses[0].toState ||
            wareHouseDetails.warehouses[0].state,
          state:
            wareHouseDetails.warehouses[0].toState ||
            wareHouseDetails.warehouses[0].state,
          country:
            wareHouseDetails.warehouses[0].toCountry ||
            wareHouseDetails.warehouses[0].country,
          zip: '+234',
        },
        createdBy: '',
        organizationId: '',
      };

      const responseData = await this.tagAndTracService.call(
        projectData,
        HTTPREQUESTMETHOD.POST,
        'project',
      );

      await this.projectRepository.create({
        project_name: company.name,
        provider_project_id: responseData?.id,
        companyId: user.companyId,
      });
    }

    logger.info(
      `About to create warehouse for userDetails: ${JSON.stringify(
        user,
      )} with wareHouseData: ${JSON.stringify(wareHouseDetails)}`,
    );

    const warehousesDetail: Array<Partial<Warehouse>> =
      wareHouseDetails.warehouses.map((warehouseDetail) => ({
        name: warehouseDetail.name,
        address: warehouseDetail.address,
        state: warehouseDetail?.state,
        country: warehouseDetail.country,
        latitude: warehouseDetail.latitude,
        longitude: warehouseDetail.longitude,
        capacity: warehouseDetail.capacity,
      }));

    const wareHouses: any = await this.warehouseRepository.bulkCreate(
      warehousesDetail,
    );

    logger.info(
      `Back from creating Warehouse for userDetails: ${JSON.stringify(
        user,
      )} with wareHouseData: ${JSON.stringify(wareHouses)}`,
    );

    await Promise.all(
      wareHouses.map((wareHouse: any) => {
        return wareHouse.setCompany(user.companyId);
      }),
    );

    logger.info('About updating company warehouses count');

    await this.companyService.update(
      { totalWarehouses: company.totalWarehouses + warehousesDetail.length },
      { id: user.companyId },
    );

    logger.info(
      'About to call tag and trac to create project for company/warehouse',
    );

    return wareHouses;
  }

  async getWarehouses(
    user: userAuthDetails,
    page: number,
    perPage: number,
  ): Promise<[Warehouse[], PaginationMeta]> {
    const offset: number = getOffSet(+page, +perPage);

    logger.info(
      `About to get warehouse for a particular company with companyId: ${user.companyId}`,
    );

    const { count: totalCount, rows: warehouses } =
      await this.warehouseRepository.findAndCountAll({
        where: { companyId: user.companyId },
        ...(perPage && { limit: perPage }),
        ...(offset && { limit: offset }),
      });

    logger.info(
      `Back from getting warehouse for with companyId: ${
        user.companyId
      } with warehouse details ${JSON.stringify(warehouses)}`,
    );

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      page,
      perPage,
      warehouses.length,
    );

    return [warehouses, paginationMetaData];
  }

  async findOne(id: string): Promise<Warehouse> {
    return await this.warehouseRepository.findOne({ where: { id } });
  }

  async topAssets(count: number, companyId: string): Promise<Warehouse[]> {
    logger.info('Start top assets retrieval');

    const results = await this.warehouseRepository.findAll({
      subQuery: false,

      where: {
        companyId,
      },

      attributes: [
        'id',
        'name',
        'state',
        'country',
        [
          sequelize.fn('COUNT', sequelize.col('assets.warehouse_id')),
          'assetsCount',
        ],
      ],

      include: [{ model: Asset, attributes: [] }],

      group: ['Warehouse.id'],

      limit: count,

      order: [[sequelize.literal('assetsCount'), 'DESC']],
    });

    return results;
  }

  async getAllAssetsInWarehouse(
    user: userAuthDetails,
    page: number,
    perPage: number,
    id: string,
  ): Promise<[Asset[], PaginationMeta]> {
    logger.info('About to start retrieving assets from warehouse');

    const offset = getOffSet(page, perPage);

    const { rows: assets, count: totalCount } =
      await this.assetService.retrieveAssetsByWarehouseId(
        id,
        user.companyId,
        offset,
        perPage,
      );

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      page,
      perPage,
      assets.length,
    );

    logger.info('Completes retrieving of assets from warehouse');

    return [assets, paginationMetaData];
  }

  async getProjectByCompanyId(companyId: string) {
    return this.projectRepository.findOne({ where: { companyId } });
  }
}
