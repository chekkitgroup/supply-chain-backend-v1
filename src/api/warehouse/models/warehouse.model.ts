import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  BelongsTo,
  Model,
  DataType,
  HasMany,
} from 'sequelize-typescript';

import { UUIDV4 } from 'sequelize';
import Company from 'api/users/models/company.model';
import Asset from 'api/asset/models/asset.model';

@Table
class Warehouse extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING(50) })
  name: string;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: true,
  })
  status: boolean;

  @Column({
    type: DataType.TEXT,
  })
  address: string;

  @Column({
    type: DataType.STRING(50),
  })
  state: string;

  @Column({
    type: DataType.STRING(50),
  })
  country: string;

  @Column({
    type: DataType.FLOAT,
  })
  latitude: number;

  @Column({
    type: DataType.FLOAT,
  })
  longitude: number;

  @Column({
    type: DataType.STRING(100),
  })
  capacity: string;

  @BelongsTo(() => Company, 'companyId')
  company: Company;

  @HasMany(() => Asset, 'warehouseId')
  assets: Asset[];

  // virtuals
  assetsCount: string;
}
export default Warehouse;
