import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
} from 'sequelize-typescript';

import { UUIDV4 } from 'sequelize';

@Table
class Project extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING })
  project_name: string;

  @Column({ type: DataType.STRING })
  provider_project_id: string;
}

export default Project;
