import { Test, TestingModule } from '@nestjs/testing';
import { WareHouseController } from './warehouse.controller';

describe('WarehouseController', () => {
  let controller: WareHouseController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WareHouseController],
    }).compile();

    controller = module.get<WareHouseController>(WareHouseController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
