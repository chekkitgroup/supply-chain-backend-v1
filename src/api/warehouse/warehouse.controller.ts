import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Request,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  AssetOutputDto,
  AssetOutputDtoSchema,
} from 'api/asset/dto/response.dto';
import { Authorize } from 'api/auth/authority.decorator';

import { RequestWithUser } from 'api/auth/types';
import { PaginationFilter } from 'utils/pagination';
import { successResponse, successResponseSpec } from 'utils/response';
import { ResponseSchemaHelper } from 'utils/types/output-model';
import { CreateWarehouseDto } from './dtos/createWareHouse.dto';
import {
  WarehouseOutputDto,
  WarehouseOutputDtoSchema,
} from './dtos/response.dto';

import { WareHouseService } from './warehouse.service';

@ApiBearerAuth()
@ApiTags('Warehouse')
@Controller('warehouse')
export class WareHouseController {
  constructor(private readonly _wareHouseService: WareHouseService) {}

  @ApiOperation({ summary: 'Add warehouses for a company' })
  @ApiResponse({ status: 201, description: 'Add warehouses for a company' })
  @Authorize('warehouses.manage')
  @Post()
  async AddWareHouse(
    @Body() addWareHouseDetails: CreateWarehouseDto,
    @Request() req: RequestWithUser,
  ): Promise<successResponseSpec> {
    const response = await this._wareHouseService.create(
      addWareHouseDetails,
      req.user,
    );

    return successResponse('WareHouse(s) added successfully', 201, response);
  }

  @ApiOperation({ summary: 'Get warehouses details' })
  @ApiResponse({
    status: 200,
    description: 'Get warehouses details for a company',
    schema: ResponseSchemaHelper(WarehouseOutputDtoSchema, 'array'),
  })
  @Authorize('warehouses.view')
  @Get()
  async getWareHouse(
    @Request() req: RequestWithUser,
    @Query() filter?: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [warehouses, paginationMetaData] =
      await this._wareHouseService.getWarehouses(
        req.user,
        +filter.page,
        +filter.perPage,
      );

    const output = warehouses?.map((warehouse) =>
      WarehouseOutputDto.FromWarehouseOutput(warehouse),
    );

    return successResponse(
      'WareHouses retrieved successfully',
      200,
      output,
      true,
      paginationMetaData,
    );
  }

  @ApiOperation({
    summary: 'Retrieves all assets in a warehouse',
  })
  @ApiResponse({
    status: 200,
    description: 'Retrieves all assets in a warehouse',
    schema: ResponseSchemaHelper(AssetOutputDtoSchema, 'array'),
  })
  @Authorize('assets.view')
  @Get('/:id/assets')
  async getAssetsBySite(
    @Request() req: RequestWithUser,
    @Param('id') id: string,
    @Query() filter?: PaginationFilter,
  ) {
    const [assets, paginationMetaData] =
      await this._wareHouseService.getAllAssetsInWarehouse(
        req.user,
        +filter.page,
        +filter.perPage,
        id,
      );

    const output = assets?.map((asset) =>
      AssetOutputDto.FromAssetOutput(asset),
    );

    return successResponse(
      'Asset(s) retrieved successfully',
      200,
      output,
      true,
      paginationMetaData,
    );
  }
}
