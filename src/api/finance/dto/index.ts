import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString, IsUUID } from 'class-validator';

enum QueryType {
  site = 'site',
  warehouse = 'warehouse',
}

export class GetFinanceDTO {
  @ApiProperty({ enum: QueryType, description: 'type' })
  @IsOptional()
  @IsString()
  @IsEnum(QueryType)
  type: QueryType;

  @ApiProperty({ description: 'Site ID', required: false })
  @IsOptional()
  @IsUUID()
  siteId?: string;

  @ApiProperty({ description: 'warehouse Id', required: false })
  @IsOptional()
  @IsUUID()
  warehouseId?: string;
}
