import { Controller, Get, Query, Req } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AssetOutputDtoSchema } from 'api/asset/dto/response.dto';
import { Authorize } from 'api/auth/authority.decorator';
import { RequestWithUser } from 'api/auth/types';
import { successResponse } from 'utils/response';
import { ResponseSchemaHelper } from 'utils/types/output-model';
import { GetFinanceDTO } from './dto';
import { FinanceService } from './finance.service';

@ApiBearerAuth()
@Controller('finance')
@ApiTags('Finance')
export class FinanceController {
  constructor(private readonly _financeService: FinanceService) {}

  @ApiResponse({
    status: 200,
    description: 'Retrieves finances of assets for a particular company',
    schema: ResponseSchemaHelper(AssetOutputDtoSchema, 'array'),
  })
  @ApiOperation({
    summary: 'Fetch Company Finance',
  })
  @Authorize('assets.manage')
  @Get()
  async getFinance(
    @Query() filter: GetFinanceDTO,
    @Req() req: RequestWithUser,
  ) {
    const result = await this._financeService.getClientFinance(
      filter,
      req.user,
    );
    return successResponse(
      'Finance(s) retrieved successfully',
      200,
      result,
      true,
    );
  }
}
