import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import Asset from 'api/asset/models/asset.model';
import { SiteModule } from 'api/site/site.module';
import { UsersModule } from 'api/users/users.module';
import { WareHouseModule } from 'api/warehouse/warehouse.module';
import { UtilsModule } from 'utils/utils.module';
import { FinanceController } from './finance.controller';
import { FinanceService } from './finance.service';

@Module({
  imports: [
    SequelizeModule.forFeature([Asset]),
    UtilsModule,
    forwardRef(() => UsersModule),
    SiteModule,
    WareHouseModule,
  ],
  controllers: [FinanceController],
  providers: [FinanceService],
  exports: [FinanceService],
})
export class FinanceModule {}
