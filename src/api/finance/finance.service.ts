import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import Asset from 'api/asset/models/asset.model';
import { userAuthDetails } from 'api/auth/types';
import { Sequelize } from 'sequelize-typescript';
import logger from 'utils/logger';
import { GetFinanceDTO } from './dto';

@Injectable()
export class FinanceService {
  constructor(
    @InjectModel(Asset) private readonly assetRepository: typeof Asset,
  ) {}

  async getClientFinance(filter: GetFinanceDTO, user: userAuthDetails) {
    logger.info('start retrieving finance records for client');

    const records = await this.assetRepository.findAll({
      where: {
        ...(filter.siteId && { site_id: filter.siteId }),
        ...(filter.warehouseId && { warehouse_id: filter.warehouseId }),
      },
      group: ['name', 'package_level'],
      raw: true,
      attributes: [
        'name',
        'price',
        'package_level',
        'created_at',
        [Sequelize.fn('COUNT', Sequelize.col('id')), 'unit'],
      ],
    });
    logger.info('done retrieving finance records for client');
    return records;
  }
}
