import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { SubscriptionController } from './subscription.controller';
import Subscription from './models/subscription.model';
import SubscriptionLogs from './models/subscriptionLogs.model';
import { SubscriptionService } from './subscription.service';
import { SubscriptionScheduleService } from './subscription.schedule';
import { UtilsModule } from 'utils/utils.module';

@Module({
  imports: [
    SequelizeModule.forFeature([Subscription, SubscriptionLogs]),
    UtilsModule,
  ],
  controllers: [SubscriptionController],
  providers: [SubscriptionService, SubscriptionScheduleService],
  exports: [SubscriptionService],
})
export class SubscriptionModule {}
