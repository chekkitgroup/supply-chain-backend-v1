import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { approveDto } from 'api/admin/dtos/approve.dto';
import Plan from 'api/plans/models/plan.model';
import { Op } from 'sequelize';

import Subscription from './models/subscription.model';
import SubscriptionLogs from './models/subscriptionLogs.model';
import { SubscriptionStatus } from './types';

@Injectable()
export class SubscriptionService {
  constructor(
    @InjectModel(SubscriptionLogs)
    private readonly subscriptionLogRepository: typeof SubscriptionLogs,
    @InjectModel(Subscription)
    private readonly subscriptionRepository: typeof Subscription,
  ) {}

  async create(subscriptionData) {
    return await this.subscriptionRepository.create(subscriptionData);
  }

  async log(subscriptionData) {
    return await this.subscriptionLogRepository.create(subscriptionData);
  }

  async updatelog(companyId: string, updateProp: any, id: string) {
    await this.subscriptionLogRepository.update(
      { ...updateProp },
      { where: { companyId, id } },
    );
  }

  async findByCompany(companyId: string) {
    const subscription = await this.subscriptionRepository.findOne({
      where: {
        companyId,
      },
    });

    return subscription;
  }

  async update(companyId: string, updateProp: approveDto) {
    const subscription = await this.subscriptionRepository
      .update({ ...updateProp }, { where: { companyId } })
      .then(() =>
        this.subscriptionRepository.findOne({
          where: { companyId },
          include: ['subscriptionLogs'],
        }),
      );

    return subscription;
  }

  async findSubLogByCompany(companyId: string, id: string) {
    const subscriptionLog = await this.subscriptionLogRepository.findOne({
      where: {
        id,
        companyId: companyId,
        status: SubscriptionStatus.Pending,
      },
    });

    return subscriptionLog;
  }

  async findAllLogs(companyId: string, perPage: number, offset: number) {
    return await this.subscriptionLogRepository.findAndCountAll({
      distinct: true,
      where: { companyId, status: SubscriptionStatus.Pending },
      include: ['plan'],
      ...(perPage && { limit: perPage }),
      ...(offset && { limit: offset }),
      order: [['created_at', 'DESC']],
    });
  }

  async getActiveSubDetails(companyId: string) {
    return this.subscriptionRepository.findOne({
      where: {
        companyId,
        status: true,
      },

      attributes: ['status', 'planId'],

      include: [
        {
          model: Plan,

          attributes: ['type', 'description'],
        },
      ],
    });
  }

  async getSubLog(companyId: string) {
    return this.subscriptionLogRepository.findOne({
      where: { companyId, status: SubscriptionStatus.Pending },
    });
  }

  async getSubScriptionsToExpire(): Promise<Subscription[]> {
    const subscriptionsToExpire = this.subscriptionRepository.findAll({
      where: {
        status: true,
        endDate: {
          [Op.lte]: new Date().toISOString(),
        },
      },
    });

    return subscriptionsToExpire;
  }

  async expireSubscription(updateProps: Partial<Subscription>, subId: string) {
    await this.subscriptionRepository.update(updateProps, {
      where: {
        id: subId,
      },
    });
  }

  async expireDueSubscription() {
    const subscriptionsToExpire = await this.getSubScriptionsToExpire();

    if (subscriptionsToExpire.length) {
      await Promise.all(
        subscriptionsToExpire.map((subscription: Subscription) => {
          const updateProps: Partial<Subscription> = {
            status: false,
          };

          this.expireSubscription(updateProps, subscription.id);
        }),
      );
    }
  }
}
