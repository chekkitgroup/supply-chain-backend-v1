import {
  Column,
  ForeignKey,
  IsUUID,
  PrimaryKey,
  Table,
  BelongsTo,
  Model,
  DataType,
  HasOne,
} from 'sequelize-typescript';
import Company from 'api/users/models/company.model';

import { UUIDV4 } from 'sequelize';
import Plan from 'api/plans/models/plan.model';
import Subscription from './subscription.model';
import { SubscriptionChannel, SubscriptionStatus } from '../types';

@Table
class SubscriptionLogs extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({
    type: DataType.FLOAT,
    allowNull: false,
  })
  price: number;

  @Column({
    type: DataType.ENUM('success', 'failed', 'pending'),
  })
  status: SubscriptionStatus;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  isApproved: boolean;

  @Column({
    type: DataType.ENUM('admin', 'others'),
  })
  channel: SubscriptionChannel;

  @ForeignKey(() => Company)
  @Column
  companyId: string;

  @ForeignKey(() => Plan)
  @Column
  planId: string;

  @Column({
    type: DataType.DATE,
    allowNull: false,
  })
  startDate: Date;

  @Column({
    type: DataType.DATE,
    allowNull: false,
  })
  endDate: Date;

  @BelongsTo(() => Plan, 'planId')
  plan: Plan;

  @BelongsTo(() => Company, 'companyId')
  company: Company;

  @HasOne(() => Subscription, 'subscriptionLogsId')
  subscription: Subscription;
}
export default SubscriptionLogs;
