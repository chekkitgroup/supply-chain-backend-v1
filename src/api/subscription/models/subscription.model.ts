import {
  Column,
  ForeignKey,
  IsUUID,
  PrimaryKey,
  Table,
  BelongsTo,
  Model,
  DataType,
} from 'sequelize-typescript';

import Company from 'api/users/models/company.model';

import { UUIDV4 } from 'sequelize';
import Plan from 'api/plans/models/plan.model';
import SubscriptionLogs from './subscriptionLogs.model';

@Table
class Subscription extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.BOOLEAN, allowNull: false })
  status: boolean;

  @Column({
    type: DataType.FLOAT,
    allowNull: false,
  })
  price: number;

  @ForeignKey(() => Company)
  @Column
  companyId: string;

  @ForeignKey(() => Plan)
  @Column
  planId: string;

  @Column({
    type: DataType.DATE,
  })
  startDate: Date;

  @Column({
    type: DataType.DATE,
    allowNull: false,
  })
  endDate: Date;

  @BelongsTo(() => Plan, 'planId')
  plan: Plan;

  @BelongsTo(() => Company, 'companyId')
  company: Company;

  @BelongsTo(() => SubscriptionLogs, 'subscriptionLogsId')
  subscriptionLogs: SubscriptionLogs;
}

export default Subscription;
