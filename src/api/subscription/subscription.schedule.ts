import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import logger from 'utils/logger';
import { RedisService } from 'utils/redis';
import { SubscriptionService } from './subscription.service';

@Injectable()
export class SubscriptionScheduleService {
  constructor(
    private readonly subscriptionService: SubscriptionService,
    private readonly redisService: RedisService,
  ) {}

  // every 11:40pm
  @Cron('0 40 23 * * *')
  async expireSubscription() {
    const lock = await this.redisService.acquireResource(
      'expire_due_subscription',
    );

    try {
      logger.info('start expiring due subscriptions');

      await this.subscriptionService.expireDueSubscription();

      logger.info('done expiring due subscriptions');
    } catch (error) {
      logger.error(
        `An error occurred while expiring due subscription ${error.message}`,
      );
    } finally {
      try {
        if (lock) {
          await lock.unlock();

          logger.info('lock released for expire_due_subscription');
        }
      } catch (err) {}
    }
  }
}
