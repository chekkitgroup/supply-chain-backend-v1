export enum SubscriptionStatus {
  Success = 'success',
  Failed = 'failed',
  Pending = 'pending',
}

export enum SubscriptionChannel {
  Admin = 'admin',
  Others = 'others',
}
