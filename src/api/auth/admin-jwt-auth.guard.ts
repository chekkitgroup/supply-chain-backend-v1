import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import Role from 'api/users/models/role.model';

import { RoleService } from 'api/users/role/role.service';

@Injectable()
export class AdminAuthGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private readonly roleService: RoleService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<any> {
    const isSystemAdmin = this.reflector.get<boolean>(
      'IS_SYSTEM',
      context.getHandler(),
    );

    const request = context.switchToHttp().getRequest();

    const userRoleId = request.user.roleId;

    let role: Role;

    if (userRoleId) role = await this.roleService.getOne(userRoleId);

    return role?.name === 'Admin' && role.system === isSystemAdmin;
  }
}
