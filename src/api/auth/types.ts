export interface userAuthDetails {
  id: string;
  inviteeId?: string;
  email: string;
  name: string;
  companyId: string;
  companyName: string;
  roleId: string;
}

export interface RequestWithUser extends Request {
  user: userAuthDetails;
}
