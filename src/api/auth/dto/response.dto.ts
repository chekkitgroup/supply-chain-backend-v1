import { getSchemaPath } from '@nestjs/swagger';

class Subscription {
  status: boolean;
}

class Company {
  name: string;
  address: string;
  country: string;
  identifier?: any;
  isFreeTrial: boolean;
  subscription: Subscription;
}

export class AccountLoginOutputDto {
  permissions: string[];
  isAdminUser: boolean;
  name: string;
  companyRole: string;
  company: Company;
  token: string;
}

export const AccountLoginOutputDtoSchema = getSchemaPath(AccountLoginOutputDto);
