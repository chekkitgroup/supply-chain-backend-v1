import { forwardRef, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from 'api/users/users.module';
import { PassportModule } from '@nestjs/passport';

import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtStrategy } from './jwt.strategy';
import { LocalStrategy } from './local.strategy';
import { AuthEvent } from './listener/verify-account';
import { PermissionGuard } from './permission.guard';
import { AdminAuthGuard } from './admin-jwt-auth.guard';
import { JwtInviteAuthGuard } from './jwt-invite-auth-guard';
import { JwtInviteStrategy } from './jwt-invite-strategy';
import { UtilsModule } from 'utils/utils.module';

@Module({
  imports: [
    forwardRef(() => UsersModule),
    PassportModule,
    ConfigModule,
    UtilsModule,

    JwtModule.registerAsync({
      useFactory: (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: { expiresIn: '24h' },
      }),
      inject: [ConfigService],
    }),
  ],

  providers: [
    AuthService,
    LocalStrategy,
    JwtStrategy,
    JwtInviteStrategy,
    AuthEvent,
    PermissionGuard,
    AdminAuthGuard,
    JwtInviteAuthGuard,
  ],

  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
