import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { AdminAuthGuard } from './admin-jwt-auth.guard';
import { JwtAuthGuard } from './jwt-auth.guard';
import { PermissionGuard } from './permission.guard';

export const AdminAuth = (isSystemAdmin?: boolean) => {
  return applyDecorators(
    SetMetadata('IS_SYSTEM', isSystemAdmin),
    UseGuards(JwtAuthGuard, AdminAuthGuard),
  );
};

export const Authorize = (...permissions: string[]) => {
  return applyDecorators(
    UseGuards(JwtAuthGuard),
    SetMetadata('PERMISSIONS', permissions),
    UseGuards(PermissionGuard),
  );
};
