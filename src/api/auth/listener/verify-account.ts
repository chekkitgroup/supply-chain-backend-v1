import { AccountEvent } from '../event/verfiy-account';
import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import {
  resetPasswordEmailTemplate,
  VerifyEmailTemplate,
} from 'utils/constant';
import sendEmail from 'utils/email';
import logger from 'utils/logger';

@Injectable()
export class AuthEvent {
  @OnEvent('verfiy.email')
  handleVerifyEmailCreatdEvent(event: AccountEvent) {
    this.sendEmailVerificationLink(event.name, event.email, event.token);
  }

  async sendEmailVerificationLink(
    fullName: string,
    email: string,
    token: string,
  ): Promise<boolean> {
    const link = `${process.env.VERIFY_ACCOUNT_URL}/${token}`;

    const emailContent = VerifyEmailTemplate(link, fullName);

    logger.info(
      `About to send verification link sent for user with details ${JSON.stringify(
        email,
      )}`,
    );

    const emailSent = await sendEmail(
      email,
      'Verify your account',
      emailContent,
    );

    return emailSent;
  }

  @OnEvent('reset.password')
  handleResetPasswordCreatedEvent(event: AccountEvent) {
    this.sendResetPasswordLink(event.name, event.email, event.token);
  }

  async sendResetPasswordLink(
    fullName: string,
    email: string,
    token: string,
  ): Promise<boolean> {
    logger.info(
      `About to send password  reset link sent for user with details ${JSON.stringify(
        email,
      )}`,
    );
    const link = `${process.env.RESET_PASSWORD_URL}/${token}`;

    const message = resetPasswordEmailTemplate(fullName, link);

    const emailSent = await sendEmail(
      email,
      'CHEKKIT ASSET MANAGEMENT: PASSWORD VERIFICATION LINK',
      message,
    );

    return emailSent;
  }
}
