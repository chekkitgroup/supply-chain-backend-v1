import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtInviteAuthGuard extends AuthGuard('jwt-invite-auth') {}
