import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from 'api/users/users.service';

@Injectable()
export class JwtInviteStrategy extends PassportStrategy(
  Strategy,
  'jwt-invite-auth',
) {
  constructor(private readonly userService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: any) {
    const user = await this.userService.findOneUserInvite({
      id: payload.id,
    });

    if (!user) throw new UnauthorizedException();

    return {
      id: user.id,
      ...payload,
    };
  }
}
