import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import {
  Injectable,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import User from 'api/users/models/user.model';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({ usernameField: 'email' });
  }

  async validate(email: string, password: string): Promise<User> {
    const userLogin = {
      email,
      password,
    };

    const user = await this.authService.validateUser(userLogin);

    if (user === 'notVerified')
      throw new UnauthorizedException(
        'Your account is not verified. Kindly check your email and verify your account',
      );

    if (!user) throw new UnprocessableEntityException('Invalid Credentials');

    return user?.dataValues;
  }
}
