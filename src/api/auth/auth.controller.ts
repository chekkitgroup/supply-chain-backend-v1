import {
  Body,
  Controller,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import {
  CreateUserDto,
  LoginDto,
  resetPasswordDto,
  updatePasswordDto,
} from 'api/users/dtos/user.dto';

import { successResponse, successResponseSpec } from 'utils/response';
import { ResponseSchemaHelper } from 'utils/types/output-model';
import { AuthService } from './auth.service';
import { AccountLoginOutputDtoSchema } from './dto/response.dto';
import { JwtAuthGuard } from './jwt-auth.guard';
import { LocalAuthGuard } from './local-auth.guard';
import { RequestWithUser } from './types';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Sign up a user' })
  @ApiResponse({
    status: 201,
    description: 'Account created successfully',
  })
  @Post('/signup')
  async createAccount(
    @Body() userDetails: CreateUserDto,
  ): Promise<successResponseSpec> {
    await this.authService.createUser(userDetails);

    return successResponse(
      'Account Created. Please check your email and to verify your account',
      201,
    );
  }

  @ApiOperation({ summary: 'Verify Account' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Patch('/verify-account')
  @ApiResponse({ status: 200, description: 'Account Updated Successfully' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  async verifyAccount(@Request() req: any): Promise<successResponseSpec> {
    await this.authService.verifyAccount(req.user.id);

    return successResponse('Account Verified Successfully');
  }

  @ApiOperation({ summary: 'SignIn User' })
  @Post('/signin')
  @UseGuards(LocalAuthGuard)
  @ApiResponse({
    status: 200,
    description: 'Login Successful',
    schema: ResponseSchemaHelper(AccountLoginOutputDtoSchema),
  })
  @ApiResponse({ status: 422, description: 'Invalid Credentials' })
  async loginUser(
    @Request() req: RequestWithUser,
    @Body() _loginDetails: LoginDto,
  ): Promise<successResponseSpec> {
    const [details, token] = await this.authService.loginUser(req.user);

    return successResponse('Login Successful', 200, {
      ...details,
      token,
    });
  }

  @ApiOperation({ summary: 'SignIn Agent' })
  @Post('/agent-signin')
  @UseGuards(LocalAuthGuard)
  @ApiResponse({
    status: 200,
    description: 'Login Successful',
    schema: ResponseSchemaHelper(AccountLoginOutputDtoSchema),
  })
  @ApiResponse({ status: 422, description: 'Invalid Credentials' })
  async loginAgent(
    @Request() req: RequestWithUser,
    @Body() _loginDetails: LoginDto,
  ): Promise<successResponseSpec> {
    const [details, token] = await this.authService.loginAgent(req.user);

    return successResponse('Login Successful', 200, {
      ...details,
      token,
    });
  }

  @ApiOperation({ summary: 'Reset Password' })
  @Post('/reset-password')
  @ApiResponse({ status: 200, description: 'Login Successful' })
  @ApiResponse({ status: 422, description: 'Invalid Credentials' })
  async resetPassword(
    @Body() userDetails: resetPasswordDto,
  ): Promise<successResponseSpec> {
    await this.authService.resetPassword(userDetails?.email);

    return successResponse(
      'Password reset link has to your registered email address',
    );
  }

  @ApiOperation({ summary: 'Update a user password' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Patch('/update-password')
  @ApiResponse({ status: 200, description: 'Update Successful' })
  async updatePassword(
    @Request() req: RequestWithUser,
    @Body() userDetails: updatePasswordDto,
  ): Promise<successResponseSpec> {
    await this.authService.updatePassword(userDetails, req.user);

    return successResponse('Password Update Successfully');
  }

  @ApiOperation({ summary: 'Seed system admin' })
  @Post('/seed-system-admin')
  @ApiResponse({ status: 201, description: 'System Admin Created Successful' })
  async createdSystemAdmin(): Promise<successResponseSpec> {
    await this.authService.createSystemAdmin();

    return successResponse('System admin created successfully', 201);
  }
}
