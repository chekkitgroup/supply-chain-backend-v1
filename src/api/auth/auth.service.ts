import {
  ConflictException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { EventEmitter2 } from '@nestjs/event-emitter';
import { CompanyService } from 'api/users/company/company.service';
import { CreateCompanyDto } from 'api/users/dtos/company.dto';
import {
  CreateUserDto,
  LoginDto,
  updatePasswordDto,
} from 'api/users/dtos/user.dto';
import { RoleService } from 'api/users/role/role.service';
import { UserExtension } from 'api/users/types';

import { comparePassword, hashPassword } from 'utils/bcrypt';

import { UsersService } from '../users/users.service';
import { userAuthDetails } from './types';
import { AccountEvent } from './event/verfiy-account';
import { objectResponse } from 'utils/response';
import logger from 'utils/logger';
import { AccountLoginOutputDto } from './dto/response.dto';
import { UtilsService } from 'utils/utils.service';

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly companyService: CompanyService,
    private readonly roleService: RoleService,
    private eventEmitter: EventEmitter2,
    private readonly utilsService: UtilsService,
  ) {}

  async createUser(userDetails: CreateUserDto): Promise<void> {
    logger.info('About to get Admin role');

    const role = await this.roleService.getAdminRole();

    if (!role)
      throw new UnprocessableEntityException(
        'Kindly contact chekkit to sync admin role',
      );

    userDetails.roleId = role.id;

    const userData = { ...userDetails };

    logger.info('About to create user with specific');

    const user = (await this.usersService.createUser(
      userData,
    )) as UserExtension;

    const companyDetails: CreateCompanyDto = {
      name: userDetails.companyName,
      address: userDetails.address,
      country: userDetails.country,
      userId: user.id,
      identifier: userDetails.companyIdentifier,
      moduleIds: userDetails.moduleIds,
    };

    logger.info(`About to create company for user with id : ${user.id}`);

    const company = await this.companyService.create(companyDetails);

    logger.info(`About to set company for user with id : ${user.id}`);

    user.setCompany(company);

    const token = this.jwtService.sign({
      id: user.id,
    });

    const verify: AccountEvent = {
      name: user.name,
      email: user.email,
      token: token,
    };

    this.eventEmitter.emit('verfiy.email', verify);
    //log notification
    const notification = {
      content: 'You have successfully created an account',
      title: 'Account creation',
      userId: user.id,
    };
    this.utilsService.createNotification(notification);
  }

  async verifyAccount(userId: string): Promise<any> {
    logger.info(`About to get user with userId : ${userId}`);

    const user = await this.usersService.findOne({ id: userId });

    if (!user)
      throw new NotFoundException('This user does not exist in our system ');

    logger.info(
      `About to update verification status for user with  id : ${userId}`,
    );

    await this.usersService.update({ id: userId }, { isVerified: true });
    //log notification
    const notification = {
      content: 'You have successfully verified your account',
      title: 'Account verification',
      userId,
    };
    this.utilsService.createNotification(notification);
  }

  async loginUser(
    user: userAuthDetails,
  ): Promise<[AccountLoginOutputDto, string]> {
    logger.info(`About to get login details for user with id : ${user.id}`);

    const userDetails = await this.usersService.getUserAuthDetails(user);

    const token = this.jwtService.sign({
      id: user.id,
      email: user.email,
      name: user.name,
      companyId: user.companyId,
      roleId: user.roleId,
    });

    //log notification
    const notification = {
      content: 'Successfully logged in to your account',
      title: 'Account log in',
      userId: user.id,
    };
    this.utilsService.createNotification(notification);
    return [userDetails, token];
  }

  async loginAgent(
    user: userAuthDetails,
  ): Promise<[AccountLoginOutputDto, string]> {
    logger.info(`About to get login details for agent with id : ${user.id}`);

    const userDetails = await this.usersService.getUserAuthDetails(user);

    const token = this.jwtService.sign({
      id: user.id,
      email: user.email,
      name: user.name,
      companyId: user.companyId,
      roleId: user.roleId,
    });

    //log notification
    const notification = {
      content: 'Successfully logged in to your account',
      title: 'Account log in',
      userId: user.id,
    };
    this.utilsService.createNotification(notification);
    return [userDetails, token];
  }

  async resetPassword(email: string): Promise<void> {
    logger.info(`About to get user details for user with email : ${email}`);

    const user = await this.usersService.findOne({ email });

    if (!user) throw new NotFoundException('Invalid credentials');

    const token = this.jwtService.sign({
      id: user.id,
    });

    const reset: AccountEvent = {
      name: user.name,
      email,
      token,
    };

    this.eventEmitter.emit('reset.password', reset);
    //log notification
    const notification = {
      content: 'You initiated a password reset process for your account',
      title: 'Password reset request',
      userId: user.id,
    };
    this.utilsService.createNotification(notification);
  }

  async updatePassword(
    userDetails: updatePasswordDto,
    requestedUser: userAuthDetails,
  ) {
    const { newPassword, confirmPassword } = userDetails;

    logger.info(
      `About to verify new Password for user with id : ${requestedUser.id}`,
    );

    if (newPassword !== confirmPassword)
      throw new UnprocessableEntityException('Password does not match');

    const user = await this.usersService.findOne({
      id: requestedUser.id,
    });

    if (!user) throw new NotFoundException('User does not exist');

    logger.info(
      `About to update user password for user with id : ${requestedUser.id}`,
    );

    await this.usersService.update(
      { id: user.id },
      {
        password: hashPassword(newPassword),
      },
    );

    //log notification
    const notification = {
      content: 'You have successfully changed your account password',
      title: 'Password update',
      userId: user.id,
    };
    this.utilsService.createNotification(notification);
  }

  async validateUser(userDetails: LoginDto): Promise<any> {
    logger.info(`About to get user with email: ${userDetails.email}`);

    const user = await this.usersService.findOne({
      email: userDetails.email,
    });

    if (user && !user.isVerified) {
      const token = this.jwtService.sign({
        id: user.id,
      });

      const verify: AccountEvent = {
        name: user.name,
        email: user.email,
        token: token,
      };

      this.eventEmitter.emit('verfiy.email', verify);

      return 'notVerified';
    }

    if (user && comparePassword(userDetails.password, user.password)) {
      user.password = undefined;

      return user;
    }

    return null;
  }

  async createSystemAdmin() {
    logger.info('About to get system admin role');

    const role = await this.roleService.getSystemAdminRole();

    if (!role)
      throw new NotFoundException('Roles not found, kindly sync/setup roles');

    const systemAdmin = await this.usersService.findOne({
      email: process.env.CHEKKIT_ADMIN_EMAIL,
    });

    if (systemAdmin) {
      throw new ConflictException('System admin already seeded');
    }

    const createUser: any = {
      name: process.env.CHEKKIT_ADMIN_NAME,
      email: process.env.CHEKKIT_ADMIN_EMAIL,
      roleId: role.id,
      password: process.env.CHEKKIT_ADMIN_PASSWORD,
    };

    logger.info('About to get create system admin user for Chekkit');

    await this.usersService.createUser(createUser);
  }

  async generateToken(payload: objectResponse, expiry: string) {
    const token = this.jwtService.sign(payload, { expiresIn: expiry });

    return token;
  }
}
