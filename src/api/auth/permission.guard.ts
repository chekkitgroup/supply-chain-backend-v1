import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { RoleService } from 'api/users/role/role.service';
import { UsersService } from 'api/users/users.service';
import { RequestWithUser } from './types';

@Injectable()
export class PermissionGuard implements CanActivate {
  constructor(
    private reflector: Reflector,

    private userService: UsersService,

    private roleService: RoleService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const permissions = this.reflector.get<string[]>(
      'PERMISSIONS',
      context.getHandler(),
    );

    if (!permissions.length) return true;

    const request = context.switchToHttp().getRequest() as RequestWithUser;

    const role = await this.roleService.getRoleAndPermissions(
      request.user.roleId,
    );

    const user = await this.userService.findOne({ id: request.user.id });

    const userPermissions = role && role.permissions ? role.permissions : [];

    return permissions.some((permission) => {
      let userPermission = userPermissions.map(
        (permission) => permission?.identifier,
      );

      const temporaryPermissions = this.userService.getTemporaryPermissions(
        user.temporaryPermissions,
      );

      userPermission = [...userPermission, ...temporaryPermissions];

      return userPermission.includes(permission);
    });
  }
}
