export class approveDto {
  status: boolean;
  startDate: Date;
  endDate: Date;
}
