import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class ProvisionGWDTO {
    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    macAddress: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    companyId: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    location: string;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    deviceType: string;
  }