import { getSchemaPath } from '@nestjs/swagger';
import Plan from 'api/plans/models/plan.model';
import SubscriptionLogs from 'api/subscription/models/subscriptionLogs.model';
import Company from 'api/users/models/company.model';

export class CompanyOutputDto {
  id: string;
  name: string;
  address: string;
  country: number;
  userId: string;
  planId: string;
  plan: Plan;
  created_at: Date;
  deleted_at: Date;
  updated_at: Date;

  static FromCompanyOutput(company: Company) {
    const output = new CompanyOutputDto();

    const exposedKeys = ['id', 'name', 'address', 'country', 'plan'];

    exposedKeys.forEach((exposedKey) => {
      output[exposedKey] = company[exposedKey];
    });

    return output;
  }
}

export const CompanyOutputDtoSchema = getSchemaPath(CompanyOutputDto);

export class SubscriptionLogsOutputDto {
  id: string;
  status: boolean;
  companyId: string;
  planId: string;
  startDate: Date;
  endDate: Date;
  created_at: Date;
  deleted_at: Date;
  updated_at: Date;

  static FromSubscriptionLogsOutput(subscriptionLogs: SubscriptionLogs) {
    const output = new SubscriptionLogsOutputDto();

    const exposedKeys = [
      'id',
      'status',
      'startDate',
      'endDate',
      'plan',
      'created_at',
      'updated_at',
    ];

    exposedKeys.forEach((exposedKey) => {
      output[exposedKey] = subscriptionLogs[exposedKey];
    });

    return output;
  }
}

export const SubscriptionLogsOutputDtoSchema = getSchemaPath(
  SubscriptionLogsOutputDto,
);
