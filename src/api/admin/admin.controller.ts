import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AdminAuth } from 'api/auth/authority.decorator';
import { AssignRFIDsDto, CreateRFIDDto } from 'api/rfid/dtos/rfid';

import { CreateRoleDto } from 'api/users/dtos/role.dto';

import { PaginationFilter } from 'utils/pagination';

import { successResponse, successResponseSpec } from 'utils/response';
import { ResponseSchemaHelper } from 'utils/types/output-model';

import { AdminService } from './admin.service';
import { ProvisionGWDTO } from './dtos';
import {
  CompanyOutputDtoSchema,
  SubscriptionLogsOutputDto,
  SubscriptionLogsOutputDtoSchema,
} from './dtos/response.dto';

@ApiTags('Admin')
@Controller('admin')
export class AdminController {
  constructor(private readonly _adminService: AdminService) {}

  @ApiOperation({ summary: 'Set up default Roles' })
  @ApiResponse({ status: 200, description: 'seeds default roles' })
  @Post('setup-roles')
  async syncRoles(): Promise<successResponseSpec> {
    await this._adminService.insertRoles();

    return successResponse('Roles setup successfully');
  }

  @ApiOperation({ summary: 'Set up default permissions' })
  @ApiResponse({ status: 200, description: 'seeds default permissions' })
  @Post('setup-permissions')
  async syncPermissions(): Promise<successResponseSpec> {
    await this._adminService.insertPermissions();

    return successResponse('Permissions setup successfully');
  }

  @ApiOperation({ summary: 'Set up default hardwares' })
  @ApiResponse({ status: 200, description: 'seeds default hardwares' })
  @Post('seed-hardwares')
  async syncHardwares(): Promise<successResponseSpec> {
    await this._adminService.insertHardwares();

    return successResponse('Hardwares setup successfully');
  }

  @ApiOperation({ summary: 'Set up default features' })
  @ApiResponse({ status: 200, description: 'seeds default features' })
  @Post('seed-features')
  async syncFeatures(): Promise<successResponseSpec> {
    await this._adminService.insertFeatureCategories();

    await this._adminService.insertFeatures();

    return successResponse('Features sync successfully');
  }

  @ApiOperation({ summary: 'Set up default plans' })
  @ApiResponse({ status: 200, description: 'seeds default plans' })
  @Post('seed-plans')
  async syncPlans(): Promise<successResponseSpec> {
    await this._adminService.insertPlans();

    return successResponse('Plans sync successfully');
  }

  @ApiOperation({ summary: 'Set up default products' })
  @ApiResponse({ status: 200, description: 'seeds default products' })
  @Post('seed-products')
  async syncProducts(): Promise<successResponseSpec> {
    await this._adminService.insertProducts();

    return successResponse('Products setup successfully');
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Get all companies' })
  @ApiResponse({
    status: 200,
    description: 'Companies retrieved successfully',
    schema: ResponseSchemaHelper(CompanyOutputDtoSchema, 'array'),
  })
  @Get('companies')
  async getCompanies(
    @Query() filter?: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [companies] =
      await this._adminService.getCompanies();

    return successResponse(
      'Companies retrieved successfully',
      200,
      companies,
      true,
    );
  }

  @ApiOperation({ summary: 'Get a Company' })
  @ApiResponse({
    status: 200,
    description: 'Get a Company',
    schema: ResponseSchemaHelper(CompanyOutputDtoSchema),
  })
  @ApiBearerAuth()
  @AdminAuth(true)
  @Get('companies/:id')
  async getCompany(@Param('id') id: string): Promise<successResponseSpec> {
    const company = await this._adminService.getCompany(id);

    return successResponse('Company users retrieved', 200, company);
  }

  @ApiOperation({ summary: 'Get users for a particular company' })
  @ApiResponse({
    status: 200,
    description: 'Get users for a company',
  })
  @ApiBearerAuth()
  @AdminAuth(true)
  @Get('companies/:companyId/users')
  async getUsersForACompany(
    @Param('companyId') companyId: string,

    @Query() filter?: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [users, paginationMetaData] =
      await this._adminService.getCompanyUsers(
        companyId,
        +filter.page,
        +filter.perPage,
      );

    return successResponse(
      'Company users retrieved',
      200,
      users,
      true,
      paginationMetaData,
    );
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Get all subscriptions logs for a company' })
  @ApiResponse({
    status: 200,
    description: 'Subscription Logs retrieved successfully',
    schema: ResponseSchemaHelper(SubscriptionLogsOutputDtoSchema, 'array'),
  })
  @Get('company/:id/subscriptions')
  async getCompanySubScriptions(
    @Param('id') companyId: string,
    @Query() filter?: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [subLogs, paginationMetaData] =
      await this._adminService.getCompanySubLogs(
        companyId,
        +filter.page,
        +filter.perPage,
      );

    const output = subLogs?.map((subLog) =>
      SubscriptionLogsOutputDto.FromSubscriptionLogsOutput(subLog),
    );

    return successResponse(
      'Subscriptions retrieved successfully',
      200,
      output,
      true,
      paginationMetaData,
    );
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Approve a company subscription' })
  @ApiResponse({
    status: 200,
    description: 'Subscriptions retrieved successfully',
  })
  @Put('company/:id/approve-subscription/:subId')
  async approveSub(
    @Param('id') companyId: string,
    @Param('subId') subLogId: string,
  ): Promise<successResponseSpec> {
    await this._adminService.approveSubscription(companyId, subLogId);

    return successResponse('subscription approved successfully');
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Get a company permissions based on their plan' })
  @ApiResponse({
    status: 200,
    description: 'Permissions retrieved successfully',
  })
  @Get('company/:id/permissions')
  async getCompanyPermissions(
    @Param('id') companyId: string,
  ): Promise<successResponseSpec> {
    const permissions = await this._adminService.getCompanyPermissions(
      companyId,
    );

    return successResponse(
      'Permissions retrieved successfully',
      200,
      permissions,
    );
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Get a company roles' })
  @ApiResponse({
    status: 200,
    description: 'Roles retrieved successfully',
  })
  @Get('company/:id/roles')
  async getCompanyRoles(
    @Param('id') companyId: string,
  ): Promise<successResponseSpec> {
    const roles = await this._adminService.getCompanyRoles(companyId);

    return successResponse('Roles retrieved successfully', 200, roles);
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Get a company roles' })
  @ApiResponse({
    status: 201,
    description: 'Create role successfully',
  })
  @Post('company/:id/create-role')
  async createRole(
    @Param('id') companyId: string,
    @Body() RolesDetails: CreateRoleDto,
  ): Promise<successResponseSpec> {
    const role = await this._adminService.createRoleForCompany(
      companyId,
      RolesDetails,
    );

    return successResponse('Roles created successfully', 201, role);
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Get all hardwares for a company' })
  @ApiResponse({
    status: 200,
    description: 'Company hardwares retrieved successfully',
  })
  @Get('company/:id/hardwares')
  async getCompanyOrderedHardwares(
    @Param('id') companyId: string,

    @Query() filter: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [hardwares, paginationMetaData] =
      await this._adminService.getCompanyHardwares(companyId, filter);

    return successResponse(
      'Company hardwares retrieved successfully',
      200,
      hardwares,
      true,
      paginationMetaData,
    );
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Get all assets for a company' })
  @ApiResponse({
    status: 200,
    description: 'Company assets retrieved successfully',
  })
  @Get('company/:id/assets')
  async getCompanyOnboardedAssets(
    @Param('id') companyId: string,

    @Query() filter?: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [assets, paginationMetaData] =
      await this._adminService.getCompanyOnboardedAssets(companyId, filter);

    return successResponse(
      'Company hardwares retrieved successfully',
      200,
      assets,
      true,
      paginationMetaData,
    );
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Asset queue paused successfully' })
  @ApiResponse({
    status: 200,
    description: 'Asset queue paused successfully',
  })
  @Post('assets-onboard/paused')
  async pauseAssetOnbaord(): Promise<successResponseSpec> {
    await this._adminService.pauseAssetOnboarding();

    return successResponse('Assets queue paused successfully', 200);
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Asset queue resumed successfully' })
  @ApiResponse({
    status: 200,
    description: 'Asset queue resumed successfully',
  })
  @Post('assets-onboard/resume')
  async resumeAssetOnboard(): Promise<successResponseSpec> {
    await this._adminService.resumeAssetOnboarding();

    return successResponse('Assets queue resumed successfully', 200);
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Register RFID Tags successfully' })
  @ApiResponse({
    status: 200,
    description: 'Register RFID Tags successfully',
  })
  @Post('rfids')
  async registerRFIDTags(
    @Body() input: CreateRFIDDto,
  ): Promise<successResponseSpec> {
    await this._adminService.registerRFIDTags(input);

    return successResponse('RFID registered successfully', 200);
  }

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Retrieves RFID Tags for a company' })
  @ApiResponse({
    status: 200,
    description: 'Retrieves RFID Tags successfully',
  })
  // @AdminAuth(true)
  @Get('rfids')
  async getAllRFIDTags(
    @Query() filter?: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [response, paginationMetaData] =
      await this._adminService.getAllRFIDTags(filter);

    return successResponse(
      'RFID Tags retrieved successfully',
      200,
      response,
      true,
      paginationMetaData,
    );
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Assigned RFID Tags to a company' })
  @ApiResponse({
    status: 200,
    description: 'Assigned RFID Tags to a company successfully',
  })
  @Put('rfids/company/:id/assign')
  async assignRFIDTags(
    @Body() input: AssignRFIDsDto,
    @Param('id') companyId: string,
  ): Promise<successResponseSpec> {
    await this._adminService.assignRFIDTags(input, companyId);

    return successResponse('RFID Tag assigned successfully', 200);
  }

  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'Get all unassigned gateways' })
  @Get('gateways/unassigned')
  async getAllAvailableGateways( @Query() filter?: PaginationFilter,): Promise<successResponseSpec>{
    const resp = await this._adminService.getAvailableGateways(filter);
    return successResponse('Gateways fatched successfully', 200, resp)
  }
  
  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'provision a gateway for a company' })
  @Put('gateways/assign')
  async assignGate(@Body() data: ProvisionGWDTO){
    await this._adminService.provisionGateway(data)
    return successResponse('Gateway provisioned successfully', 200);
  }
  
  @ApiBearerAuth()
  @AdminAuth(true)
  @ApiOperation({ summary: 'deprovision a gateway and unassign from company' })
  @Delete('gateways/de-provision/:macAddress')
  async deprovisionGateway(@Param('macAddress') macAddress: string){
    await this._adminService.deprovisionGateway(macAddress)
    return successResponse('Gateway deprovisioned successfully', 200);
  }
}
