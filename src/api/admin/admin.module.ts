import { forwardRef, Module } from '@nestjs/common';
import { AssetModule } from 'api/asset/asset.module';
import { HardwareModule } from 'api/hardware/hardware.module';
import { PlansModule } from 'api/plans/plans.module';
import { RfidModule } from 'api/rfid/rfid.module';
import { SubscriptionModule } from 'api/subscription/subscription.module';
import { UsersModule } from 'api/users/users.module';
import { SeedersModule } from 'database/seeders/seeders.module';
import { UtilsModule } from 'utils/utils.module';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';

@Module({
  imports: [
    forwardRef(() => UsersModule),
    forwardRef(() => PlansModule),

    SeedersModule,
    SubscriptionModule,

    UtilsModule,
    AssetModule,

    HardwareModule,
    RfidModule,
  ],
  controllers: [AdminController],
  providers: [AdminService],
  exports: [AdminService],
})
export class AdminModule {}
