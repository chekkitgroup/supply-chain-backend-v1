import {
  ConflictException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AssetService } from 'api/asset/asset.service';
import Asset from 'api/asset/models/asset.model';
import { HardwareService } from 'api/hardware/hardware.service';
import HardwareOrder from 'api/hardware/models/hardwareOrder.model';
import { PlansService } from 'api/plans/plans.service';
import { AssignRFIDsDto, CreateRFIDDto } from 'api/rfid/dtos/rfid';
import Rfid from 'api/rfid/models/rfid.model';
import { RfidService } from 'api/rfid/rfid.service';

import SubscriptionLogs from 'api/subscription/models/subscriptionLogs.model';
import { SubscriptionService } from 'api/subscription/subscription.service';
import { SubscriptionStatus } from 'api/subscription/types';

import { CompanyService } from 'api/users/company/company.service';

import { CreateRoleDto } from 'api/users/dtos/role.dto';
import Company from 'api/users/models/company.model';
import Role from 'api/users/models/role.model';
import { RoleService } from 'api/users/role/role.service';
import { UsersService } from 'api/users/users.service';
import { SeedersService } from 'database/seeders/seeders.service';
import logger from 'utils/logger';

import {
  getOffSet,
  getPaginationMetaData,
  PaginationFilter,
} from 'utils/pagination';
import { PaginationMeta } from 'utils/types/pagination';
import { UtilsService } from 'utils/utils.service';
import { ProvisionGWDTO } from './dtos';
import { CompanyOutputDto } from './dtos/response.dto';

@Injectable()
export class AdminService {
  constructor(
    private readonly companyService: CompanyService,
    private readonly subscriptionService: SubscriptionService,

    @Inject(forwardRef(() => PlansService))
    private readonly plansService: PlansService,

    private readonly roleService: RoleService,
    private readonly utilsService: UtilsService,

    @Inject(forwardRef(() => UsersService))
    private readonly userService: UsersService,

    private readonly assetService: AssetService,

    private readonly hardwareService: HardwareService,

    private readonly rfidService: RfidService,

    private readonly seedersService: SeedersService,
  ) {}

  async getCompanies(
  ): Promise<[CompanyOutputDto[]]> {
    logger.info(
      'About to get all companies in supply chain system by pagination',
    );
    const [companies] = await this.companyService.getAll(
    );

    logger.info('Completes get companies');

    return [companies];
  }

  async getCompanySubLogs(
    companyId: string,
    page: number,
    perPage: number,
  ): Promise<[SubscriptionLogs[], PaginationMeta]> {
    const offset = getOffSet(page, perPage);

    logger.info(`About to get subscription logs for  company ${companyId}`);

    const { count: totalCount, rows: subLogs } =
      await this.subscriptionService.findAllLogs(companyId, perPage, offset);

    logger.info(
      `About to get pagination metadata for subscription logs for company ${companyId}`,
    );

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      page,
      perPage,
      subLogs.length,
    );

    logger.info(`Retrieved pagination metadata for company ${companyId}`);

    return [subLogs, paginationMetaData];
  }

  async approveSubscription(companyId: string, subLogId: string) {
    logger.info(
      `About to get subscription log with id ${subLogId} for company with id ${companyId}`,
    );
    const subscriptionLog = await this.subscriptionService.findSubLogByCompany(
      companyId,
      subLogId,
    );

    if (!subscriptionLog)
      throw new NotFoundException('No subscription log found');

    logger.info(
      `About to get the company\'s subscription plan with id ${subscriptionLog.planId}`,
    );

    const plan = await this.plansService.findOne(subscriptionLog.planId);

    logger.info(`Setting subscription end Date`);

    const subscriptionEndDate = new Date();

    subscriptionEndDate.setFullYear(
      subscriptionEndDate.getFullYear() + plan.numberOfYears,
    );

    const updateProps = {
      startDate: new Date(),
      endDate: subscriptionEndDate,
      status: SubscriptionStatus.Success,
    };

    logger.info(
      `About to update the subscription log status for the id: ${subscriptionLog.id}`,
    );
    await this.subscriptionService.updatelog(
      companyId,
      updateProps,
      subscriptionLog.id,
    );

    const subscriptionData = {
      price: plan.price,
      planId: plan.id,
      companyId,
      startDate: new Date(),
      endDate: subscriptionEndDate,
      status: true,
    };

    logger.info(
      `Create subscription for company with subscription details: ${JSON.stringify(
        subscriptionData,
      )}`,
    );

    const newSubscription = (await this.subscriptionService.create(
      subscriptionData,
    )) as any;

    logger.info(
      `Setting subscription logs ${JSON.stringify(
        subscriptionLog,
      )} for subscription with id ${newSubscription.id}`,
    );

    await newSubscription.setSubscriptionLogs(subscriptionLog);

    logger.info(`About to set company's current plan with id : ${plan.id}`);

    await this.companyService.update({ planId: plan.id }, { id: companyId });

    return newSubscription;
  }

  async createRoleForCompany(
    companyId: string,
    roleData: CreateRoleDto,
  ): Promise<Role> {
    roleData.name = this.utilsService.transformToSentenceCase(roleData.name);

    logger.info(
      `About to get role for company with id: ${companyId} and role with name ${roleData.name}`,
    );
    const role = await this.roleService.getOneByCompany(companyId, {
      name: roleData.name,
    });

    if (role) throw new ConflictException('Role already exist for company');

    roleData.companyId = companyId;

    logger.info(
      `About to create new role for company with details ${JSON.stringify(
        roleData,
      )}`,
    );

    const newRole = (await this.roleService.create(roleData)) as any;

    logger.info(
      `About to get company permissions for a particular company  with id ${companyId}`,
    );

    const companyPermissions = await this.getCompanyPermissions(companyId);

    roleData.permissions = roleData.permissions.filter((permission) =>
      companyPermissions.includes(permission),
    );

    logger.info(
      `About to get permission Ids for a particular company with id ${JSON.stringify(
        roleData.permissions,
      )}`,
    );

    const permissionIds = await Promise.all(
      roleData.permissions.map((permission) =>
        this.roleService.getPermissions(permission),
      ),
    );

    logger.info(
      'About to create role permission to bind roleId with permissions ',
    );

    await Promise.all(
      permissionIds.map((id) =>
        this.roleService.createRolePermission({
          roleId: newRole.id,
          permissionId: id,
        }),
      ),
    );

    return newRole;
  }

  async getCompanyPermissions(companyId: string): Promise<string[]> {
    logger.info(`About to get retrieve company Details with id ${companyId}`);

    const company = await this.companyService.findOne({ id: companyId });

    if (!company) throw new NotFoundException('company does not exist');

    logger.info(`About to retrieve company plan with id : ${company.planId}`);

    const plan = await this.plansService.findOne(company.planId);

    const role = await this.roleService.getAdminRole();

    let permissions = [];

    if (plan) {
      permissions = [...plan?.features.map((feature) => feature.name)];
    }

    const features = await this.plansService.getPlansFeatures();

    permissions = [
      ...permissions,
      ...role.permissions.map((permission) => permission.identifier),
    ];

    if (!plan) {
      const featureName = features.map((feature) => feature.name);

      permissions = permissions.filter(
        (permission) => !featureName.includes(permission),
      );
    }

    permissions = [...new Set(permissions)];

    return permissions;
  }

  async getCompanyRoles(companyId: string): Promise<Role[]> {
    logger.info(
      `About to get roles created for/by a company with id: ${companyId}`,
    );

    const roles = await this.roleService.getRolesforCompany(companyId);

    const defaultRoles = await this.roleService.getDefaultRoles();

    logger.info('Ccmpletes companies role retrieval');

    return [...roles, ...defaultRoles];
  }

  async getCompanyUsersByPage(
    companyId: string,
    page: number,
    perPage: number,
  ) {
    const companyUsers = await this.userService.getCompanyUsers(
      companyId,
      page,
      perPage,
    );

    return companyUsers;
  }

  async getCompanyUsers(companyId: string, page: number, perPage: number) {
    logger.info('start retrieving company users');

    const companyUsers = await this.userService.getCompanyUsers(
      companyId,
      +page,
      +perPage,
    );

    logger.info('completes retrieving company users');

    return companyUsers;
  }

  async getCompany(id: string): Promise<Company> {
    logger.info('start retrieving company');

    const company = await this.companyService.findOne({ id });

    if (!company) throw new NotFoundException('Company does not exist');

    logger.info('completes company retrieval');

    return company;
  }

  async getCompanyOnboardedAssets(
    companyId: string,
    input: PaginationFilter,
  ): Promise<[Asset[], PaginationMeta]> {
    const offset = getOffSet(+input.page, +input.perPage);

    const { count: totalCount, rows: assets } =
      await this.assetService.getCompanyAssets(
        companyId,
        +input.perPage,
        offset,
        ['stickerId'],
      );

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      +input.page,
      +input.perPage,
      assets.length,
    );

    return [assets, paginationMetaData];
  }

  async getCompanyHardwares(
    companyId: string,
    input: PaginationFilter,
  ): Promise<[HardwareOrder[], PaginationMeta]> {
    const offset = getOffSet(+input.page, +input.perPage);

    const { count: totalCount, rows: hardwares } =
      await this.hardwareService.getOrderedHardwares(
        companyId,
        +input.perPage,
        offset,
      );

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      +input.page,
      +input.perPage,
      hardwares.length,
    );

    return [hardwares, paginationMetaData];
  }
  async getAllRFIDTags(
    filter: PaginationFilter,
  ): Promise<[Rfid[], PaginationMeta]> {
    const [rfids, paginationMetaData] = await this.rfidService.getRfids(filter);

    return [rfids, paginationMetaData];
  }

  async getAvailableGateways(filter: PaginationFilter,): Promise<[Rfid[], PaginationMeta]> {
    const [gateways, paginationMetaData] = await this.rfidService.getGateways(filter);
    return [gateways, paginationMetaData];
  }

  async deprovisionGateway(macAddress: string){
    await this.rfidService.deprovisionGateway(macAddress)
  }

  async provisionGateway(data: ProvisionGWDTO){
    await this.rfidService.provisionGateway(data);
  }

  async pauseAssetOnboarding(): Promise<void> {
    await this.assetService.pause();
  }

  async resumeAssetOnboarding(): Promise<void> {
    await this.assetService.resume();
  }

  async registerRFIDTags(input: CreateRFIDDto) {
    await this.rfidService.register(input);
  }

  async assignRFIDTags(input: AssignRFIDsDto, companyId: string) {
    await this.rfidService.assign(input, companyId);
  }

  async insertRoles() {
    await this.seedersService.insertRoles();
  }
  async insertPermissions() {
    await this.seedersService.insertPermissions();
  }
  async insertHardwares() {
    await this.seedersService.insertHardwares();
  }
  async insertFeatures() {
    await this.seedersService.insertFeatures();
  }
  async insertFeatureCategories() {
    await this.seedersService.insertFeatureCategories();
  }

  async insertPlans() {
    await this.seedersService.insertPlans();
  }

  async insertProducts() {
    await this.seedersService.insertProducts();
  }
}
