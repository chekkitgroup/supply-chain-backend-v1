import { forwardRef, Module } from '@nestjs/common';
import { SiteService } from './site.service';
import { SiteController } from './site.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import Site from './models/site.model';
import { UsersModule } from 'api/users/users.module';
import { AssetModule } from 'api/asset/asset.module';

@Module({
  imports: [
    SequelizeModule.forFeature([Site]),
    forwardRef(() => UsersModule),
    forwardRef(() => AssetModule),
  ],
  exports: [SiteService],
  providers: [SiteService],
  controllers: [SiteController],
})
export class SiteModule {}
