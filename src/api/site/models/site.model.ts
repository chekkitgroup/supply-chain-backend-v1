import Asset from 'api/asset/models/asset.model';
import Company from 'api/users/models/company.model';
import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  HasMany,
  BelongsTo,
} from 'sequelize-typescript';

@Table
class Site extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({
    type: DataType.STRING(100),
    allowNull: false,
  })
  name: string;

  @Column({
    type: DataType.STRING(100),
    allowNull: false,
  })
  manager: string;

  @Column({
    type: DataType.STRING(50),
    allowNull: false,
  })
  phoneNumber: string;

  @Column({
    type: DataType.STRING(100),
  })
  email: string;

  @Column({
    type: DataType.STRING(50),
  })
  address: string;

  @Column({
    type: DataType.STRING(50),
  })
  state: string;

  @Column({
    type: DataType.STRING(50),
  })
  country: string;

  @Column({
    type: DataType.FLOAT,
  })
  latitude: number;

  @Column({
    type: DataType.FLOAT,
  })
  longitude: number;

  @HasMany(() => Asset, 'siteId')
  assets: Asset[];

  @BelongsTo(() => Company, 'companyId')
  company: Company;
}

export default Site;
