import { getSchemaPath } from '@nestjs/swagger';
import Site from '../models/site.model';

export class SiteOutputDto {
  id: string;
  name: string;
  manager: string;
  phoneNumber: string;
  email: string;
  state: string;
  country: string;
  latitude: number;
  longitude: number;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
  companyId: string;

  static FromSiteOutput(site: Site) {
    const output = new SiteOutputDto();

    const exposedKeys = [
      'id',
      'name',
      'manager',
      'phoneNumber',
      'email',
      'state',
      'country',
      'latitude',
      'longitude',
    ];

    exposedKeys.forEach((key) => {
      output[key] = site[key];
    });

    return output;
  }
}

export const SiteOutputDtoSchema = getSchemaPath(SiteOutputDto);
