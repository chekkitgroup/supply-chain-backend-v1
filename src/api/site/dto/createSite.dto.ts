import { ApiProperty } from '@nestjs/swagger';

import {
  IsNotEmpty,
  IsNumber,
  IsString,
  IsEmail,
  IsPhoneNumber,
} from 'class-validator';

export class CreateSiteDto {
  @ApiProperty({ example: 'Angels and Co' })
  @IsString()
  @IsNotEmpty()
  siteName: string;

  @ApiProperty({ example: 'Angels' })
  @IsString()
  @IsNotEmpty()
  siteManagerName: string;

  @ApiProperty({ example: '+2348167672018' })
  @IsPhoneNumber()
  @IsNotEmpty()
  phoneNumber: string;

  @ApiProperty({ example: 'vv@gmail.com' })
  @IsEmail()
  @IsNotEmpty()
  emailAddress: string;

  @ApiProperty({ example: 'Lagos' })
  @IsString()
  state: string;

  @ApiProperty({ example: 'Nigeria' })
  @IsString()
  @IsNotEmpty()
  country: string;

  @ApiProperty({ example: 6.453 })
  @IsNumber()
  @IsNotEmpty()
  latitude: number;

  @ApiProperty({ example: 8.453 })
  @IsNumber()
  @IsNotEmpty()
  longitude: number;
}
