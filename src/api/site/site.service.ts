import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { AssetService } from 'api/asset/asset.service';
import { userAuthDetails } from 'api/auth/types';
import { CompanyService } from 'api/users/company/company.service';
import logger from 'utils/logger';
import { getOffSet, getPaginationMetaData } from 'utils/pagination';
import { objectResponse } from 'utils/response';
import { PaginationMeta } from 'utils/types/pagination';
import { CreateSiteDto } from './dto/createSite.dto';
import Site from './models/site.model';

import Asset from 'api/asset/models/asset.model';

@Injectable()
export class SiteService {
  constructor(
    @InjectModel(Site) private readonly siteRepository: typeof Site,

    private readonly companyService: CompanyService,

    @Inject(forwardRef(() => AssetService))
    private readonly assetService: AssetService,
  ) {}

  async create(input: CreateSiteDto, user: userAuthDetails) {
    const newSite: Partial<Site> = {
      name: input.siteName,
      manager: input.siteManagerName,
      country: input.country,
      state: input.state,
      email: input.emailAddress,
      latitude: input.latitude,
      longitude: input.longitude,
      phoneNumber: input.phoneNumber,
    };

    logger.info(
      `About creating site for company with details: ${JSON.stringify(
        user.companyId,
      )}`,
    );

    const createdSite = await this.siteRepository.create(newSite);

    createdSite.set('companyId', user.companyId);
    await createdSite.save();

    logger.info('About to update company total Sites');

    const company = await this.companyService.findOne({ id: user.companyId });

    await this.companyService.update(
      { totalSites: company.totalSites + 1 },
      { id: user.companyId },
    );

    return createdSite;
  }

  async getAllSitesForCompany(
    user: userAuthDetails,
    page: number,
    perPage: number,
  ): Promise<[Site[], PaginationMeta]> {
    const offset: number = getOffSet(+page, +perPage);

    logger.info(
      `About to get all sites for a particular company with companyId: ${user.companyId}`,
    );

    const { count: totalCount, rows: sites } =
      await this.siteRepository.findAndCountAll({
        where: { companyId: user.companyId },
        ...(perPage && { limit: perPage }),
        ...(offset && { limit: offset }),
      });

    logger.info(
      `Back from getting sites for with companyId: ${user.companyId}`,
    );

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      page,
      perPage,
      sites.length,
    );

    return [sites, paginationMetaData];
  }

  async findOne(details: objectResponse): Promise<Site> {
    logger.info(
      `About to find site with details,
        ${JSON.stringify(details)}`,
    );

    const site = await this.siteRepository.findOne({
      where: {
        ...details,
      },
    });

    return site;
  }

  async getAllAssetsInSite(
    user: userAuthDetails,
    page: number,
    perPage: number,
    id: string,
  ): Promise<[Asset[], PaginationMeta]> {
    logger.info('About to start retrieving assets from site');

    const offset = getOffSet(page, perPage);

    const { rows: assets, count: totalCount } =
      await this.assetService.retrieveAssetsBySiteId(
        id,
        user.companyId,
        offset,
        perPage,
      );

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      page,
      perPage,
      assets.length,
    );

    logger.info('Completes retrieving of assets from site');

    return [assets, paginationMetaData];
  }
}
