import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Request,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  AssetOutputDto,
  AssetOutputDtoSchema,
} from 'api/asset/dto/response.dto';
import { Authorize } from 'api/auth/authority.decorator';
import { RequestWithUser } from 'api/auth/types';
import { PaginationFilter } from 'utils/pagination';
import { successResponse, successResponseSpec } from 'utils/response';
import { ResponseSchemaHelper } from 'utils/types/output-model';
import { CreateSiteDto } from './dto/createSite.dto';

import { SiteOutputDto, SiteOutputDtoSchema } from './dto/response.dto';
import { SiteService } from './site.service';

@ApiBearerAuth()
@Controller('site')
@ApiTags('Site')
export class SiteController {
  constructor(private readonly siteService: SiteService) {}

  @ApiResponse({
    status: 200,
    description: 'Retrieves all sites for a particular company',
    schema: ResponseSchemaHelper(SiteOutputDtoSchema),
  })
  @ApiOperation({ summary: 'Create a company installation site' })
  @Authorize('assets.manage')
  @Post('')
  async createSite(
    @Body() input: CreateSiteDto,
    @Request() req: RequestWithUser,
  ): Promise<successResponseSpec> {
    const site = await this.siteService.create(input, req.user);

    return successResponse('Site created successfully', 201, site);
  }

  @ApiOperation({
    summary: 'Get all installation sites for a particular company',
  })
  @ApiResponse({
    status: 200,
    description: 'Retrieves all sites for a particular company',
    schema: ResponseSchemaHelper(SiteOutputDtoSchema, 'array'),
  })
  @Authorize('assets.manage')
  @Get('')
  async getSites(
    @Request() req: RequestWithUser,
    @Query() filter?: PaginationFilter,
  ) {
    const [sites, paginationMetaData] =
      await this.siteService.getAllSitesForCompany(
        req.user,
        +filter.page,
        +filter.perPage,
      );

    const output = sites?.map((site) => SiteOutputDto.FromSiteOutput(site));

    return successResponse(
      'Sites retrieved successfully',
      200,
      output,
      true,
      paginationMetaData,
    );
  }

  @ApiOperation({
    summary: 'Retrieves all assets in a site',
  })
  @ApiResponse({
    status: 200,
    description: 'Retrieves all assets in a site',
    schema: ResponseSchemaHelper(AssetOutputDtoSchema, 'array'),
  })
  @Authorize('assets.view')
  @Get('/:id/assets')
  async getAssetsBySite(
    @Request() req: RequestWithUser,
    @Param('id') id: string,
    @Query() filter?: PaginationFilter,
  ) {
    const [assets, paginationMetaData] =
      await this.siteService.getAllAssetsInSite(
        req.user,
        +filter.page,
        +filter.perPage,
        id,
      );

    const output = assets?.map((asset) =>
      AssetOutputDto.FromAssetOutput(asset),
    );

    return successResponse(
      'Asset(s) retrieved successfully',
      200,
      output,
      true,
      paginationMetaData,
    );
  }
}
