import { Body, Controller, Get, Post, Query, Request } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Authorize } from 'api/auth/authority.decorator';

import { RequestWithUser } from 'api/auth/types';
import { PaginationFilter } from 'utils/pagination';

import { successResponse, successResponseSpec } from 'utils/response';
import { ResponseSchemaHelper } from 'utils/types/output-model';
import { CreateHardwareOrderDto } from './dtos/hardware.dto';
import {
  HardwareOrderOutputDto,
  HardwareOrderOutputDtoSchema,
  HardwareOutputDto,
  HardwareOutputDtoSchema,
} from './dtos/response.dto';
import { HardwareService } from './hardware.service';

@ApiBearerAuth()
@ApiTags('Hardware')
@Controller('hardware')
export class HardwareController {
  constructor(private readonly _hardwareService: HardwareService) {}

  @ApiOperation({
    summary: 'Retrieved Chekkit Hardwares that are for Orders/Pre-Orders',
  })
  @ApiResponse({
    status: 200,
    description: 'Retrieved hardware successfully',
    schema: ResponseSchemaHelper(HardwareOutputDtoSchema, 'array'),
  })
  @Authorize('assets.manage')
  @Get()
  async getHardWares(): Promise<successResponseSpec> {
    const hardwares = await this._hardwareService.getHardwares();

    const hardwaresOutput =
      hardwares.length > 0
        ? hardwares.map((hardware) =>
            HardwareOutputDto.FromHardwareOutput(hardware),
          )
        : [];

    return successResponse(
      'Hardware(s) retrieved successfully',
      200,
      hardwaresOutput,
    );
  }

  @ApiOperation({ summary: 'Order Chekkit Hardwares' })
  @ApiResponse({
    status: 201,
    description: 'Account created successfully',
    schema: ResponseSchemaHelper(HardwareOrderOutputDtoSchema),
  })
  @Authorize('assets.manage')
  @Post('order')
  async orderHardware(
    @Request() req: RequestWithUser,
    @Body() orderDetails: CreateHardwareOrderDto,
  ): Promise<successResponseSpec> {
    const hardwareOrdersDetails = await this._hardwareService.orderHardwares(
      req.user,
      orderDetails,
    );

    return successResponse(
      'Hardware(s) ordered successfully',
      201,
      hardwareOrdersDetails,
    );
  }

  @ApiOperation({ summary: 'Get all Company Ordered Hardwares ' })
  @ApiResponse({
    status: 201,
    description: 'Ordered hardwares retrieved successfully',
    schema: ResponseSchemaHelper(HardwareOrderOutputDtoSchema, 'array'),
  })
  @Authorize('assets.manage')
  @Get('order')
  async getOrderHardWareRequest(
    @Request() req: RequestWithUser,
    @Query() filter?: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [hardwareOrders, paginationMetaData] =
      await this._hardwareService.getOrderHardwareRequests(
        req.user,
        +filter.page,
        +filter.perPage,
      );

    const hardwareOrdersOutput = hardwareOrders?.map((hardwareOrders) =>
      HardwareOrderOutputDto.FromHardwareOrderOutput(hardwareOrders),
    );

    return successResponse(
      'Ordered hardware(s) successfully',
      201,
      hardwareOrdersOutput,
      true,
      paginationMetaData,
    );
  }
}
