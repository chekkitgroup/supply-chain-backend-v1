import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  BeforeUpdate,
  BeforeCreate,
} from 'sequelize-typescript';

@Table
class Hardware extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING(50), allowNull: false })
  name: string;

  @Column({ type: DataType.BIGINT })
  quantity: number;

  @Column({ type: DataType.BOOLEAN })
  isAvailable: boolean;

  @BeforeCreate
  @BeforeUpdate
  static assignIsAvailable(instance: Hardware) {
    instance.isAvailable = instance.quantity > 0;
  }
}

export default Hardware;
