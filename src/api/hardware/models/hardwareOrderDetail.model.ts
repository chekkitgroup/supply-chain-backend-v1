import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import Hardware from './hardware.model';
import HardwareOrder from './hardwareOrder.model';

@Table
class HardwareOrderDetail extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING(50), allowNull: false })
  name: string;

  @Column({ type: DataType.BIGINT })
  quantity: number;

  @Column({ type: DataType.FLOAT })
  unitPrice: number;

  @Column({ type: DataType.FLOAT })
  percentageDiscount: number;

  @ForeignKey(() => Hardware)
  hardwareId: string;

  @BelongsTo(() => HardwareOrder, 'hardwareOrderId')
  hardwareOrder: HardwareOrder;
}

export default HardwareOrderDetail;
