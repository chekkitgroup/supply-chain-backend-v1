import Company from 'api/users/models/company.model';
import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  BeforeUpdate,
  BeforeCreate,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import HardwareOrderDetail from './hardwareOrderDetail.model';

@Table
class HardwareOrder extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  isDelivered: boolean;

  @Column({ type: DataType.DATE, defaultValue: Date })
  order_date: Date;

  @Column({ type: DataType.DATE })
  delivery_date: Date;

  @BelongsTo(() => Company, 'companyId')
  company: Company;

  @HasMany(() => HardwareOrderDetail, 'hardwareOrderId')
  hardwareOrdersDetails: HardwareOrderDetail[];

  @BeforeCreate
  @BeforeUpdate
  static assignIsAvailable(instance: HardwareOrder) {
    instance.order_date = new Date();
  }
}

export default HardwareOrder;
