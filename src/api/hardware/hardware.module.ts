import { forwardRef, Module } from '@nestjs/common';
import { HardwareService } from './hardware.service';
import { HardwareController } from './hardware.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import HardwareOrder from './models/hardwareOrder.model';
import HardwareOrderDetail from './models/hardwareOrderDetail.model';
import Hardware from './models/hardware.model';
import { UsersModule } from 'api/users/users.module';

@Module({
  imports: [
    SequelizeModule.forFeature([Hardware, HardwareOrder, HardwareOrderDetail]),

    forwardRef(() => UsersModule),
  ],
  providers: [HardwareService],
  exports: [HardwareService],
  controllers: [HardwareController],
})
export class HardwareModule {}
