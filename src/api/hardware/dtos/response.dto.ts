import { getSchemaPath } from '@nestjs/swagger';
import Hardware from '../models/hardware.model';
import HardwareOrder from '../models/hardwareOrder.model';

export class HardwareOutputDto {
  id: string;
  name: string;
  quantity: number;
  isAvailable: boolean;
  created_at: Date;
  deleted_at: Date;
  updated_at: Date;

  static FromHardwareOutput(hardware: Hardware) {
    const output = new HardwareOutputDto();

    const exposedKeys = ['id', 'name', 'quantity', 'isAvailable'];

    exposedKeys.forEach((exposedKey) => {
      output[exposedKey] = hardware[exposedKey];
    });

    return output;
  }
}

export const HardwareOutputDtoSchema = getSchemaPath(HardwareOutputDto);

export class HardwareOrderOutputDto {
  id: string;
  isDelivered: boolean;
  order_date: Date;
  delivery_date: Date;
  hardwareOrdersDetails: HardwareOrdersDetailsOutputDto[];

  static FromHardwareOrderOutput(hardwareOrder: HardwareOrder) {
    const output = new HardwareOrderOutputDto();

    const exposedKeys = [
      'id',
      'isDelivered',
      'order_date',
      'delivery_date',
      'hardwareOrdersDetails',
    ];

    exposedKeys.forEach((exposedKey) => {
      output[exposedKey] = hardwareOrder[exposedKey];
    });

    return output;
  }
}

export const HardwareOrderOutputDtoSchema = getSchemaPath(
  HardwareOrderOutputDto,
);

export class HardwareOrdersDetailsOutputDto {
  id: string;
  name: string;
  quantity: number;
}
