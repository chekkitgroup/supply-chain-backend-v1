import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';

export class hardwareOrderDto {
  @ApiProperty({ example: 'Chekkit RFID' })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ example: 10 })
  @IsNumber()
  @IsNotEmpty()
  quantity: number;
}

export class CreateHardwareOrderDto {
  @ApiProperty({
    description: 'hardware creation request',
    type: [hardwareOrderDto],
  })
  @ValidateNested({ each: true })
  @IsArray()
  @Type(() => hardwareOrderDto)
  hardwares: [hardwareOrderDto];
}
