export const hardWares = [
  {
    name: 'Chekkit RFID',
    quantity: 210,
  },
  {
    name: 'Sticker Printer',
    quantity: 320,
  },
  {
    name: 'Sticker Label',
    quantity: 330,
  },
];
