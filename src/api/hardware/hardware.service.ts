import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { userAuthDetails } from 'api/auth/types';
import { CompanyService } from 'api/users/company/company.service';
import { hardwareOrderEmailTemplate } from 'utils/constant';
import sendEmail from 'utils/email';
import logger from 'utils/logger';
import { getOffSet, getPaginationMetaData } from 'utils/pagination';
import { PaginationMeta } from 'utils/types/pagination';
import { CreateHardwareOrderDto } from './dtos/hardware.dto';
import Hardware from './models/hardware.model';
import HardwareOrder from './models/hardwareOrder.model';
import HardwareOrderDetail from './models/hardwareOrderDetail.model';
import { hardwareEmailDetails } from './types';

@Injectable()
export class HardwareService {
  constructor(
    @InjectModel(Hardware) private readonly hardwareRepository: typeof Hardware,

    @InjectModel(HardwareOrder)
    private readonly hardwareOrderRepository: typeof HardwareOrder,

    @InjectModel(HardwareOrderDetail)
    private readonly hardwareOrderDetailRepository: typeof HardwareOrderDetail,

    private readonly companyService: CompanyService,
  ) {}

  async getHardwares(): Promise<Hardware[]> {
    const hardwares = await this.hardwareRepository.findAll();

    return hardwares;
  }

  async orderHardwares(
    user: userAuthDetails,
    orderDetails: CreateHardwareOrderDto,
  ): Promise<HardwareOrderDetail[]> {
    logger.info(
      `About creating hardwares Order with order details: ${JSON.stringify(
        orderDetails,
      )}`,
    );

    const date = new Date();
    date.setHours(date.getHours() + 72);

    const hardwaresOrder = (await this.hardwareOrderRepository.create({
      delivery_date: date,
    })) as any;

    logger.info(
      `About setting company id for newly created order  order details: ${JSON.stringify(
        hardwaresOrder,
      )}`,
    );

    await hardwaresOrder.setCompany(user.companyId);

    const hardwareOrderData = orderDetails.hardwares.map(
      ({ name, quantity }) => {
        const newhardwareOrderDetail = {
          name,
          quantity,
        };

        const hardwareOrderDetail = this.hardwareOrderDetailRepository.create(
          newhardwareOrderDetail,
        );

        return hardwareOrderDetail;
      },
    );

    logger.info(
      `About inserting into hardware_order_details table for hardware order with details: ${JSON.stringify(
        hardwaresOrder,
      )}`,
    );

    const newhardwareOrderDetails = (await Promise.all(
      hardwareOrderData,
    )) as any;

    logger.info(
      `About setting into hardwareOrder id table for newly inserted hardwareOrderDetails with details: ${JSON.stringify(
        newhardwareOrderDetails,
      )}`,
    );

    await Promise.all(
      newhardwareOrderDetails.map((newhardwareOrderDetail: any) => {
        newhardwareOrderDetail.setHardwareOrder(hardwaresOrder);
      }),
    );

    const newOrderDetails: hardwareEmailDetails = {
      newhardwareOrderDetails,
      email: user.email,
      name: user.name,
      ref: `CHK-${newhardwareOrderDetails[0].hardwareOrderId}-HDW`,
    };

    this.sendHardwareOrderEmail(newOrderDetails);

    const company = await this.companyService.findOne({ id: user.companyId });

    company.set(
      'totalHardwares',
      company.totalHardwares + orderDetails.hardwares.length,
    );

    await company.save();

    return newhardwareOrderDetails;
  }

  async sendHardwareOrderEmail(
    emailDetails: hardwareEmailDetails,
  ): Promise<boolean> {
    const emailContent = hardwareOrderEmailTemplate(
      emailDetails.name,
      emailDetails.ref,
    );

    logger.info(
      `About sending email for hardware Order with email details :${JSON.stringify(
        emailDetails,
      )}`,
    );
    const emailSent = await sendEmail(
      emailDetails.email,
      'Chekkit Supply Chain Hardware Order',
      emailContent,
    );

    logger.info(
      `Back from sendEmail for hardwareOrder with  response : ${emailSent}`,
    );
    return emailSent;
  }

  async getOrderHardwareRequests(
    user: userAuthDetails,
    page: number,
    perPage: number,
  ): Promise<[HardwareOrder[], PaginationMeta]> {
    const offset = getOffSet(+page, +perPage);

    logger.info(
      `About to get hardware Orders for a particular company with companyId: ${user.companyId}`,
    );

    const { count: totalCount, rows: hardwareOrders } =
      await this.getOrderedHardwares(user.companyId, +perPage, offset);

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      +page,
      +perPage,
      hardwareOrders.length,
    );

    return [hardwareOrders, paginationMetaData];
  }

  async getOrderedHardwares(
    companyId: string,
    perPage: number,
    offset: number,
  ) {
    return await this.hardwareOrderRepository.findAndCountAll({
      distinct: true,

      where: {
        companyId,
      },

      include: ['hardwareOrdersDetails'],

      ...(perPage && { limit: perPage }),
      ...(offset && { limit: offset }),
    });
  }

  async countAll(companyId: string) {
    const totalHardwares = await this.hardwareOrderRepository.count({
      where: {
        companyId,
      },
    });

    return totalHardwares;
  }
}
