import HardwareOrderDetail from '../models/hardwareOrderDetail.model';

export interface hardwareEmailDetails {
  newhardwareOrderDetails: HardwareOrderDetail[];
  email: string;
  name: string;
  ref: string;
}
