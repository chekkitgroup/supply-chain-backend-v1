import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Request,
  Put,
  Param,
  ParseUUIDPipe,
  Sse,
} from '@nestjs/common';

import { interval, Observable, defer } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import { Authorize } from 'api/auth/authority.decorator';
import { RequestWithUser } from 'api/auth/types';
import { SurveyOutputDto } from 'api/survey/dto/response.dto';

import { PaginationFilter } from 'utils/pagination';
import { successResponse, successResponseSpec } from 'utils/response';
import { ResponseSchemaHelper } from 'utils/types/output-model';
import { AssetService } from './asset.service';

import {
  AssetProgressFilter,
  AssetStatusUpdateDto,
  CreateAssetDetailsDto,
  GatewayDto,
  ListAssetLocationFilter,
  MoveAssetDto,
  MoveAssetToWarehouseDto,
  StockCountDTO,
} from './dto/index.dto';

import {
  AssetOutputDto,
  AssetOutputDtoSchema,
  AssetTypeOutputDtoSchema,
  OnboardingRequestOutputDtoSchema,
  OnboardingRequestOutputoDto,
} from './dto/response.dto';
import { configuration } from '../../config';

@ApiBearerAuth()
@Controller('asset')
@ApiTags('Asset')
export class AssetController {
  constructor(private readonly assetService: AssetService) {}

  @ApiResponse({
    status: 200,
    description: 'Retrieves all assets for a particular company',
    schema: ResponseSchemaHelper(AssetOutputDtoSchema, 'array'),
  })
  @ApiOperation({
    summary: 'Onboard Company Assets',
  })
  @Authorize('assets.manage')
  @Post()
  async onBoardAsset(
    @Body() input: CreateAssetDetailsDto,
    @Request() req: RequestWithUser,
  ): Promise<successResponseSpec> {
    await this.assetService.onBoard(input, req.user);

    return successResponse('Asset(s) submitted successfully', 200);
  }



  @ApiResponse({
    status: 200,
    description: 'Create asset status update',
  })
  @ApiOperation({
    summary: 'Asset status successfully created',
  })
  @Authorize('assets.manage')
  @Post('asset-status')
  async createAssetUpdate(
    @Body() data: AssetStatusUpdateDto,
    @Request() req: RequestWithUser,
  ) {
    await this.assetService.createAssetStatusUpdate(data, req.user);
    return successResponse('Asset status updated created successfully', 200);
  }


  @ApiResponse({
    status: 200,
    description: 'Retrieves all assets for a particular company',
    schema: ResponseSchemaHelper(AssetOutputDtoSchema, 'array'),
  })

  
  @ApiOperation({
    summary: 'Get all onboarded assets for a particular company',
  })
  @Authorize('assets.view')
  @Get()
  async getOnBoardAssets(
    @Request() req: RequestWithUser,
    @Query() filter?: PaginationFilter,
  ) {
    const [assets, paginationMetaData] = await this.assetService.getAllAssets(
      req.user,
      +filter.page,
      +filter.perPage,
    );

    const output = assets?.map((asset) =>
      AssetOutputDto.FromAssetOutput(asset),
    );

    return successResponse(
      'Asset(s) retrieved successfully',
      200,
      output,
      true,
      paginationMetaData,
    );
  }

  
  @ApiOperation({
    summary: 'Get asset details with rfid data',
  })
  @Authorize('assets.view')
  @Get('detail/:id/')
  async getAsset(
    @Request() req: RequestWithUser,
    @Param('id') id: string,
  ) {
    const asset = await this.assetService.getAsset(
      id,
      req.user
    );

    return successResponse(
      'Asset retrieved successfully',
      200,
      asset,
      true
    );
  }

  @ApiOperation({
    summary: 'Move Asset to Site',
  })
  @ApiResponse({
    status: 200,
    description: 'Move asset to site successfully',
  })
  @Authorize('assets.manage')
  @Put('/site/:id/move')
  async moveAssetToSite(
    @Request() req: RequestWithUser,
    @Param('id') siteId: string,
    @Body() input: MoveAssetDto,
  ) {
    await this.assetService.moveAssetToSite(req.user, input, siteId);

    return successResponse('Site movement submitted successfully', 200);
  }

  @ApiOperation({
    summary: 'Move Asset to Warehouse',
  })
  @ApiResponse({
    status: 200,
    description: 'Move asset to warehouse successfully',
  })
  @Authorize('assets.manage')
  @Put('/warehouse/:id/move')
  async moveAssetToWarehouse(
    @Request() req: RequestWithUser,
    @Param('id') siteId: string,
    @Body() input: MoveAssetToWarehouseDto,
  ) {
    await this.assetService.moveAssetToWarehouse(req.user, input, siteId);

    return successResponse('Assets moved to warehouse successfully', 200);
  }

  @ApiResponse({
    status: 200,
    description: 'Retrieves all asset types for a particular company',
    schema: ResponseSchemaHelper(AssetTypeOutputDtoSchema, 'array'),
  })
  @ApiOperation({
    summary: 'Get all onboarded asset types for a particular company',
  })
  @Authorize('assets.view')
  @Get('type')
  async getOnBoardedAssetByType(
    @Request() req: RequestWithUser,
    @Query() filter?: PaginationFilter,
  ) {
    const [assets, paginationMetaData] =
      await this.assetService.getAllAssetsByType(
        req.user,
        +filter.page,
        +filter.perPage,
      );

    return successResponse(
      'Asset(s) retrieved successfully',
      200,
      assets,
      true,
      paginationMetaData,
    );
  }

  @ApiResponse({
    status: 200,
    description: 'Retrieves all stock count report',
  })
  @ApiOperation({
    summary: 'Retrieves all stock count report',
  })
  @Authorize('assets.view')
  @Get('stock-count-history')
  async getStockCountReport(@Request() req: RequestWithUser) {
    const results = await this.assetService.getStockCountHistory(req.user);

    return successResponse(
      'Stock Count retrieved successfully',
      200,
      results,
      true,
    );
  }

  @ApiResponse({
    status: 200,
    description: 'Retrieves stock count batch report',
  })

  @ApiOperation({
    summary: 'Retrieves stock count batch report',
  })
  @Authorize('assets.view')
  @Get('stock-count-history/:countBatch')
  async getStockCountBatch(
    @Param('countBatch', ParseUUIDPipe) countBatch: string,
  ) {
    const results = await this.assetService.getStockCountBatch(countBatch);

    return successResponse(
      'Stock Count retrieved successfully',
      200,
      results,
      true,
    );
  }

  @ApiResponse({
    status: 200,
    description: 'Retrieves asset onboarding progress for a particular company',
    schema: ResponseSchemaHelper(OnboardingRequestOutputDtoSchema, 'array'),
  })


  @ApiOperation({
    summary: 'Get all asset onboarding progress for a particular company',
  })
  @Authorize('assets.view')
  @Get('progress')
  async getAssetOnBoardingProgress(
    @Request() req: RequestWithUser,
    @Query() filter?: AssetProgressFilter,
  ) {

    console.log('filter>>', filter)
    const [results, paginationMetaData] =
      await this.assetService.getOnboardingRequests(req.user, filter);

    const output = results?.map((result) =>
      OnboardingRequestOutputoDto.fromAssetOnboardingRequest(result),
    );

    return successResponse(
      'Asset(s) onboarding progress fetched successfully',
      200,
      output,
      true,
      paginationMetaData,
    );
  }

  @ApiResponse({
    status: 200,
    description: 'Retrieves completed asset for a particular company',
    schema: ResponseSchemaHelper(AssetOutputDtoSchema, 'array'),
  })
  @ApiOperation({
    summary: 'Retrieves completed asset for a particular company',
  })
  @Authorize('assets.view')
  @Get('completed/:id')
  async getCompletedOnboardingAssets(
    @Request() req: RequestWithUser,
    @Param('id') requestId: string,
  ) {
    const results = await this.assetService.getCompletedOnboardingAssets(
      req.user,
      requestId,
    );

    const output = results?.map((result) =>
      AssetOutputDto.FromAssetOutput(result),
    );

    return successResponse('Asset(s) fetched successfully', 200, output);
  }

  @ApiResponse({
    status: 200,
    description: 'Retrieves assets by survey Id for a company',
    schema: ResponseSchemaHelper(OnboardingRequestOutputDtoSchema, 'array'),
  })
  @ApiOperation({
    summary: 'Retrieves assets by survey Id for a company',
  })
  @Authorize('surveys.view')
  @Get('/:id/surveys')
  async fetchSurveyByAssetId(
    @Request() req: RequestWithUser,
    @Param('id') assetId: string,
  ) {
    const survey = await this.assetService.fetchSurveysByAsset(
      req.user,
      assetId,
    );

    const output = SurveyOutputDto.FromSurveyOutput(survey);

    return successResponse('Survey fetched successfully', 200, output);
  }

  @ApiResponse({
    status: 200,
    description: 'Assign RFID tags to assets',
  })
  @ApiOperation({
    summary: 'Assign Rfid tags to assets',
  })
  @Authorize('assets.manage')
  @Put(':id/rfid/:rfidId')
  async assignRFIDTagToAsset(
    @Request() req: RequestWithUser,
    @Param('id') assetId: string,
    @Param('rfidId') rfidId: string,
  ) {
    await this.assetService.attachRFDTagtoAssets(req.user, assetId, rfidId);

    return successResponse('RFID tag attached to asset successfully', 200);
  }

  @ApiResponse({
    status: 200,
    description: 'Bulk create stock count',
  })
  @ApiOperation({
    summary: 'Stock count successfully created',
  })
  @Authorize('assets.manage')
  @Post('stock-count')
  async createStockCount(
    @Body() data: StockCountDTO,
    @Request() req: RequestWithUser,
  ) {
    await this.assetService.createStockCount(data, req.user);
    return successResponse('Stock count created successfully', 200);
  }

  @ApiResponse({
    status: 200,
    description: 'Gateway provision for asset already',
  })
  @ApiOperation({
    summary: "Provision Gateway for reading for uploading asset's device data",
  })
  // @Authorize('assets.manage')
  @Post('provision-gateway')
  async provisionGateway(
    @Request() req: RequestWithUser,
    @Body() input: GatewayDto,
  ) {
    await this.assetService.provisionGateway(req.user, input);

    return successResponse('Gateway provisioned successfully', 200);
  }

  @ApiResponse({
    status: 200,
    description: 'Retrieve asset data for TNT',
  })
  @ApiOperation({
    summary: 'Server sent event for real time device data',
  })
  @Authorize('assets.manage', 'assets.view')
  @Sse(':id/location')
  async RecentLocationData(
    @Request() req: RequestWithUser,
    @Param('id') resourceId: string,
    @Query() filter: ListAssetLocationFilter,
  ): Promise<Observable<any>> {
    const recentAssetLocationData$ = defer(() =>
      this.assetService.getRecentLocationAssetData(
        req.user.companyId,
        resourceId,
        filter,
      ),
    ).pipe(
      map((data) => ({
        data,
      })),
    );

    return interval(configuration().tagAndTrac.interval).pipe(
      switchMap(() => recentAssetLocationData$),
    );
  }

  //==================================//
  // Agent Mobile App Apis           //
  //================================//
  @ApiResponse({
    status: 200,
    description: 'Retrieves agent dashboard stats',
  })

  @ApiOperation({
    summary: 'Retrieves agent dashboard stats',
  })
  @Authorize('assets.view')
  @Get('agent-dashboard-stats')
  async getAgentDashboardStats(@Request() req: RequestWithUser) {
    const results = await this.assetService.getAgentDashboardStats(req.user);

    return successResponse(
      'Agent stats retrieved successfully',
      200,
      results,
      true,
    );
  }
  @ApiResponse({
    status: 200,
    description: 'Retrieves all assets',
  })

  @ApiOperation({
    summary: 'Retrieves all assets',
  })
  @Authorize('assets.view')
  @Get('agent-assets')
  async getAllAssetsAgent(
    @Request() req: RequestWithUser,
    @Query() filter?: PaginationFilter,
  ) {
    const [assets, paginationMetaData] = await this.assetService.getAllAssetsAgent(
      req.user,
      +filter.page,
      +filter.perPage,
    );

    const output = assets?.map((asset) =>
      AssetOutputDto.FromAssetOutput(asset),
    );

    return successResponse(
      'Asset(s) retrieved successfully',
      200,
      output,
      true,
      paginationMetaData,
    );
  }

  
  
}
