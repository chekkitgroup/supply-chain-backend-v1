import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import logger from 'utils/logger';
import { RedisService } from 'utils/redis';
import { AssetService } from './asset.service';

@Injectable()
export class AssetScheduleService {
  constructor(
    private readonly assetService: AssetService,
    private readonly redisService: RedisService,
  ) {}

  // every 15 minutes
  @Cron('0 */20 * * * *')
  async completeAssetOnboardingViaTagAndTrac() {
    const lock = await this.redisService.acquireResource(
      'pending_asset_onboarding',
    );

    try {
      logger.info('Start pending asset onboarding');

      await this.assetService.completeAssetOnboarding();

      logger.info('completes pending asset onboarding');
    } catch (error) {
      logger.error(
        `An error occurred while complete asset onboarding ${error.message}`,
      );
    } finally {
      try {
        if (lock) {
          await lock.unlock();

          logger.info('lock released for pending_assets_onboarding');
        }
      } catch (err) {}
    }
  }
}
