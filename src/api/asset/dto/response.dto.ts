import { getSchemaPath } from '@nestjs/swagger';
import { WarehouseOutputDto } from 'api/warehouse/dtos/response.dto';
import AssetOnboardingRequest from '../models/asset-onboarding-request.model';
import Asset from '../models/asset.model';
import Pin from '../models/pin.model';
import { PackageLevel } from '../types';

export class PinOutputDto {
  id: string;
  value: number;
  isAssigned: boolean;
  assetId: string;

  static FromPinOutput(pin: Pin) {
    const output = new PinOutputDto();
    const exposedkeys = ['value'];

    exposedkeys.forEach((key) => {
      output[key] = pin[key];
    });

    return output;
  }
}

export class AssetOutputDto {
  id: string;
  name: string;
  price: number;
  serialNumber: string;
  batchNumber: string;
  expiryDate: Date;
  category: string;
  macAddress: string;
  packageLevel: PackageLevel;
  numberOfSubUnits: number;
  stickerId: PinOutputDto;
  warehouse: WarehouseOutputDto;
  created_at: Date;
  deleted_at: Date;
  updated_at: Date;

  static FromAssetOutput(asset: Asset) {
    const output = new AssetOutputDto();

    const exposedkeys = [
      'id',
      'name',
      'price',
      'serialNumber',
      'batchNumber',
      'macAddress',
      'expiryDate',
      'category',
      'packageLevel',
      'numberOfSubUnits',
      'stickerId',
      'warehouse',
      'created_at',
    ];

    exposedkeys.forEach((key) => {
      if (key === 'stickerId' && asset.stickerId) {
        output[key] = PinOutputDto.FromPinOutput(asset.stickerId);

        return;
      }

      if (key === 'warehouse' && asset.warehouse) {
        output[key] = WarehouseOutputDto.FromWarehouseOutput(asset.warehouse);

        return;
      }

      output[key] = asset[key];
    });

    return output;
  }
}

export const AssetOutputDtoSchema = getSchemaPath(AssetOutputDto);

export class AssetTypeOutputDto {
  name: string;

  static FromAssetOutput(asset: Asset) {
    const output = new AssetOutputDto();

    const exposedkeys = ['name'];

    exposedkeys.forEach((key) => {
      output[key] = asset[key];
    });

    return output;
  }
}

export const AssetTypeOutputDtoSchema = getSchemaPath(AssetTypeOutputDto);

export class OnboardingRequestOutputoDto {
  id: string;
  status: AssetOnboardingRequest;
  created_at: string;
  assets: AssetOutputDto[] = [];

  static fromAssetOnboardingRequest(
    assetsOnboardingRequest: AssetOnboardingRequest,
  ) {
    const output = new OnboardingRequestOutputoDto();

    const exposedkeys = ['assets', 'id', 'status', 'created_at'];

    exposedkeys.forEach((key) => {
      if (key === 'assets' && assetsOnboardingRequest.assets) {
        assetsOnboardingRequest.assets.map((asset) => {
          output.assets.push(AssetOutputDto.FromAssetOutput(asset));
        });

        return;
      }

      output[key] = assetsOnboardingRequest[key];
    });

    return output;
  }
}

export const OnboardingRequestOutputDtoSchema = getSchemaPath(
  OnboardingRequestOutputoDto,
);
