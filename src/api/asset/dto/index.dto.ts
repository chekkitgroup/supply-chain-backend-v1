import { ApiProperty } from '@nestjs/swagger';

import { Type } from 'class-transformer';

import {
  IsNotEmpty,
  IsString,
  IsNumber,
  IsEnum,
  ValidateNested,
  IsArray,
  IsDateString,
  IsOptional,
  ValidateIf,
  IsUUID,
} from 'class-validator';
import { PaginationFilter } from 'utils/pagination';

import { AssetUpdateStatus, ListTypeLocation, PackageLevel } from '../types';

export class CreateSurveyDto {
  @ApiProperty()
  title: string;

  @ApiProperty()
  content: string;

  numberOfQuestions: number;

  @ValidateNested({ each: true })
  @IsArray()
  @Type(() => CreateSurveyQuestionDto)
  surveyQuestions: CreateSurveyQuestionDto[];
}

export class CreateSurveyQuestionDto {
  @ApiProperty()
  question: string;

  @ApiProperty()
  @IsString({ each: true })
  choices: string[];
}

export class OnboardAssetDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  price: number;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  serialNumber: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  batchNumber: string;

  @ApiProperty({ example: '2020-04-20' })
  @IsString()
  expiryDate: Date;

  @ApiProperty()
  @IsString()
  @IsOptional()
  warehouseId?: string;

  @ApiProperty()
  @IsString()
  category?: string;

  @ValidateIf(
    (dto: OnboardAssetDto) => dto.packageLevel !== PackageLevel.PRIMARY,
  )
  @ApiProperty()
  @IsNumber()
  numberOfSubUnits?: number;

  @ApiProperty()
  @IsString()
  @IsOptional()
  macAddress: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  surveyId?: string;

  @ApiProperty({ example: PackageLevel.PRIMARY })
  @IsEnum(PackageLevel)
  packageLevel?: PackageLevel;
}

export class CreateStickerDto {
  @ApiProperty()
  @IsString()
  description: string;

  @ApiProperty()
  image: string;
}
export class CreateAssetPhotoDto {
  @ApiProperty()
  image: string;
}

export class CreateAssetDetailsDto {
  @IsOptional()
  survey?: CreateSurveyDto;

  @IsOptional()
  surveyId?: string;

  @IsOptional()
  sticker?: CreateStickerDto;

  @IsOptional()
  photo?: CreateAssetPhotoDto;

  @ValidateNested({ each: true })
  @IsArray()
  @Type(() => OnboardAssetDto)
  assetDetails: OnboardAssetDto[];
}

export class MoveAssetDetailsDto {
  @ValidateIf(
    (dto: MoveAssetDetailsDto) =>
      dto.packageLevel !== PackageLevel.PRIMARY && dto.numberOfMovedUnits > 0,
  )
  @ApiProperty()
  @IsNumber()
  numberOfMovedUnits: number;

  @ValidateIf((dto: MoveAssetDetailsDto) => dto.numberOfMovedUnits > 0)
  @ValidateNested({ each: true })
  @IsArray()
  @Type(() => OnboardAssetDto)
  assetDetails: OnboardAssetDto[];

  @ApiProperty({ example: PackageLevel.PRIMARY })
  @IsEnum(PackageLevel)
  packageLevel?: PackageLevel;

  @ApiProperty()
  @IsString()
  assetId: string;
}

export class MoveAssetDto {
  @ValidateNested({ each: true })
  @IsArray()
  @Type(() => MoveAssetDetailsDto)
  moveAssetDetails: MoveAssetDetailsDto[];
}

export class MoveAssetToWarehouseDto {
  @ApiProperty()
  @IsString({ each: true })
  assetIds: string[];
}

export class AssetProgressFilter extends PaginationFilter {
  @ApiProperty({ example: '', required: false })
  @IsString()
  @IsOptional()
  requestId?: string;
}

export class StockCount {
  @ApiProperty()
  @IsUUID()
  id: string;

  @ApiProperty()
  @IsNumber()
  count: number;

  @ApiProperty()
  @IsOptional()
  @IsString()
  comment: string;
}

export class StockCountDTO {
  @IsArray()
  @Type(() => StockCount)
  records: StockCount[];
}

export class GatewayDto {
  @ApiProperty()
  @IsString()
  gatewayType: string;

  @ApiProperty()
  @IsString()
  location: string;

  @ApiProperty()
  @IsString()
  longitude: string;

  @ApiProperty()
  @IsString()
  latitude: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  projectId?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  companyId?: string;

  @ApiProperty()
  @IsString({ each: true })
  deviceIds: string[];
}

export class AssetStatusUpdateDto {
  @ApiProperty()
  @IsString()
  status: AssetUpdateStatus;

  @ApiProperty()
  @IsString()
  comment?: string;

  @ApiProperty()
  @IsString()
  reporterId: string;

  @ApiProperty()
  @IsString()
  assetId: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  companyId?: string;

}

export class ListAssetLocationFilter {
  @ApiProperty({ example: ListTypeLocation.Warehouse })
  @IsEnum(ListTypeLocation)
  type: ListTypeLocation;
}
