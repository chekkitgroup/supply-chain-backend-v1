import { Injectable } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { setQueues, BullAdapter } from 'bull-board';
import { ONBOARD_ASSETS } from './constant';

@Injectable()
export class QueueUIProvider {
  constructor(@InjectQueue(ONBOARD_ASSETS) private readonly queueOne: Queue) {
    setQueues([new BullAdapter(queueOne)]);
  }
}
