import { forwardRef, Module } from '@nestjs/common';

import { AssetService } from './asset.service';
import { AssetController } from './asset.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import Asset from './models/asset.model';
import { SurveyModule } from 'api/survey/survey.module';

import { BullModule } from '@nestjs/bull';
import { ONBOARD_ASSETS } from 'api/asset/constant';
import { AssetConsumerService } from './asset.consumer';
import { UtilsModule } from 'utils/utils.module';
import Pin from './models/pin.model';
import { SiteModule } from 'api/site/site.module';
import { WareHouseModule } from 'api/warehouse/warehouse.module';
import { UsersModule } from 'api/users/users.module';
import { QueueUIProvider } from './queue.provider';
import AssetOnboardingRequest from './models/asset-onboarding-request.model';
import { RfidModule } from 'api/rfid/rfid.module';

import { TagAndTracService } from './apps/tagAndTrac.service';
import StockCountHistory from './models/stock-count-history.model';
import { AssetScheduleService } from './asset.schedule';
import WareHouse from 'api/warehouse/models/warehouse.model';
import Site from 'api/site/models/site.model';
import AssetStatusUpdate from './models/asset-status-updates.model';

@Module({
  imports: [
    SequelizeModule.forFeature([
      AssetStatusUpdate,
      Asset,
      WareHouse,
      AssetOnboardingRequest,
      Pin,
      Site,
      StockCountHistory,
    ]),
    forwardRef(() => SurveyModule),

    BullModule.registerQueue({
      name: ONBOARD_ASSETS,
    }),

    UtilsModule,
    SiteModule,
    WareHouseModule,

    forwardRef(() => UsersModule),

    RfidModule,
  ],
  providers: [
    AssetService,
    AssetConsumerService,
    QueueUIProvider,
    TagAndTracService,
    AssetScheduleService,
  ],
  exports: [AssetService, TagAndTracService],
  controllers: [AssetController],
})
export class AssetModule {}
