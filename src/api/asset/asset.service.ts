import { InjectQueue } from '@nestjs/bull';
import {
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { Op } from 'sequelize';
import { InjectModel } from '@nestjs/sequelize';
import { v4 as uuidv4 } from 'uuid';
import { userAuthDetails } from 'api/auth/types';

import { SurveyService } from 'api/survey/survey.service';
import { Queue } from 'bull';

import logger from 'utils/logger';
import { getOffSet, getPaginationMetaData } from 'utils/pagination';
import { PaginationMeta } from 'utils/types/pagination';
import Asset from './models/asset.model';
import { ONBOARD_ASSETS } from './constant';
import { SiteService } from 'api/site/site.service';
import { AwsUploadService } from 'utils/uploader';
import {
  ActiveAssetData,
  AssetOnbaordingViaTNTDto,
  AssetOnboardingRequestWithExtension,
  AssetOnboardingStatus,
  AssetUpdateStatus,
  AssetWithRequest,
  IAssetData,
  IPackageLevelCatgory,
  ListTypeLocation,
  OnboardingStatus,
  PackageLevel,
  UploaderResponse,
} from './types';
import { StockCountFilter } from 'api/analytics/dto';
import sequelize from 'sequelize';
import {
  AssetProgressFilter,
  AssetStatusUpdateDto,
  CreateAssetDetailsDto,
  CreateSurveyDto,
  GatewayDto,
  ListAssetLocationFilter,
  MoveAssetDetailsDto,
  MoveAssetDto,
  MoveAssetToWarehouseDto,
  OnboardAssetDto,
  StockCountDTO,
} from './dto/index.dto';
import { WareHouseService } from 'api/warehouse/warehouse.service';
import Survey from 'api/survey/models/survey.model';
import AssetOnboardingRequest from './models/asset-onboarding-request.model';
import { RfidService } from 'api/rfid/rfid.service';
import { CompanyService } from 'api/users/company/company.service';
import { TagAndTracService } from './apps/tagAndTrac.service';
import Pin from './models/pin.model';
import { HTTPREQUESTMETHOD } from './apps/types';

import StockCountHistory from './models/stock-count-history.model';
import { Sequelize } from 'sequelize-typescript';
import { UtilsService } from 'utils/utils.service';
import Warehouse from '../warehouse/models/warehouse.model';
import Site from 'api/site/models/site.model';
import { IDeviceDetailsResponse } from 'api/rfid/types';
import AssetStatusUpdate from './models/asset-status-updates.model';

@Injectable()
export class AssetService {
  constructor(
    @InjectModel(Asset) private readonly assetRepository: typeof Asset,
    @InjectModel(Site) private readonly siteRepository: typeof Site,
    @InjectModel(Warehouse)
    private readonly warehouseRepository: typeof Warehouse,

    @InjectModel(Pin) private readonly pinRepository: typeof Pin,

    @InjectModel(AssetOnboardingRequest)
    private readonly assetOnboardingRequestRepository: typeof AssetOnboardingRequest,

    @InjectModel(AssetStatusUpdate)
    private readonly assetStatusUpdateRepository: typeof AssetStatusUpdate,

    @InjectModel(StockCountHistory)
    private readonly stockCountHistoryRequestRepository: typeof StockCountHistory,

    @Inject(forwardRef(() => SurveyService))
    private readonly surveyService: SurveyService,

    private readonly siteService: SiteService,

    private readonly warehouseService: WareHouseService,

    private readonly rfidService: RfidService,

    private readonly awsUploadService: AwsUploadService,

    private readonly utilsService: UtilsService,

    private readonly assetService: AssetService,

    private readonly tagAndTracService: TagAndTracService,

    private readonly companyService: CompanyService,

    @InjectQueue(ONBOARD_ASSETS) private assetQueue: Queue,
  ) {}

  async onBoard(input: CreateAssetDetailsDto, user: userAuthDetails) {
    let survey: Survey;
    let surveyId = null;
    let firstAsset = input.assetDetails?.[0];

    // console.log('-------======--------', input.assetDetails[0].surveyId)
    // console.log('-------====input==--------', input)
    // return 

    
    if (firstAsset && firstAsset.surveyId) {
      surveyId = firstAsset.surveyId;
    } else if (input.survey) {
      const surveyDetails: CreateSurveyDto = input.survey;

      if (surveyDetails) {
        survey = await this.surveyService.create(surveyDetails, user);
      }
    }

    // console.log('-------======--------', surveyId)
    // return

    logger.info(`About to create survey for asset with details`);

    let stickerPhoto: Partial<UploaderResponse> = { description: '' };
    let assetPhoto: Partial<UploaderResponse> = { description: '' };

    if (input.sticker?.image) {
      stickerPhoto = await this.awsUploadService.base64Upload(
        input.sticker.image,
        input.sticker.description,
      );
    }
    if (input.photo?.image) {

      const uid = uuidv4();
      let photoName  =  uid + '-asset'
      assetPhoto = await this.awsUploadService.base64Upload(
        input.photo.image,
        photoName,
      );
      // console.log('-------====input==--------', assetPhoto)

    }

    // return


    stickerPhoto.description = input.sticker?.description;

    await this.onboardAsset(
      input.assetDetails,
      user,
      stickerPhoto,
      assetPhoto,
      survey,
      surveyId,
    );
  }

  private async onboardAsset(
    assetDetailsToOnboard: OnboardAssetDto[],
    user: userAuthDetails,
    stickerPhoto?: Partial<UploaderResponse>,
    assetPhoto?: Partial<UploaderResponse>,
    survey?: Survey,
    surveyid?: string,
  ) {

    const assetDetails = await Promise.all(
      assetDetailsToOnboard.map(async (assetDetail) => {
        const asset = await this.assetService.findOne({
          serialNumber: assetDetail.serialNumber,
          companyId: user.companyId,
        });

        if (asset) {
          throw new UnprocessableEntityException(
            'One of your asset(s) serial number already exist',
          );
        }
        return {
          name: assetDetail.name,
          price: assetDetail.price,
          serialNumber: assetDetail.serialNumber,
          batchNumber: assetDetail.batchNumber,
          expiryDate: assetDetail.expiryDate,
          category: assetDetail.category,
          photo: assetPhoto?.Location,
          numberOfSubUnits: assetDetail.numberOfSubUnits,
          macAddress: assetDetail.macAddress,
          packageLevel: assetDetail.packageLevel,
          stickerDescription: stickerPhoto?.description,
          stickerImage: stickerPhoto?.Location,
          warehouseId: assetDetail.warehouseId,
          surveyId: surveyid ? surveyid : survey?.id,
        };
      }),
    );

    logger.info('About to log asset onboarding request');

    const requestDetails: Partial<AssetOnboardingRequestWithExtension> = {
      status: AssetOnboardingStatus.PROCESSNG,
      companyId: user.companyId,
      userId: user.id,
    };

    const onboardedRequestLog = await this.logAssetOnboardingRequest(
      requestDetails,
    );
    
    // console.log('-------======--------',  {
    //   // surveyId: surveyid ? surveyid : survey?.id,
    //   // companyId: user.companyId,
    //   // requestId: onboardedRequestLog.id,
    //   // assetDetails,
    // })
    // return
    await this.assetQueue.add(
      {
        surveyId: surveyid ? surveyid : survey?.id,
        companyId: user.companyId,
        requestId: onboardedRequestLog.id,
        assetDetails,
      },
      { delay: 5000 },
    );
  }

  async getAllAssets(
    user: userAuthDetails,
    page: number,
    perPage: number,
  ): Promise<[Asset[], PaginationMeta]> {
    const offset = getOffSet(page, perPage);

    logger.info(
      `About to get all assets for a particular company with companyId: ${user.companyId}`,
    );

    const { count: totalCount, rows: assets } = await this.getCompanyAssets(
      user.companyId,
      perPage,
      offset,
      ['stickerId', 'warehouse'],
    );

    logger.info(
      `Back from getting assets for with companyId: ${user.companyId}`,
    );

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      page,
      perPage,
      assets.length,
    );

    return [assets, paginationMetaData];
  }

  async getAsset(assetId: string, user: userAuthDetails): Promise<any> {
    // try {
    let chekkitDetail = await this.utilsService.getChekkitDetailFromCache();

    if (!chekkitDetail) {
      await this.tagAndTracService.login();

      chekkitDetail = await this.utilsService.getChekkitDetailFromCache();
    }

    logger.info(
      `About to get all assets for a particular company with companyId: ${user.companyId}`,
    );

    const asset = await this.assetRepository.findOne({
      where: {
        id: assetId,
      },

      include: ['stickerId', 'warehouse', 'site'],
    });

    if (!asset)
      throw new NotFoundException('Asset not found, please check passed id');

    let deviceData: IDeviceDetailsResponse;

    try {
      deviceData = (await this.tagAndTracService.call(
        {},
        HTTPREQUESTMETHOD.GET,
        `device/${asset.macAddress}/recentData`,
      )) as unknown as IDeviceDetailsResponse;
      let deviceDetails = deviceData.response;
      console.log('deviceDetails>> ', deviceDetails);
    } catch (error) {
      console.log('fetch device err: ', error);
    }

    logger.info(
      `Back from getting asset for with companyId: ${user.companyId}`,
    );

    return { asset: asset, device: deviceData?deviceData.response:null };
    // } catch (error) {
    //   console.log('query error: ', error);
    // }
  }

  async getCompanyAssets(
    id: string,
    perPage: number,
    offset: number,
    relation?: string[],
  ) {
    return await this.assetRepository.findAndCountAll({
      distinct: true,
      where: { companyId: id },
      ...(perPage && { limit: perPage }),
      ...(offset && { offset: offset }),
      order: [['created_at', 'DESC']],
      include: relation,
    });
  }

  async moveAssetToSite(
    user: userAuthDetails,
    input: MoveAssetDto,
    siteId: string,
  ) {
    const site = await this.siteService.findOne({
      id: siteId,
      companyId: user.companyId,
    });

    if (!site) throw new NotFoundException('site does not exist');

    const assetMovement = input.moveAssetDetails.map(
      async (moveAssetDetail) => {
        switch (moveAssetDetail.packageLevel) {
          case PackageLevel.PRIMARY: {
            logger.info(
              `About to move primary package level asset to site for company ${user.companyId}`,
            );

            await this.movePrimaryLevelAsset(
              siteId,
              moveAssetDetail.assetId,
              user.companyId,
            );

            logger.info(
              `Done  moving primary package level asset to site for company ${user.companyId}`,
            );

            break;
          }

          case PackageLevel.SECONDARY: {
            logger.info(
              `About to move secondary package level asset to site for company ${user.companyId}`,
            );

            await this.moveOtherLevelAsset(siteId, moveAssetDetail, user);

            logger.info(
              `Done moving secondary package level asset to site for company ${user.companyId}`,
            );

            break;
          }

          case PackageLevel.TERTIARY: {
            logger.info(
              `About to move tertiary package level asset to site for company ${user.companyId}`,
            );

            await this.moveOtherLevelAsset(siteId, moveAssetDetail, user);

            logger.info(
              `Done moving tertiary package level asset to site for company ${user.companyId}`,
            );

            break;
          }

          default: {
            logger.error('Unknown Package level');

            throw 'Unknown package level';
          }
        }
      },
    );

    await Promise.all(assetMovement);
  }

  async movePrimaryLevelAsset(
    siteId: string,
    assetId: string,
    companyId: string,
  ) {
    const asset = await this.findOne({ id: assetId, companyId });

    if (!asset) throw new NotFoundException('asset does not exist');

    await this.assetRepository.update(
      { siteId, lastTimeMoved: new Date(), warehouseId: null },
      {
        where: { id: asset.id },
      },
    );
  }

  async moveOtherLevelAsset(
    siteId: string,
    moveAssetDetail: MoveAssetDetailsDto,
    user: userAuthDetails,
  ) {
    const asset = await this.findOne({
      id: moveAssetDetail.assetId,
      companyId: user.companyId,
    });

    if (!asset) throw new NotFoundException('asset does not exist');

    let numberOfSubUnits = asset.numberOfSubUnits;

    if (
      moveAssetDetail.numberOfMovedUnits > 0 &&
      moveAssetDetail.assetDetails.length > 0
    ) {
      numberOfSubUnits = numberOfSubUnits - moveAssetDetail.numberOfMovedUnits;

      logger.info('About to onboard asset');

      const assetData: OnboardAssetDto[] = [];

      for (let i = 0; i < moveAssetDetail.numberOfMovedUnits; i++) {
        assetData.push(moveAssetDetail.assetDetails[0]);
      }

      await this.onboardAsset(assetData, user);
    }

    await this.assetRepository.update(
      {
        siteId,
        lastTimeMoved: new Date(),
        warehouseId: null,
        numberOfSubUnits,
      },
      {
        where: { id: asset.id },
      },
    );
  }

  async moveAssetToWarehouse(
    user: userAuthDetails,
    input: MoveAssetToWarehouseDto,
    warehouseId: string,
  ): Promise<void> {
    const warehouse = await this.warehouseService.findOne(warehouseId);

    if (!warehouse) throw new NotFoundException('warehouse does not exist');

    const assets = await this.findAll(input.assetIds, user.companyId);

    const movedAssets = assets.map((asset) => {
      return this.assetRepository.update(
        {
          warehouseId,
          lastTimeMoved: new Date(),
          siteId: null,
        },
        {
          where: { id: asset.id },
        },
      );
    });

    await Promise.all(movedAssets);
  }

  async findAll(assetIds: string[], companyId: string): Promise<Asset[]> {
    const assets = await this.assetRepository.findAll({
      where: {
        id: {
          [Op.in]: assetIds,
        },

        companyId,
      },
    });

    return assets;
  }

  async findOne(criteria: any): Promise<Asset> {
    return this.assetRepository.findOne({ where: { ...criteria } });
  }

  async countAllAssetsInWarehouses(companyId: string) {
    const totalAssetsInWarehouses = this.assetRepository.count({
      where: {
        companyId,
        warehouseId: {
          [Op.ne]: null,
        },
      },
    });

    return totalAssetsInWarehouses;
  }

  async getAllAssetsByType(
    user: userAuthDetails,
    page: number,
    perPage: number,
  ): Promise<[Asset[], PaginationMeta]> {
    const offset: number = getOffSet(+page, +perPage);

    logger.info(
      `About to get all assets by type for a particular company with companyId: ${user.companyId}`,
    );

    const { count: countsGroup, rows: assets } =
      await this.assetRepository.findAndCountAll({
        distinct: true,
        where: {
          companyId: user.companyId,
        },
        attributes: ['name'],
        group: ['name'],
        ...(perPage && { limit: perPage }),
        ...(offset && { offset: offset }),
      });

    logger.info(
      `Back from getting assets for with companyId: ${user.companyId}`,
    );

    const paginationMetaData = getPaginationMetaData(
      countsGroup.length,
      page,
      perPage,
      assets.length,
    );

    return [assets, paginationMetaData];
  }

  async getStockCountReport(
    companyId: string,
    filter: StockCountFilter,
  ): Promise<[Asset[], PaginationMeta]> {
    const page: number = +filter.page;
    const perPage: number = +filter.perPage;

    const offset: number = getOffSet(+page, +perPage);

    const { rows: assets, count: countsGroup } =
      await this.assetRepository.findAndCountAll({
        subQuery: false,

        distinct: true,

        where: {
          companyId,

          ...(filter.assetName && {
            name: filter.assetName,
          }),

          ...(filter.warehouseId && {
            warehouseId: filter.warehouseId,
          }),
        },

        attributes: [
          'name',
          'created_at',
          [sequelize.fn('COUNT', sequelize.col('*')), 'stockCount'],
        ],

        include: ['warehouse'],

        group: ['name'],

        order: [['name', 'ASC']],

        ...(perPage && { limit: perPage }),
        ...(offset && { offset: offset }),
      });

    const paginationMetaData = getPaginationMetaData(
      countsGroup.length,
      page,
      perPage,
      assets.length,
    );

    return [assets, paginationMetaData];
  }

  async getOnboardingRequests(
    user: userAuthDetails,
    filter: AssetProgressFilter,
  ): Promise<[AssetOnboardingRequest[], PaginationMeta]> {
    logger.info('start retrieving asset onboarding requests');
    const page: number = +filter.page;
    const perPage: number = +filter.perPage;

    const offset: number = getOffSet(+page, +perPage);

    const { rows: assetRequests, count: totalCount } =
      await this.assetOnboardingRequestRepository.findAndCountAll({
        where: {
          companyId: user.companyId,
        },
        ...(perPage && { limit: perPage }),
        ...(offset && { limit: offset }),
        order: [['created_at', 'DESC']],
      });

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      +page,
      +perPage,
      assetRequests.length,
    );

    return [assetRequests, paginationMetaData];
  }

  async getCompletedOnboardingAssets(
    user: userAuthDetails,
    requestId: string,
  ): Promise<Asset[]> {
    const assets = await this.assetRepository.findAll({
      where: {
        companyId: user.companyId,
        assetOnboardingRequestId: requestId,
      },

      order: [['created_at', 'DESC']],

      include: ['stickerId'],
    });

    return assets;
  }

  async create(input: Partial<AssetWithRequest>): Promise<Asset> {
    return this.assetRepository.create(input);
  }

  async updateOnboardingRequests(condition: any, values: any): Promise<void> {
    this.assetOnboardingRequestRepository.update(
      { ...values },
      { where: { ...condition } },
    );
  }

  async logAssetOnboardingRequest(
    input: Partial<AssetOnboardingRequest>,
  ): Promise<AssetOnboardingRequest> {
    return this.assetOnboardingRequestRepository.create(input);
  }

  async retrieveAssetsBySiteId(
    id: string,
    companyId: string,
    offset: number,
    perPage: number,
  ) {
    return this.assetRepository.findAndCountAll({
      distinct: true,

      where: { siteId: id, companyId },

      ...(perPage && { limit: perPage }),
      ...(offset && { limit: offset }),

      order: [['created_at', 'DESC']],

      include: ['stickerId'],
    });
  }

  async retrieveAssetsByWarehouseId(
    id: string,
    companyId: string,
    offset: number,
    perPage: number,
  ) {
    return this.assetRepository.findAndCountAll({
      where: { warehouseId: id, companyId },

      ...(perPage && { limit: perPage }),
      ...(offset && { limit: offset }),

      order: [['created_at', 'DESC']],

      include: ['stickerId'],
    });
  }

  async fetchSurveysByAsset(user: userAuthDetails, assetId: string) {
    logger.info('About to retrieve surveys by asset id');

    const asset = await this.assetRepository.findOne({
      where: {
        companyId: user.companyId,
        id: assetId,
      },

      include: ['survey'],
    });

    const survey = await this.surveyService.getSurvey(asset.survey?.id);

    return survey;
  }

  async resume() {
    const isPaused = this.assetQueue.isPaused();

    if (isPaused) await this.assetQueue.resume();
  }

  async pause() {
    const isPaused = await this.assetQueue.isPaused();

    if (!isPaused) await this.assetQueue.pause();
  }

  async attachRFDTagtoAssets(
    user: userAuthDetails,
    assetId: string,
    rfidId: string,
  ) {
    const asset = await this.assetRepository.findOne({
      where: {
        id: assetId,
      },
    });

    if (!asset) throw new NotFoundException('Asset does not exist');

    const rfidTag = await this.rfidService.findOneWithCompany(rfidId);

    if (!rfidTag) throw new NotFoundException('Rfid tag does not exist');

    if (rfidTag.isAssigned && rfidTag.company.id !== user.companyId)
      throw new UnprocessableEntityException('Rfid tag is not assigned to you');

    if (rfidTag.disabled || rfidTag.status)
      throw new UnprocessableEntityException(
        'Rfid Tag is disabled/deactivated',
      );

    logger.info('About attaching rfidTag to asset');

    await this.assetRepository.update(
      {
        rfidId,
        macAddress: rfidTag.macAddress,
      },
      {
        where: {
          id: assetId,
          companyId: user.companyId,
        },
      },
    );
  }

  async processAssetOnboarding(data: IAssetData) {
    const { assetDetails, companyId, surveyId, requestId } = data;

    await Promise.all(
      assetDetails.map(async (assetDetail: AssetWithRequest) => {
        const assetData: Partial<AssetWithRequest> = {
          name: assetDetail.name,
          price: assetDetail.price,
          serialNumber: assetDetail.serialNumber,
          batchNumber: assetDetail.batchNumber,
          expiryDate: assetDetail.expiryDate,
          category: assetDetail.category,
          numberOfSubUnits: assetDetail.numberOfSubUnits,
          macAddress: assetDetail.macAddress,
          packageLevel: assetDetail.packageLevel,
          stickerDescription: assetDetail.stickerDescription,
          stickerImage: assetDetail.stickerImage,
          photo: assetDetail.photo,
          warehouseId: assetDetail.warehouseId ? assetDetail.warehouseId : null,
          companyId,
          onboardingStatus: assetDetail.macAddress
            ? OnboardingStatus.PENDING
            : null,
          assetOnboardingRequestId: requestId,
        };

        let asset: Asset;

        asset = await this.assetService.findOne({
          serialNumber: assetDetail.serialNumber,
          companyId,
        });

        if (asset) {
          logger.error(
            `An asset with serial Number ${asset.serialNumber} already exist`,
          );

          await this.assetService.updateOnboardingRequests(
            { id: requestId },
            {
              status: AssetOnboardingStatus.FAIL,
            },
          );

          return;
        }

        asset = await this.assetService.create(assetData);

        await this.assetOnboardingViaTagAndTrac({
          companyId,
          assetData,
          asset,
          assetDetail,
          surveyId,
          assetDetails,
          requestId,
        });
      }),
    );
  }

  async assetOnboardingViaTagAndTrac(input: AssetOnbaordingViaTNTDto) {
    const trackedId = this.utilsService.generateTrackerId();

    if (input.assetData.macAddress) {
      logger.info(
        `About to get available rfid with macAddess: ${input.assetData.macAddress}`,
      );

      const rfid = await this.rfidService.find({
        macAddress: input.assetData.macAddress,
        disabled: false,
        status: false,
        isAssigned: true,
        companyId: input.companyId,
      });

      if (!rfid) {
        logger.info('No available rfid for company');

        return;
      }

      if (rfid) {
        const project = await this.warehouseService.getProjectByCompanyId(
          input.companyId,
        );

        if (!project) {
          logger.info('Company does not have project');

          return;
        }
        const trackedId = this.utilsService.generateTrackerId();

        const provisionAssetData = {
          trackedId,
          tu_type: IPackageLevelCatgory[input.assetData.packageLevel],
          projectId: project.provider_project_id,
        };

        logger.info(
          'About calling tag and trac service to provision asset with a specific device',
        );

        try {
          await this.tagAndTracService.call(
            provisionAssetData,
            HTTPREQUESTMETHOD.POST,
            `device/${input.assetData.macAddress}/asset`,
          );
          logger.info(
            `successfully provisioned assets with trackedId: ${trackedId}`,
          );
        } catch (error) {
          logger.error(
            'An error occurred while provisioning asset on tag and trac',
            error,
          );

          return;
        }

        const updateRfidProp = {
          status: true,
        };

        await this.rfidService.updateOne(updateRfidProp, rfid.id);

        input.asset.set('rfid', rfid.id);
      }
    }

    logger.info(`About to generate unique pin for asset`);

    const pin = this.utilsService.myUniqueID(16);

    const newPin: Partial<Pin> = {
      value: pin.toString(),
      assetId: input.asset.id,
    };

    const createdPin = await this.pinRepository.create(newPin);

    const updateAssetProp = {
      companyId: input.companyId,
      provider_tracking_id: trackedId,
      ...(input.surveyId && { surveyId: input.surveyId }),
      pinId: createdPin.id,
      warehouseId: '',
      onboardingStatus: OnboardingStatus.COMPLETED,
    };

    if (input.assetDetail.warehouseId) {
      const warehouse = await this.warehouseService.findOne(
        input.assetDetail.warehouseId,
      );

      if (warehouse) updateAssetProp.warehouseId = warehouse.id;
    }

    await this.update(input.asset.id, updateAssetProp);

    const company = await this.companyService.findOne({
      id: input.companyId,
    });

    logger.info(`About updating count of assets onboarded by company`);

    await this.companyService.update(
      {
        totalAssets: company.totalAssets + input.assetDetails.length,
      },
      { id: input.companyId },
    );

    logger.info(`About updating asset request`);

    await this.assetService.updateOnboardingRequests(
      { id: input.requestId },
      {
        status: AssetOnboardingStatus.COMPLETED,
      },
    );
  }

  async completeAssetOnboardingViaTagAndTrac(asset: Asset) {
    const trackedId = this.utilsService.generateTrackerId();

    if (asset.macAddress) {
      logger.info(
        `About to get available rfid with macAddess: ${asset.macAddress}`,
      );

      const rfid = await this.rfidService.find({
        macAddress: asset.macAddress,
        disabled: false,
        status: false,
        isAssigned: true,
        companyId: asset.companyId,
      });

      if (!rfid) {
        logger.info('No available rfid for company');

        return;
      }

      if (rfid) {
        const project = await this.warehouseService.getProjectByCompanyId(
          asset.companyId,
        );

        if (!project) {
          logger.info('Company does not have project');

          return;
        }

        const provisionAssetData = {
          trackedId,
          tu_type: IPackageLevelCatgory[asset.packageLevel],
          projectId: project.provider_project_id,
        };

        logger.info(
          'About calling tag and trac service to provision asset with a specific device',
        );

        try {
          await this.tagAndTracService.call(
            provisionAssetData,
            HTTPREQUESTMETHOD.POST,
            `device/${asset.macAddress}/asset`,
          );
        } catch (error) {
          logger.error(
            'An error occurred while provisioning asset on tag and trac',
            error,
          );

          return;
        }

        const updateRfidProp = {
          status: true,
        };

        await this.rfidService.updateOne(updateRfidProp, rfid.id);

        asset.set('rfid', rfid.id);
      }
    }

    logger.info(`About to generate unique pin for asset`);

    const pin = this.utilsService.myUniqueID(16);

    const newPin: Partial<Pin> = {
      value: pin.toString(),
      assetId: asset.id,
    };

    const createdPin = await this.pinRepository.create(newPin);

    const updateAssetProp = {
      companyId: asset.companyId,
      provider_tracking_Id: trackedId,
      ...(asset.surveyId && { surveyId: asset.surveyId }),
      pinId: createdPin.id,
      warehouseId: '',
      onboardingStatus: OnboardingStatus.COMPLETED,
    };

    if (asset.warehouseId) {
      const warehouse = await this.warehouseService.findOne(asset.warehouseId);

      if (warehouse) updateAssetProp.warehouseId = warehouse.id;
    }

    await this.update(asset.id, updateAssetProp);

    const company = await this.companyService.findOne({
      id: asset.companyId,
    });

    logger.info(`About updating count of assets onboarded by company`);

    await this.companyService.update(
      {
        totalAssets: company.totalAssets + 1,
      },
      { id: asset.companyId },
    );

    logger.info(`About updating asset request`);

    await this.assetService.updateOnboardingRequests(
      { id: asset.assetOnboardingRequestId },
      {
        status: AssetOnboardingStatus.COMPLETED,
      },
    );
  }

  async update(assetId: string, values: any): Promise<void> {
    await this.assetRepository.update(
      {
        ...values,
      },
      {
        where: { id: assetId },
      },
    );
  }

  async createStockCount(
    data: StockCountDTO,
    user: userAuthDetails,
  ): Promise<void> {
    const countBatch = uuidv4();

    const bulkData = await Promise.all(
      data.records.map(async (record) => {
        return {
          newCount: record.count,
          comment: record?.comment,
          companyId: user.companyId,
          assetId: record.id,
          countBatch,
          lastCount: await this._getOldCount(record.id),
        };
      }),
    );

    logger.info(`about to create stock count}`);

    this.stockCountHistoryRequestRepository.bulkCreate(bulkData);
  }

  async createAssetStatusUpdate(
    data: AssetStatusUpdateDto,
    user: userAuthDetails,
  ): Promise<void> {

    const statusData: Partial<AssetStatusUpdate> = {
      companyId: user.companyId,
      status: data.status,
      comment: data.comment,
      reporterId: user.id,
      assetId: data.assetId,
    };
    logger.info(`about to create status update}`);
    this.assetStatusUpdateRepository.create(statusData)
  }

  async getStockCountHistory(user: userAuthDetails) {
    return this.stockCountHistoryRequestRepository.findAll({
      where: {
        companyId: user.companyId,
      },

      group: ['count_batch'],

      attributes: [
        'created_at',
        'count_batch',
        [Sequelize.fn('SUM', Sequelize.col('new_count')), 'new_count'],
        [Sequelize.fn('SUM', Sequelize.col('last_count')), 'last_count'],
      ],

      order: [['created_at', 'DESC']],
    });
  }

  async getStockCountBatch(countBatch: string) {
    return this.stockCountHistoryRequestRepository.findAll({
      where: { countBatch },

      include: [
        {
          model: Asset,
          attributes: ['name'],
        },
      ],
    });
  }

  async _getOldCount(assetId: string) {
    const resp = await this.stockCountHistoryRequestRepository.findOne({
      where: { assetId },

      order: [['created_at', 'DESC']],

      attributes: ['new_count'],
    });

    return resp?.getDataValue('new_count') || 0;
  }

  async getRecentLocationAssetData(
    companyId: string,
    resourceId: string,
    filter: ListAssetLocationFilter,
  ): Promise<any> {
    try {
      logger.info('About to get project details');

      const project = await this.warehouseService.getProjectByCompanyId(
        companyId,
      );

      if (!project) throw new NotFoundException('Project does not exist');

      logger.info('starts getting asset data for chekkit organization');

      const response = await this.tagAndTracService.call(
        {},
        HTTPREQUESTMETHOD.GET,
        `asset/data?project=${project.provider_project_id}`,
      );

      logger.info(
        `About to retrieve asset resource with id ${resourceId} for type ${filter.type}`,
      );

      let resource: Warehouse | Site;

      let assets: Asset[];

      if (filter?.type === ListTypeLocation.Warehouse) {
        resource = (await this.warehouseService.findOne(
          resourceId,
        )) as Warehouse;

        assets = await this.assetRepository.findAll({
          where: {
            warehouseId: resource.id,

            macAddress: {
              [Op.ne]: null,
            },
          },
        });
      }

      if (filter?.type === ListTypeLocation.Site) {
        resource = (await this.siteService.findOne({ id: resourceId })) as Site;

        assets = await this.assetRepository.findAll({
          where: {
            siteId: resource.id,

            macAddress: {
              [Op.ne]: null,
            },
          },
        });
      }

      if (!resource)
        throw new NotFoundException(
          `No assets onboarded with resource ${filter?.type} with id = ${resourceId} does not exist`,
        );

      logger.info(
        `start filtering asset data by project id: ${project.id}  and assets for company with id: ${companyId}`,
      );

      const activeAssets = response.assets as ActiveAssetData[];

      if (!activeAssets)
        throw new NotFoundException(
          `No assets data has been uploaded for a particular company for chekkit organization`,
        );

      // const activeAssetDataForCompany = await Promise.all(
      //   activeAssets.filter(
      //     (activeAsset) =>
      //       activeAsset.projectId === project.provider_project_id &&
      //       assets?.find(
      //         ({ macAddress }) => macAddress === activeAsset.device.id,
      //       ),
      //   ),
      // );

      const mappedTrackingData = activeAssets.map((trackingData) => ({
        lat: trackingData.recentData.lat,
        lng: trackingData.recentData.lng,
        assetDetail: assets.find(
          (asset) => asset.macAddress === trackingData.device.id,
        ),
      }));

      logger.info('done filtering asset data by project');

      return mappedTrackingData;
    } catch (error) {
      console.log({ error });

      logger.error(
        'An error occurred while getting asset location error:',
        error,
      );

      return error;
    }
  }

  async provisionGateway(user: userAuthDetails, input: GatewayDto) {
    const project = await this.warehouseService.getProjectByCompanyId(
      input?.companyId,
    );

    if (!project) throw new NotFoundException('Project does not exist');

    const gatewayData = {
      gatewayType: input.gatewayType,
      location: input.location || 'Lagos, Nigeria',
      longitude: input.longitude,
      latitude: input.latitude,
      projectId: project.provider_project_id,
      lastReading: { '': '' },
      attributes: { '': '' },
    };

    logger.info('starts provisioning gateway');

    await Promise.all(
      input.deviceIds.map(async (deviceId) => {
        await this.tagAndTracService.call(
          gatewayData,
          HTTPREQUESTMETHOD.POST,
          `device/${deviceId}/gateway`,
        );
      }),
    );

    logger.info('done provisioning gateway');
  }

  async completeAssetOnboarding() {
    const pendingAssetsOnboarding = await this.assetRepository.findAll({
      where: {
        onboardingStatus: OnboardingStatus.PENDING,
      },
    });

    await Promise.all(
      pendingAssetsOnboarding.map(async (asset) => {
        await this.completeAssetOnboardingViaTagAndTrac(asset);
      }),
    );
  }

  async getAgentDashboardStats(user: userAuthDetails) {
    const assetStat = await this.assetRepository.count({
      where: { companyId: user.companyId },
    });

    const warehouseStat = await this.warehouseRepository.count({
      where: { companyId: user.companyId },
    });

    const assetTypes = await this.assetRepository.count({
      distinct: true,
      where: {
        companyId: user.companyId,
      }
    });
    const siteCount = await this.siteRepository.count({
      where: {
        companyId: user.companyId,
      },
    });

    let stats = {
      assets: assetStat,
      warehouses: warehouseStat,
      types: assetTypes,
      sites: siteCount,
    };
    console.log('assetStat>', stats);

    return stats;
  }

  async getAllAssetsAgent(
    user: userAuthDetails,
    page: number,
    perPage: number,
  ): Promise<[Asset[], PaginationMeta]> {
    const offset = getOffSet(page, perPage);

    logger.info(
      `About to get all assets for a particular company with companyId: ${user.companyId}`,
    );

    const { count: totalCount, rows: assets } = await this.getCompanyAssets(
      user.companyId,
      perPage,
      offset,
      ['stickerId', 'warehouse'],
    );

    logger.info(
      `Back from getting assets for with companyId: ${user.companyId}`,
    );

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      page,
      perPage,
      assets.length,
    );

    return [assets, paginationMetaData];
  }
}
