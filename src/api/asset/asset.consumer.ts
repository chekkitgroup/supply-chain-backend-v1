import {
  Processor,
  Process,
  OnQueueError,
  OnQueueActive,
  OnQueueWaiting,
  OnQueueProgress,
  OnQueueFailed,
  OnQueueCompleted,
} from '@nestjs/bull';

import { Job } from 'bull';
import logger from 'utils/logger';

import { ONBOARD_ASSETS } from './constant';

import { AssetService } from './asset.service';
import { IAssetData } from './types';

@Processor(ONBOARD_ASSETS)
export class AssetConsumerService {
  constructor(private readonly assetService: AssetService) {}

  @Process()
  async processAsset(job: Job<IAssetData>) {
    try {
      logger.info(`Job process started`);

      await this.assetService.processAssetOnboarding(job.data);

      logger.info('Done processing job on queue');
    } catch (error) {
      throw error;
    }
  }

  @OnQueueActive()
  async onActive(job: Job) {
    logger.info(
      `Processing job ${job.id} of type ${job.name} with data ${JSON.stringify(
        job.data,
      )}......`,
    );
  }

  @OnQueueWaiting()
  async jobWaiting(jobId: number) {
    logger.info(`Job with ${jobId} waiting to be executed`);
  }

  @OnQueueProgress()
  async jobProgress(job: Job, progress: number) {
    logger.info(
      `Progress report with job id ${job.id} received with progress ${progress}`,
    );
  }

  @OnQueueError()
  async onError(error: Error) {
    logger.error(`An error occurred while processing jobs in queue: ${error}`);
  }

  @OnQueueFailed()
  async onJobFailed(job: Job, error: Error) {
    logger.error(`Job failed with id: ${job.id} reason err ${error}`);
  }

  @OnQueueCompleted()
  async onQueueCompleted(job: Job) {
    logger.info(`Job with ${job.id} successfully completed`);
  }
}
