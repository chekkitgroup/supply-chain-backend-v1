import Company from 'api/users/models/company.model';
import User from 'api/users/models/user.model';
import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import { AssetUpdateStatus } from '../types';
import Asset from './asset.model';

@Table
class AssetStatusUpdate extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({
    type: DataType.ENUM('Fairly Good', 'Good Condition', 'Bad Condition'),
  })
  status: AssetUpdateStatus;

  @Column({
    type: DataType.TEXT,
    allowNull: true,
  })
  comment?: string;


  @ForeignKey(() => User)
  reporter: string;

  @BelongsTo(() => Company, 'companyId')
  company: Company;

  @BelongsTo(() => Asset, 'assetId')
  asset: Asset;


  ///virtuals
  companyId: string;
  assetId: string;
  reporterId: string;
}
export default AssetStatusUpdate;
