import Company from 'api/users/models/company.model';
import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  BelongsTo,
} from 'sequelize-typescript';
import Asset from './asset.model';

@Table
class StockCountHistory extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  newCount: number;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  lastCount: number;

  @Column({
    type: DataType.TEXT,
    allowNull: true,
  })
  commnet: string;

  @IsUUID(4)
  @Column({
    type: DataType.STRING(50),
    allowNull: false,
  })
  countBatch: string;

  @BelongsTo(() => Company, 'companyId')
  company: Company;

  @BelongsTo(() => Asset, 'assetId')
  asset: Asset;
}
export default StockCountHistory;
