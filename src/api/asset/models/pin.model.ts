import Asset from 'api/asset/models/asset.model';
import { UUIDV4 } from 'sequelize';
import {
  Column,
  Model,
  DataType,
  HasOne,
  PrimaryKey,
  IsUUID,
  ForeignKey,
  Table,
} from 'sequelize-typescript';

@Table
class Pin extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({
    type: DataType.STRING(16),
    allowNull: false,
    unique: true,
  })
  value: string;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: true,
  })
  isAssigned: boolean;

  @ForeignKey(() => Asset)
  @Column
  assetId: string;

  @HasOne(() => Asset, 'pinId')
  asset: Asset;
}

export default Pin;
