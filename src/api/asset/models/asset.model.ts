import Site from 'api/site/models/site.model';
import Survey from 'api/survey/models/survey.model';
import Company from 'api/users/models/company.model';
import Warehouse from 'api/warehouse/models/warehouse.model';
import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  BelongsTo,
} from 'sequelize-typescript';
import { OnboardingStatus, PackageLevel } from '../types';
import Pin from './pin.model';

@Table
class Asset extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({
    type: DataType.STRING(50),
    allowNull: false,
  })
  name: string;

  @Column({
    type: DataType.FLOAT,
    allowNull: false,
  })
  price: number;

  @Column({
    type: DataType.STRING({ length: 50 }),
    allowNull: false,
  })
  serialNumber: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  batchNumber: string;

  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  photo: string;

  @Column({
    type: DataType.DATE,
  })
  expiryDate: Date;

  @Column({
    type: DataType.STRING(50),
  })
  category: string;

  @Column({ type: DataType.BIGINT })
  numberOfSubUnits: number;

  @Column({ type: DataType.STRING })
  macAddress: string;

  @Column({
    type: DataType.ENUM('Tertiary', 'Secondary', 'Primary'),
  })
  packageLevel: PackageLevel;

  @Column({
    type: DataType.TEXT,
  })
  stickerDescription: string;

  @Column({
    type: DataType.TEXT,
  })
  stickerImage: string;

  @Column({
    type: DataType.DATE,
  })
  lastTimeMoved: Date;

  @Column({
    type: DataType.STRING,
  })
  providerTrackingId: string;

  @Column({
    type: DataType.ENUM('Completed', 'Pending', 'Failed'),
  })
  onboardingStatus: OnboardingStatus;

  @BelongsTo(() => Company, 'companyId')
  company: Company;

  @BelongsTo(() => Warehouse, 'warehouseId')
  warehouse: Warehouse;

  @BelongsTo(() => Site, 'siteId')
  site: Site;

  @BelongsTo(() => Survey, 'surveyId')
  survey: Survey;

  @BelongsTo(() => Pin, 'pinId')
  stickerId: Pin;

  @BelongsTo(() => Asset, 'parentAssetId')
  parentAsset: Asset;

  ///virtuals
  assetOnboardingRequestId: string;
  companyId: string;
  surveyId: string;
  warehouseId: string;
}
export default Asset;
