import Company from 'api/users/models/company.model';
import User from 'api/users/models/user.model';
import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  BelongsTo,
  DataType,
  HasMany,
} from 'sequelize-typescript';
import { AssetOnboardingStatus } from '../types';
import Asset from './asset.model';

@Table
class AssetOnboardingRequest extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({
    type: DataType.ENUM('Fail', 'Processing', 'Completed'),
  })
  status: AssetOnboardingStatus;

  @BelongsTo(() => Company, 'companyId')
  company: Company;

  @BelongsTo(() => User, 'userId')
  user: User;

  @HasMany(() => Asset, 'assetOnboardingRequestId')
  assets: Asset[];
}
export default AssetOnboardingRequest;
