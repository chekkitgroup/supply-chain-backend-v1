import AssetOnboardingRequest from './models/asset-onboarding-request.model';
import Asset from './models/asset.model';

export enum PackageLevel {
  TERTIARY = 'Tertiary',
  SECONDARY = 'Secondary',
  PRIMARY = 'Primary',
}

export enum OnboardingStatus {
  COMPLETED = 'Completed',
  FAILED = 'Failed',
  PENDING = 'Pending',
}

export interface UploaderResponse {
  ETag: string;
  Location: string;
  key: string;
  Key: string;
  Bucket: string;
  description: string;
}

export enum AssetOnboardingStatus {
  FAIL = 'Fail',
  PROCESSNG = 'Processing',
  COMPLETED = 'Completed',
}

export enum AssetUpdateStatus {
  FAIRLY_GOOD = 'Fairly Good',
  GOOD_CONDITION = 'Good Condition',
  BAD_CONDITION = 'Bad Condition',
}

export interface AssetWithRequest extends Asset {
  assetOnboardingRequestId: string;
  warehouseId: string;
}

export interface AssetOnboardingRequestWithExtension
  extends AssetOnboardingRequest {
  companyId: string;
  userId: string;
}

export enum IPackageLevelCatgory {
  Tertiary = 'CONTAINER',
  Secondary = 'BOX',
  Primary = 'UNIT',
}

export interface IAssetData {
  assetDetails: AssetWithRequest[];
  companyId: string;
  surveyId: string;
  requestId: string;
}

export interface RecentData {
  timeStamp: string;
  tm: string;
  lat: string;
  lng: string;
  vbat: string;
  rssi: string;
  type: string;
  aY: string;
  ts: string;
  count: string;
  deviceId: string;
}

export interface TrackedUnit {
  tuType: string;
  trackingId: string;
}

export interface Device {
  id: string;
  deviceType: string;
}

export interface Project {
  projectName: string;
}

export interface SourceAddress {
  locality: string;
  city: string;
  state: string;
  zip: string;
  country: string;
}

export interface DestinationAddress {
  locality: string;
  city: string;
  state: string;
  zip: string;
  country: string;
}

export interface ActiveAssetData {
  id: string;
  deviceId: string;
  tuId: string;
  projectId: string;
  organizationId: string;
  state: string;
  attributes?: any;
  currentLocation?: any;
  lastReadBy?: any;
  isActive: boolean;
  lastReportedAt: Date;
  createdAt: Date;
  updatedAt: Date;
  recentData: RecentData;
  trackedUnit: TrackedUnit;
  device: Device;
  project: Project;
  sourceAddress: SourceAddress;
  destinationAddress: DestinationAddress;
}

export interface AssetOnbaordingViaTNTDto {
  companyId: string;
  assetData: Partial<AssetWithRequest>;
  asset: Asset;
  assetDetail: AssetWithRequest;
  surveyId: string;
  assetDetails: AssetWithRequest[];
  requestId: string;
}

export enum ListTypeLocation {
  Warehouse = 'warehouse',
  Site = 'site',
}
