import { Injectable } from '@nestjs/common';

import axios from 'axios';
import { configuration } from '../../../config/index';
import { HTTPREQUESTMETHOD } from './types';

import logger from 'utils/logger';
import { UtilsService } from 'utils/utils.service';

@Injectable()
export class TagAndTracService {
  private clientId: string;
  private origin: string;

  private apiKey: string;

  private url: string;

  private email: string;
  private password: string;

  private options: any;

  constructor(private utilsService: UtilsService) {
    this.url = configuration().tagAndTrac.url;
    this.clientId = configuration().tagAndTrac.clientId;
    this.origin = configuration().tagAndTrac.origin;
    this.email = configuration().tagAndTrac.email;
    this.password = configuration().tagAndTrac.password;

    this.apiKey = configuration().tagAndTrac.apiKey;

    this.options = {
      headers: {
        'Content-Type': 'application/json',
        Origin: this.origin,
        Authorization: '',
        'X-Api-Key': this.apiKey,
      },
      params: {
        clientId: this.clientId,
      },
    };
  }

  async login() {
    try {
      const requestOptions = Object.assign({}, this.options);

      requestOptions.url = `${this.url}/login`;

      requestOptions.method = HTTPREQUESTMETHOD.POST;

      requestOptions.data = {
        emailId: this.email,
        userSecret: this.password,
      };

      requestOptions.data = JSON.stringify(requestOptions.data);

      logger.info('About to login to Tag and Trac');

      const response = await axios(requestOptions);

      logger.info(
        `endpoint: ${requestOptions.url}, method: ${
          requestOptions.method
        } : response from tag and trac service ${JSON.stringify(
          response?.data,
        )}`,
      );

      await this.utilsService.setChekkitDetailFromCache(
        'providerAuthPayload',
        response.data,
      );
    } catch (error) {
      const logError = error.response?.data ? error.response?.data : error;

      logger.error(
        `An error occurred while calling tag and trac service for login endpoint, error: ${JSON.stringify(
          logError,
        )}, status: ${error.response?.status}`,
      );
    }
  }

  async call<T>(data: T, method: HTTPREQUESTMETHOD, endpoint: string) {
    let chekkitDetail = await this.utilsService.getChekkitDetailFromCache();

    if (!chekkitDetail) {
      await this.login();

      chekkitDetail = await this.utilsService.getChekkitDetailFromCache();
    }

    this.options.headers.Authorization = chekkitDetail.token;

    const requestOptions = Object.assign({}, this.options);

    requestOptions.url = `${this.url}/${endpoint}`;
    console.log('url >> ', requestOptions.url )
    if (method !== HTTPREQUESTMETHOD.GET) {
      const datum = {
        ...data,
        createdBy: chekkitDetail.user.userId,
        organizationId: chekkitDetail.organization.id,
      };

      requestOptions.data = JSON.stringify(datum);
    }

    requestOptions.method = method;

    logger.info(
      `About to call Tag and Trac ${endpoint} with data ${requestOptions.data}`,
    );

    try {
      const response = await axios(requestOptions);

      logger.info(
        `endpoint: ${endpoint}, method: ${method}, response from tag and trac service ${JSON.stringify(
          response?.data,
        )}`,
      );

      return response?.data;
    } catch (error) {
      const logError = error.response?.data ? error.response?.data : error;

      logger.error(
        `An error occurred while calling tag and trac service for '${endpoint}', error: ${JSON.stringify(
          logError,
        )}, status: ${error.response?.status}`,
      );

      throw logError;
    }
  }
}
