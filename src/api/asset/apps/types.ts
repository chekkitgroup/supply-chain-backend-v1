export enum HTTPREQUESTMETHOD {
  POST = 'post',
  GET = 'get',
  DELETE = 'delete',
  PUT = 'put',
}

export interface User {
  userName: string;
  slug?: any;
  email: string;
  contact?: any;
  role: string;
  avatar?: any;
  userId: string;
}

export interface ClientApiKey {
  clientId: string;
}

export interface Organization {
  id: string;
  organizationName: string;
  organizationType: string;
  ClientApiKey: ClientApiKey;
}

export interface IProviderAuthPayload {
  status: string;
  user: User;
  project: any[];
  token: string;
  idToken: string;
  refreshToken: string;
  expiration: number;
  organization: Organization;
  clientApiKey: ClientApiKey;
}
