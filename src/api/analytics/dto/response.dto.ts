import { getSchemaPath } from '@nestjs/swagger';
import Asset from 'api/asset/models/asset.model';
import { WarehouseOutputDto } from 'api/warehouse/dtos/response.dto';
import { Warehouse } from 'aws-sdk/clients/quicksight';
import { SurveyPerformanceRecords } from '../types';

export class WarehouseWithTopAssetsOutputDto {
  id: string;
  name: string;
  state: string;
  country: string;
  assetsCount: string;
  contribution: number;

  static fromWarehouseWithTopAssets(warehouse: Warehouse) {
    const output = new WarehouseWithTopAssetsOutputDto();

    const exposedKeys = [
      'id',
      'name',
      'state',
      'country',
      'assetsCount',
      'contribution',
    ];

    exposedKeys.forEach((key) => {
      output[key] = warehouse[key];
    });

    return output;
  }
}

export const WarehouseWithTopAssetsOutputDtoSchema = getSchemaPath(
  WarehouseWithTopAssetsOutputDto,
);

export class NumberOfDaysOutputDto {
  dayOfWeek: string;
  assetsCount: string;

  static fromNumberOfDaysOutputDto(warehouse: Warehouse) {
    const output = new NumberOfDaysOutputDto();

    const exposedKeys = ['dayOfWeek', 'assetsCount'];

    exposedKeys.forEach((key) => {
      output[key] = warehouse[key];
    });

    return output;
  }
}

export const NumberOfDaysOutputDtoSchema = getSchemaPath(NumberOfDaysOutputDto);

export class StockCountOutputDto {
  name: string;
  stockCount: string;
  warehouseName: string;
  warehouseLocation: string;
  created_at: string;

  static fromStockOutput(asset: Asset) {
    const output = new StockCountOutputDto();

    const exposedKeys = [
      'name',
      'stockCount',
      'warehouseName',
      'warehouseLocation',
      'created_at',
    ];

    exposedKeys.forEach((key) => {
      if (key === 'warehouseName' && asset.warehouse) {
        const warehouseData = WarehouseOutputDto.FromWarehouseOutput(
          asset.warehouse,
        );

        output[key] = warehouseData.name;

        return;
      }

      if (key === 'warehouseLocation' && asset.warehouse) {
        const warehouseData = WarehouseOutputDto.FromWarehouseOutput(
          asset.warehouse,
        );

        output[key] = `${warehouseData.state},${warehouseData.country}`;

        return;
      }

      output[key] = asset[key];
    });

    return output;
  }
}

export const StockCountOuputDtoSchema = getSchemaPath(StockCountOutputDto);

export class SurveyPerformanceOutputDto {
  id: string;
  title: string;
  content: string;
  numberOfResponse: number;

  static fromSurveyPerformanceOutput(records: SurveyPerformanceRecords) {
    const output = new SurveyPerformanceOutputDto();

    const exposedKeys = ['id', 'title', 'content', 'numberOfResponse'];

    exposedKeys.forEach((key) => {
      if (key === 'numberOfResponse' && records.numberOfResponse) {
        output[key] = +records.numberOfResponse;

        return;
      }

      output[key] = records[key];
    });

    return output;
  }
}

export const SurveyPerformanceOutputDtoSchema = getSchemaPath(
  SurveyPerformanceOutputDto,
);
