import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsNumberString,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { PaginationFilter } from 'utils/pagination';

export class WarehouseTopAssetFilter {
  @ApiProperty({ example: 5, required: false })
  @IsNumberString()
  @IsOptional()
  count?: string = '5';
}

export class getNumberOfDaysOfAssetsFilter {
  @ApiProperty({ description: 'filter from' })
  @IsDateString()
  from: Date;

  @ApiProperty({ description: 'filter to' })
  @IsDateString()
  to: Date;
}

export class StockCountFilter extends PaginationFilter {
  @ApiProperty({ example: 'Solar panel', required: false })
  @IsString()
  @IsOptional()
  assetName: string;

  @ApiProperty({ example: '', required: false })
  @IsString()
  @IsOptional()
  warehouseId: string;
}

export class SurveyPerformanceFilter {
  @ApiProperty({ description: 'filter from' })
  @IsDateString()
  from: Date;

  @ApiProperty({ description: 'filter to' })
  @IsDateString()
  to: Date;
}

export class DashboardFilterDTO {
  @ApiProperty({ description: 'filter from' })
  @IsOptional()
  @IsDateString()
  from: Date;

  @ApiProperty({ description: 'filter to' })
  @IsOptional()
  @IsDateString()
  to: Date;

  @ApiProperty({ description: 'asset name', required: false })
  @IsOptional()
  @IsString()
  assetName?: string;

  @ApiProperty({ description: 'warehouse Id', required: false })
  @IsOptional()
  @IsUUID()
  warehouseId?: string;
}
