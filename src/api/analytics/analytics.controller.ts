import { Controller, Get, Query, Request } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Authorize } from 'api/auth/authority.decorator';
import { RequestWithUser } from 'api/auth/types';
import { successResponse } from 'utils/response';
import { ResponseSchemaHelper } from 'utils/types/output-model';
import { AnalyticsService } from './analytics.service';
import {
  DashboardFilterDTO,
  getNumberOfDaysOfAssetsFilter,
  StockCountFilter,
  SurveyPerformanceFilter,
  WarehouseTopAssetFilter,
} from './dto';
import {
  NumberOfDaysOutputDtoSchema,
  StockCountOuputDtoSchema,
  StockCountOutputDto,
  SurveyPerformanceOutputDto,
  SurveyPerformanceOutputDtoSchema,
  WarehouseWithTopAssetsOutputDtoSchema,
} from './dto/response.dto';

@ApiBearerAuth()
@Controller('analytics')
@ApiTags('Analytics')
export class AnalyticsController {
  constructor(private analyticsService: AnalyticsService) {}

  @ApiResponse({
    status: 200,
    description: 'Retrieves all assets for a particular company',
    schema: ResponseSchemaHelper(
      WarehouseWithTopAssetsOutputDtoSchema,
      'array',
    ),
  })
  @ApiOperation({
    summary: 'Get warehouses with top assets',
  })
  @Authorize('reports.view')
  @Get('/warehouse-with-most-assets')
  async getWarehousesWithTopAssets(
    @Request() req: RequestWithUser,
    @Query() filter?: WarehouseTopAssetFilter,
  ) {
    const response = await this.analyticsService.getWarehousesWithTopAssets(
      req.user,
      filter,
    );

    return successResponse('Analytics retrieved successfully', 200, response);
  }

  @ApiResponse({
    status: 200,
    description: 'Retrieves all assets for a particular company',
    schema: ResponseSchemaHelper(NumberOfDaysOutputDtoSchema, 'array'),
  })
  @ApiOperation({
    summary: 'Get warehouses with top assets',
  })
  @Authorize('reports.view')
  @Get('/days-with-assets')
  async getNumberOfDaysOfAssets(
    @Request() req: RequestWithUser,
    @Query() filter?: getNumberOfDaysOfAssetsFilter,
  ) {
    const response = await this.analyticsService.getNumberOfDaysOfAssets(
      req.user,
      filter,
    );

    return successResponse('Analytics retrieved successfully', 200, response);
  }

  @ApiResponse({
    status: 200,
    description: 'List stock count reports for a particular company',
    schema: ResponseSchemaHelper(StockCountOuputDtoSchema, 'array'),
  })
  @ApiOperation({
    summary: 'List stockcount reports',
  })
  @Authorize('reports.view')
  @Get('/stock-count-reports')
  async getStockCountReport(
    @Request() req: RequestWithUser,
    @Query() filter?: StockCountFilter,
  ) {
    const [assets, paginationMetaData] =
      await this.analyticsService.getStockCountReport(req.user, filter);

    const output = assets?.map((data) =>
      StockCountOutputDto.fromStockOutput(data),
    );

    return successResponse(
      'Stock count report retrieved successfully',
      200,
      output,
      true,
      paginationMetaData,
    );
  }

  @ApiResponse({
    status: 200,
    description: 'Get suvery performance analytics for a particular company',
    schema: ResponseSchemaHelper(SurveyPerformanceOutputDtoSchema, 'array'),
  })
  @ApiOperation({
    summary: 'Survey Performance analytics',
  })
  @Authorize('reports.view')
  @Get('/performance')
  async getSurveyPerformance(
    @Request() req: RequestWithUser,
    @Query() filter?: SurveyPerformanceFilter,
  ) {
    const results = await this.analyticsService.getSurveyPerformance(
      req.user,
      filter,
    );

    const output = results?.map((result) =>
      SurveyPerformanceOutputDto.fromSurveyPerformanceOutput(result),
    );

    return successResponse('Analytics retrieved successfully', 200, output);
  }

  @ApiResponse({
    status: 200,
    description: 'Get total asset count for a particular company',
    schema: ResponseSchemaHelper(SurveyPerformanceOutputDtoSchema, 'array'),
  })
  @ApiOperation({
    summary: 'Asset total count',
  })
  @Authorize('reports.view')
  @Get('/total-asset-by-months')
  async getTotalAssetsByMonth(
    @Request() req: RequestWithUser,
    @Query() filter?: SurveyPerformanceFilter,
  ) {
    const results = await this.analyticsService.getSurveyPerformance(
      req.user,
      filter,
    );

    const output = results?.map((result) =>
      SurveyPerformanceOutputDto.fromSurveyPerformanceOutput(result),
    );

    return successResponse('Analytics retrieved successfully', 200, output);
  }

  @ApiResponse({
    status: 200,
    description:
      'Get Asset and Inventory Management dashboard for a particular company',
    schema: ResponseSchemaHelper(SurveyPerformanceOutputDtoSchema, 'array'),
  })
  @ApiOperation({
    summary: 'Asset and inventory management dashboard analytics',
  })
  @Authorize('reports.view')
  @Get('/asset-dashboard')
  async assetReport(
    @Request() req: RequestWithUser,
    @Query() filter?: DashboardFilterDTO,
  ) {
    const total_assets_by_month =
      await this.analyticsService.getTotalAssetCountByMonth(req.user, filter);
    const asset_by_type = await this.analyticsService.getTotalAssetCountByType(
      req.user,
      filter,
    );
    return successResponse('Analytics retrieved successfully', 200, {
      total_assets_by_month,
      asset_by_type,
    });
  }
}
