import { Module } from '@nestjs/common';
import { AnalyticsService } from './analytics.service';
import { AnalyticsController } from './analytics.controller';
import { WareHouseModule } from 'api/warehouse/warehouse.module';
import { UsersModule } from 'api/users/users.module';
import { UtilsModule } from 'utils/utils.module';
import { AssetModule } from 'api/asset/asset.module';

@Module({
  imports: [WareHouseModule, UsersModule, UtilsModule, AssetModule],
  providers: [AnalyticsService],
  controllers: [AnalyticsController],
})
export class AnalyticsModule {}
