import { Injectable } from '@nestjs/common';
import { AssetService } from 'api/asset/asset.service';
import Asset from 'api/asset/models/asset.model';
import { userAuthDetails } from 'api/auth/types';
import { WareHouseService } from 'api/warehouse/warehouse.service';
import sequelize from 'sequelize';
import * as moment from 'moment';

import { Sequelize } from 'sequelize-typescript';
import logger from 'utils/logger';
import { PaginationMeta } from 'utils/types/pagination';
import { UtilsService } from 'utils/utils.service';

import {
  DashboardFilterDTO,
  getNumberOfDaysOfAssetsFilter,
  StockCountFilter,
  SurveyPerformanceFilter,
  WarehouseTopAssetFilter,
} from './dto';
import { SurveyPerformanceRecords } from './types';

@Injectable()
export class AnalyticsService {
  constructor(
    private sequelize: Sequelize,

    private warehouseService: WareHouseService,

    private utilsService: UtilsService,

    private assetService: AssetService,
  ) {}

  async getWarehousesWithTopAssets(
    user: userAuthDetails,
    filter?: WarehouseTopAssetFilter,
  ) {
    logger.info('Start retrieving top assets');

    const results = (await this.warehouseService.topAssets(
      +filter.count,
      user.companyId,
    )) as any;

    const assetsInWarehouse =
      await this.assetService.countAllAssetsInWarehouses(user.companyId);

    const response = results.map(({ dataValues }) => {
      const {
        id,
        name,
        state,
        country,
        assetsCount: countOfAssets,
      } = dataValues;

      const contribution =
        this.utilsService.convertToPercentage(
          +countOfAssets,
          assetsInWarehouse,
        ) || 0;

      return {
        id,
        name,
        state,
        country,
        assetsCount: +countOfAssets,
        contribution,
      };
    });

    logger.info('Completes top assets retrieval');

    return response;
  }

  async getNumberOfDaysOfAssets(
    user: userAuthDetails,
    filter: getNumberOfDaysOfAssetsFilter,
  ) {
    logger.info('start retrieving number of days against number of assets');
    const query = this._prepareDayMonthQuery(user, filter);

    const records = await this.sequelize.query(query, {
      type: sequelize.QueryTypes.SELECT,
    });

    logger.info('completes retrieving number of days against number of assets');

    return records;
  }

  async getStockCountReport(
    user: userAuthDetails,
    filter: StockCountFilter,
  ): Promise<[Asset[], PaginationMeta]> {
    logger.info('start retrieving stock count');

    const [assets, PaginationMeta] =
      await this.assetService.getStockCountReport(user.companyId, filter);

    logger.info('done retrieving stock count report');

    return [assets, PaginationMeta];
  }

  async getSurveyPerformance(
    user: userAuthDetails,
    filter: SurveyPerformanceFilter,
  ): Promise<SurveyPerformanceRecords[]> {
    logger.info('start retrieving survey performance');

    const records = (await this.sequelize.query(
      `
      SELECT surveys.id, surveys.title, surveys.content, COUNT(survey_responses.response) as numberOfResponse 
      FROM survey_responses, surveys 
      WHERE survey_responses.survey_id = surveys.id AND surveys.company_id = '${user.companyId}'
      AND survey_responses.created_at BETWEEN '${filter.from}' AND '${filter.to}' 
      GROUP BY surveys.id
    `,
      {
        type: sequelize.QueryTypes.SELECT,
      },
    )) as SurveyPerformanceRecords[];

    logger.info('completes retrieving survey performance');

    return records;
  }

  private _prepareDayMonthQuery(user, filter) {
    const fromDate = moment(filter.from);
    const toDate = moment(filter.to);
    let daysCount = Math.abs(fromDate.diff(toDate, 'days'));
    daysCount++;
    if (daysCount < 60) {
      return `
    SELECT COUNT(*) as assetsCount, CONCAT(YEAR(created_at),'/',MONTH(created_at),'/',DAY(created_at)) as date
    FROM assets 
    WHERE company_id = '${user.companyId}'
    AND created_at >= '${filter.from}'
    AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
    GROUP BY date 
    ORDER BY created_at ASC
  `;
    } else {
      return `
    SELECT COUNT(*) as assetsCount, CONCAT(YEAR(created_at),'/',MONTH(created_at)) as date
    FROM assets 
    WHERE company_id = '${user.companyId}'
    AND created_at >= '${filter.from}'
    AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
    GROUP BY date 
    ORDER BY created_at ASC
  `;
    }
  }

  async getTotalAssetCountByMonth(
    user: userAuthDetails,
    filter: DashboardFilterDTO,
  ) {
    const fromDate = moment(filter.from);
    const toDate = moment(filter.to);
    let daysCount = Math.abs(fromDate.diff(toDate, 'days'));

    if (filter?.assetName || filter?.warehouseId) {
      if (filter?.assetName && !filter?.warehouseId) {
        return this._getAssetOnlyReport(filter, daysCount, user);
      } else if (!filter?.assetName && filter?.warehouseId) {
        return this._getAssetByWarehouseId(filter, daysCount, user);
      } else {
        return this._getAssetByWarehouseIdAndAssetId(filter, daysCount, user);
      }
    }

    if (daysCount < 60) {
      //for daily grouping
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, CONCAT(YEAR(created_at),'/',MONTH(created_at),'/',DAY(created_at)) as date, DATE_FORMAT(created_at, '%b %e') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];

      return records;
    } else {
      //monthly groupings
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, CONCAT(YEAR(created_at),'/',MONTH(created_at)) as date, DATE_FORMAT(created_at, '%b') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];

      return records;
    }
  }

  async getTotalAssetCountByType(
    user: userAuthDetails,
    filter: DashboardFilterDTO,
  ) {
    const fromDate = moment(filter.from);
    const toDate = moment(filter.to);
    let daysCount = Math.abs(fromDate.diff(toDate, 'days'));
    daysCount++;
    if (filter.assetName || filter.warehouseId) {
      if (filter.assetName) {
        return null;
      } else {
        return this._getTotalAssetCountByTypeInWareHouse(
          filter,
          daysCount,
          user,
        );
      }
    }
    if (daysCount < 60) {
      //for days count
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, name, CONCAT(YEAR(created_at),'/',MONTH(created_at),'/',DAY(created_at)) as date, DATE_FORMAT(created_at, '%b %e') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date, name 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];
      logger.info('completes retrieving ...');

      return records;
    } else {
      //for months grouping
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, name, CONCAT(YEAR(created_at),'/',MONTH(created_at)) as date, DATE_FORMAT(created_at, '%b') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date, name 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];
      logger.info('completes retrieving ...');
      return records;
    }
  }

  private async _getAssetOnlyReport(
    filter: DashboardFilterDTO,
    daysCount: number,
    user: userAuthDetails,
  ) {
    if (daysCount < 60) {
      //for daily grouping
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, CONCAT(YEAR(created_at),'/',MONTH(created_at),'/',DAY(created_at)) as date, DATE_FORMAT(created_at, '%b %e') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND name = '${filter.assetName}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];

      return records;
    } else {
      //monthly groupings
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, CONCAT(YEAR(created_at),'/',MONTH(created_at)) as date, DATE_FORMAT(created_at, '%b') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND name = '${filter.assetName}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];

      return records;
    }
  }

  private async _getAssetByWarehouseId(
    filter: DashboardFilterDTO,
    daysCount: number,
    user: userAuthDetails,
  ) {
    if (daysCount < 60) {
      //for daily grouping
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, CONCAT(YEAR(created_at),'/',MONTH(created_at),'/',DAY(created_at)) as date, DATE_FORMAT(created_at, '%b %e') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND warehouse_id = '${filter.warehouseId}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];

      return records;
    } else {
      //monthly groupings
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, CONCAT(YEAR(created_at),'/',MONTH(created_at)) as date, DATE_FORMAT(created_at, '%b') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND warehouse_id = '${filter.warehouseId}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];

      return records;
    }
  }

  private async _getAssetByWarehouseIdAndAssetId(
    filter: DashboardFilterDTO,
    daysCount: number,
    user: userAuthDetails,
  ) {
    if (daysCount < 60) {
      //for daily grouping
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, CONCAT(YEAR(created_at),'/',MONTH(created_at),'/',DAY(created_at)) as date, DATE_FORMAT(created_at, '%b %e') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND warehouse_id = '${filter.warehouseId}'
      AND name = '${filter.assetName}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];

      return records;
    } else {
      //monthly groupings
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, CONCAT(YEAR(created_at),'/',MONTH(created_at)) as date, DATE_FORMAT(created_at, '%b') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND warehouse_id = '${filter.warehouseId}'
      AND name = '${filter.assetName}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];

      return records;
    }
  }

  private async _getTotalAssetCountByTypeInWareHouse(
    filter: DashboardFilterDTO,
    daysCount: number,
    user: userAuthDetails,
  ) {
    if (daysCount < 60) {
      //for days count
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, name, CONCAT(YEAR(created_at),'/',MONTH(created_at),'/',DAY(created_at)) as date, DATE_FORMAT(created_at, '%b %e') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND warehouse_id = '${filter.warehouseId}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date, name 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];
      logger.info('completes retrieving ...');

      return records;
    } else {
      //for months grouping
      const records = (await this.sequelize.query(
        `
      SELECT COUNT(*) as assetsCount, name, CONCAT(YEAR(created_at),'/',MONTH(created_at)) as date, DATE_FORMAT(created_at, '%b') as formattedDate
      FROM assets 
      WHERE company_id = '${user.companyId}'
      AND warehouse_id = '${filter.warehouseId}'
      AND created_at >= '${filter.from}'
      AND created_at < DATE_ADD('${filter.to}', INTERVAL 1 DAY)
      GROUP BY date, name 
      ORDER BY created_at ASC
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];
      logger.info('completes retrieving ...');
      return records;
    }
  }
}
