export class SurveyPerformanceRecords {
  id: string;
  title: string;
  content: string;
  numberOfResponse: string;
}
