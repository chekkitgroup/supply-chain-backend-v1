export const plans = [
  {
    type: 'Basic Plan',
    description: '20 tags & 1 gateway reader',
    price: 1.5,
    numberOfYears: 1,
    featuresList: JSON.stringify([
      'dashboard.view',
      'assets.view',
      'assets.manage',
      'warehouses.view',
      'warehouses.manage',
      'surveys.view',
      'surveys.manage',
      'rewards.view',
      'rewards.manage',
      'insights.view',
    ]),
  },

  {
    type: 'Premium Plan',
    description: '100 tags & 1 gateway reader',
    price: 5,
    numberOfYears: 1,
    featuresList: JSON.stringify([
      'dashboard.view',
      'assets.view',
      'assets.manage',
      'warehouses.view',
      'warehouses.manage',
      'surveys.view',
      'surveys.manage',
      'rewards.view',
      'rewards.manage',
      'reports.view',
      'insights.view',
    ]),
  },

  {
    type: 'Entreprise Plan',
    description: '300 tags & 4 gateway reader',
    price: 15,
    numberOfYears: 1,
    featuresList: JSON.stringify([
      'dashboard.view',
      'assets.view',
      'assets.manage',
      'warehouses.view',
      'warehouses.manage',
      'surveys.view',
      'surveys.manage',
      'rewards.view',
      'rewards.manage',
      'insights.view',
      'reports.view',
      'rfids.view',
      'rfids.manage',
    ]),
  },
];

export const features = [
  {
    name: 'dashboard.view',
    code: 10,
    description: 'viewing dashboard',
  },
  {
    name: 'assets.view',
    code: 20,
    description: 'viewing assets',
  },
  {
    name: 'assets.manage',
    code: 21,
    description: 'onboarding assets',
  },
  {
    name: 'warehouses.view',
    code: 30,
    description: 'viewing fields',
  },
  {
    name: 'warehouses.manage',
    code: 31,
    description: 'adding fields',
  },
  {
    name: 'surveys.view',
    code: 40,
    description: 'adding surveys',
  },
  {
    name: 'surveys.manage',
    code: 41,
    description: 'delete surveys',
  },
  {
    name: 'rewards.view',
    code: 50,
    description: 'adding rewards',
  },
  {
    name: 'rewards.manage',
    code: 51,
    description: 'removing rewards',
  },
  {
    name: 'reports.view',
    code: 60,
    description: 'viewing reports',
  },
  {
    name: 'insights.view',
    code: 70,
    description: 'viewing insights',
  },
  {
    name: 'rfids.view',
    code: 80,
    description: 'viewing rfids',
  },
  {
    name: 'rfids.manage',
    code: 81,
    description: 'assigning rfids',
  },
];

export const featureCategory = [
  {
    name: 'dashboard',
  },
  {
    name: 'assets',
  },
  {
    name: 'warehouses',
  },
  {
    name: 'surveys',
  },
  {
    name: 'rewards',
  },
  {
    name: 'reports',
  },
  {
    name: 'insights',
  },
  {
    name: 'rfids',
  },
];
