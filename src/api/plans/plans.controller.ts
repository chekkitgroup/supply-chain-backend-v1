import {
  Controller,
  Get,
  Param,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'api/auth/jwt-auth.guard';

import { RequestWithUser } from 'api/auth/types';

import { successResponse, successResponseSpec } from 'utils/response';
import { PlansOutputDto } from './dtos/response.dto';
import { PlansService } from './plans.service';

@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@ApiTags('Plans')
@Controller('plans')
export class PlansController {
  constructor(private readonly _planService: PlansService) {}

  @ApiOperation({ summary: 'Retrieves Chekkit Plans' })
  @ApiResponse({
    status: 201,
    description: 'Plans retrieved successfully',
  })
  @Get('')
  async getPlans(): Promise<successResponseSpec> {
    const plans = await this._planService.getAll();

    const plansOutput: PlansOutputDto[] = plans?.map((plan) =>
      PlansOutputDto.FromPlansOutput(plan),
    );

    return successResponse('Plans retrieved successfully', 200, plansOutput);
  }

  @ApiOperation({ summary: 'Subscribe to plan' })
  @ApiResponse({
    status: 201,
    description: 'Plans choosed and subscribed successfully',
  })
  @Post(':id/subscribe')
  async choosePlan(
    @Request() req: RequestWithUser,
    @Param('id') planId: string,
  ): Promise<successResponseSpec> {
    await this._planService.subscribePlan(req.user, planId);

    return successResponse('Plans subscribed successfully', 201);
  }
}
