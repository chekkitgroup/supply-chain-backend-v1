import { UUIDV4 } from 'sequelize';
import {
  Column,
  BelongsTo,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  BelongsToMany,
} from 'sequelize-typescript';

import FeatureCategory from './featureCategory.model';
import FeaturePlan from './featurePlan';
import Plan from './plan.model';

@Table
class Feature extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING(50), allowNull: false })
  name: string;

  @Column({ type: DataType.TEXT })
  description: string;

  @Column({ type: DataType.INTEGER, allowNull: false })
  code: number;

  @BelongsTo(() => FeatureCategory, 'categoryId')
  featureCategory: FeatureCategory;

  @BelongsToMany(() => Plan, () => FeaturePlan)
  plans: Plan[];
}

export default Feature;
