import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  BelongsToMany,
} from 'sequelize-typescript';
import { PlanType } from '../types';
import Feature from './feature.model';
import FeaturePlan from './featurePlan';

@Table
class Plan extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({
    type: DataType.ENUM('Basic Plan', 'Premium Plan', 'Entreprise Plan'),
    allowNull: false,
  })
  type: PlanType;

  @Column({ type: DataType.TEXT, allowNull: false })
  description: string;

  @Column({
    type: DataType.FLOAT,
    allowNull: false,
  })
  price: number;

  @Column({
    type: DataType.INTEGER,
  })
  numberOfYears: number;

  @BelongsToMany(() => Feature, () => FeaturePlan)
  features: Feature[];
}
export default Plan;
