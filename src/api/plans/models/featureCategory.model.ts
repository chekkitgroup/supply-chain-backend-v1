import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  HasMany,
} from 'sequelize-typescript';
import Feature from './feature.model';

@Table
class FeatureCategory extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING(50), allowNull: false })
  name: string;

  @HasMany(() => Feature, 'categoryId')
  features: Feature[];
}

export default FeatureCategory;
