import { UUIDV4 } from 'sequelize';
import {
  Column,
  ForeignKey,
  IsUUID,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import Feature from './feature.model';
import Plan from './plan.model';

@Table
class FeaturePlan extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @ForeignKey(() => Feature)
  @Column
  featureId: string;

  @ForeignKey(() => Plan)
  @Column
  planId: string;
}

export default FeaturePlan;
