import {
  ConflictException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import { InjectModel } from '@nestjs/sequelize';
import { AdminService } from 'api/admin/admin.service';

import { userAuthDetails } from 'api/auth/types';

import { SubscriptionService } from 'api/subscription/subscription.service';
import {
  SubscriptionChannel,
  SubscriptionStatus,
} from 'api/subscription/types';

import logger from 'utils/logger';

import Feature from './models/feature.model';

import Plan from './models/plan.model';

@Injectable()
export class PlansService {
  constructor(
    @InjectModel(Plan) private readonly planRepository: typeof Plan,

    @InjectModel(Feature) private readonly featureRepository: typeof Feature,

    @Inject(forwardRef(() => AdminService))
    private readonly adminService: AdminService,

    private readonly subscriptionService: SubscriptionService,
  ) {}

  async findOne(id: string): Promise<Plan> {
    logger.info(`About to get a  plan with id : ${id}`);
    return await this.planRepository.findOne({
      where: { id },
      include: ['features'],
    });
  }

  async getAll(): Promise<Plan[]> {
    logger.info('get all available plans');

    return this.planRepository.findAll();
  }

  async subscribePlan(user: userAuthDetails, id: string) {
    logger.info('start subscribing to plans');

    const plan = await this.planRepository.findOne({
      where: { id },
    });

    if (!plan) throw new NotFoundException('Plan does not exist');

    const pendingsubScriptionRequest = await this.subscriptionService.getSubLog(
      user.companyId,
    );

    if (pendingsubScriptionRequest)
      throw new ConflictException('you currently having a pending request');

    const subscriptionEndDate = new Date();

    const activeSubscription =
      await this.subscriptionService.getActiveSubDetails(user.companyId);

    if (activeSubscription)
      throw new ConflictException(
        'you currently having an active subscription',
      );

    subscriptionEndDate.setFullYear(
      subscriptionEndDate.getFullYear() + plan.numberOfYears,
    );

    const subscriptionLogsData = {
      price: plan.price,
      planId: plan.id,
      companyId: user.companyId,
      startDate: new Date(),
      endDate: subscriptionEndDate,
      status: SubscriptionStatus.Pending,
      channel: SubscriptionChannel.Admin,
    };

    const subLog = await this.subscriptionService.log(subscriptionLogsData);

    const isDev = process.env.NODE_ENV !== 'production';

    //approve subscription for in development environment.
    if (isDev) {
      await this.adminService.approveSubscription(user.companyId, subLog.id);
    }
  }

  async getPlansFeatures(): Promise<Feature[]> {
    const features = await this.featureRepository.findAll();

    return features;
  }
}
