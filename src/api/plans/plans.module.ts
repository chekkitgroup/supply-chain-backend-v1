import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { PlansController } from './plans.controller';
import { PlansService } from './plans.service';
import Feature from './models/feature.model';
import FeatureCategory from './models/featureCategory.model';
import Plan from './models/plan.model';
import { SubscriptionModule } from 'api/subscription/subscription.module';
import FeaturePlan from './models/featurePlan';
import { UsersModule } from 'api/users/users.module';
import { AdminModule } from 'api/admin/admin.module';

@Module({
  imports: [
    SequelizeModule.forFeature([Feature, FeatureCategory, Plan, FeaturePlan]),
    SubscriptionModule,

    forwardRef(() => UsersModule),
    forwardRef(() => AdminModule),
  ],
  controllers: [PlansController],
  providers: [PlansService],
  exports: [PlansService],
})
export class PlansModule {}
