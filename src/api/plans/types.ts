export enum PlanType {
  Basic = 'Basic Plan',
  Premium = 'Premium Plan',
  Entreprise = 'Entreprise Plan',
}
