import Plan from '../models/plan.model';

export class PlansOutputDto {
  id: string;
  type: string;
  description: string;
  price: string;
  numberOfYears: number;
  created_at: Date;
  deleted_at: Date;
  updated_at: Date;

  static FromPlansOutput(plan: Plan) {
    const output = new PlansOutputDto();

    const exposedKeys = ['id', 'type', 'description', 'price', 'numberOfYears'];

    exposedKeys.forEach((exposedKey) => {
      output[exposedKey] = plan[exposedKey];

      if (exposedKey === 'price' && plan.price) {
        output[exposedKey] = `₦${plan.price}m`;
      }
    });

    return output;
  }
}
