import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import logger from 'utils/logger';
import { CreateNotificationDTO } from './dto';
import Notification from './model/notification.model';

@Injectable()
export class NotificationService {
  constructor(
    @InjectModel(Notification)
    private readonly notificationRepository: typeof Notification,
  ) {}

  async create(data: CreateNotificationDTO): Promise<void> {
    logger.info(
      `about to create notification ${data.title} for ${data.userId}`,
    );
    await this.notificationRepository.create(data);
  }

  async getUserNotifications(userId: string) {
    return this.notificationRepository.findAll({
      where: { userId },
      order: [
        ['isRead', 'ASC'],
        ['created_at', 'DESC'],
      ],
      limit: 100,
      attributes: ['id', 'content', 'title', 'isRead', 'created_at'],
    });
  }

  async updateStatus(id: string): Promise<void> {
    await this.notificationRepository.update(
      { isRead: true },
      { where: { id } },
    );
  }
}
