import User from 'api/users/models/user.model';
import { UUIDV4 } from 'sequelize';
import {
  Column,
  Model,
  DataType,
  PrimaryKey,
  IsUUID,
  ForeignKey,
  Table,
  HasMany,
  BelongsTo,
} from 'sequelize-typescript';

@Table
class Notification extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({
    type: DataType.TEXT,
    allowNull: false,
  })
  content: string;

  @Column({
    type: DataType.STRING(100),
    allowNull: false,
  })
  title: string;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  isRead: boolean;

  @ForeignKey(() => User)
  @Column
  userId: string;

  @BelongsTo(() => User, 'userId')
  user: User;
}

export default Notification;
