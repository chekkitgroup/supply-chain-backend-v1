import {
  Controller,
  Get,
  Inject,
  Param,
  ParseUUIDPipe,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'api/auth/jwt-auth.guard';
import { RequestWithUser } from 'api/auth/types';
import { successResponse } from 'utils/response';
import { NotificationService } from './notification.service';

@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@ApiTags('Notifications')
@Controller('notifications')
export class NotificationController {
  constructor(
    @Inject(NotificationService)
    private readonly notificationService: NotificationService,
  ) {}

  @Get()
  async getUserNotifications(@Req() req: RequestWithUser): Promise<any> {
    const response = await this.notificationService.getUserNotifications(
      req.user.id,
    );
    return successResponse(
      'Notification(s) retrieved successfully',
      200,
      response,
      true,
    );
  }

  @Put('/:notificationId')
  async updateNotification(
    @Param('notificationId', ParseUUIDPipe) notificationId: string,
  ) {
    await this.notificationService.updateStatus(notificationId);
    return successResponse(
      'Notification(s) status updated successfully',
      200,
      null,
      true,
    );
  }
}
