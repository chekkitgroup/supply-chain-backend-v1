export class CreateNotificationDTO {
  content: string;
  title: string;
  userId: string;
}
