import Company from './models/company.model';
import User from './models/user.model';

export enum Identifiers {
  Manufacturer = 'manufacturer',
  Distributor = 'distributor',
  Retailer = 'retailer',
}

export interface UserExtension extends User {
  setCompany: (company: Company) => void;
  companyId: string;
  companyName: string;
}
