import { UUIDV4 } from 'sequelize';
import {
  Column,
  ForeignKey,
  IsUUID,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import Permission from './permission.model';
import Role from './role.model';

@Table
class RolePermission extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @ForeignKey(() => Role)
  @Column
  roleId: string;

  @ForeignKey(() => Permission)
  @Column
  permissionId: string;
}

export default RolePermission;
