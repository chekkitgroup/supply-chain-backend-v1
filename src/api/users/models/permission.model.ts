import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  BelongsToMany,
} from 'sequelize-typescript';
import Role from './role.model';
import RolePermission from './rolePermission.model';

@Table
class Permission extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({
    type: DataType.STRING(50),
    allowNull: false,
  })
  group: string;

  @Column({
    type: DataType.STRING(100),
    allowNull: false,
  })
  identifier: string;

  @BelongsToMany(() => Role, () => RolePermission)
  roles: Role[];
}

export default Permission;
