import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  BelongsTo,
  BelongsToMany,
  ForeignKey,
} from 'sequelize-typescript';
import Company from './company.model';
import Permission from './permission.model';
import RolePermission from './rolePermission.model';

@Table
class Role extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({
    type: DataType.STRING(100),
    allowNull: false,
  })
  name: string;

  @Column({
    type: DataType.BOOLEAN,
  })
  system: boolean;

  @ForeignKey(() => Company)
  companyId: Company;

  @BelongsTo(() => Company, 'companyId')
  company: Company;

  @BelongsToMany(() => Permission, () => RolePermission)
  permissions: Permission[];
}

export default Role;
