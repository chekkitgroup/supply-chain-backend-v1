import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  BelongsTo,
  Model,
  IsEmail,
  DataType,
  ForeignKey,
  BeforeCreate,
} from 'sequelize-typescript';

import Company from './company.model';
import Role from './role.model';

import { UUIDV4 } from 'sequelize';
import { hashPassword } from 'utils/bcrypt';

@Table
class User extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING(50) })
  name: string;

  @IsEmail
  @Column({
    type: DataType.STRING(50),
    unique: {
      name: 'email',
      msg: 'An account already exists with this email',
    },
  })
  email: string;

  @Column({
    type: DataType.STRING(100),
  })
  password: string;

  @Column({
    type: DataType.STRING(100),
  })
  companyRole: string;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  isVerified: boolean;

  @ForeignKey(() => Role)
  @Column
  roleId: string;

  @BelongsTo(() => Company, 'companyId')
  company: Company;

  @BelongsTo(() => Role, 'roleId')
  role: Role;

  @Column({
    type: DataType.TEXT,
  })
  temporaryPermissions: string;

  @BeforeCreate
  static hashPassword(instance: User) {
    instance.password = hashPassword(instance.password);
  }
}
export default User;
