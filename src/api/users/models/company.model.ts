import {
  Column,
  ForeignKey,
  HasMany,
  HasOne,
  IsUUID,
  PrimaryKey,
  Table,
  BelongsToMany,
  Model,
  DataType,
} from 'sequelize-typescript';

import User from './user.model';
import { UUIDV4 } from 'sequelize';
import WareHouse from 'api/warehouse/models/warehouse.model';
import HardwareOrder from 'api/hardware/models/hardwareOrder.model';
import Rfid from 'api/rfid/models/rfid.model';
import Subscription from 'api/subscription/models/subscription.model';
import Plan from 'api/plans/models/plan.model';
import { Identifiers } from '../types';
import Asset from 'api/asset/models/asset.model';
import Survey from 'api/survey/models/survey.model';
import Site from 'api/site/models/site.model';
import Project from 'api/warehouse/models/project.model';
import AppModule from 'api/appmodule/models/appmodule.model';
import CompanyAppModule from './companyAppModule.model';

@Table
class Company extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING(50), allowNull: false })
  name: string;

  @Column({ type: DataType.STRING(200) })
  address: string;

  @Column({ type: DataType.STRING(50) })
  country: string;

  @Column({ type: DataType.ENUM('manufacturer', 'distributor', 'retailer') })
  identifier: Identifiers;

  @Column({
    type: DataType.INTEGER,
  })
  totalAssets: number;

  @Column({
    type: DataType.INTEGER,
  })
  totalHardwares: number;

  @Column({
    type: DataType.INTEGER,
  })
  totalSurveys: number;

  @Column({
    type: DataType.INTEGER,
  })
  totalWarehouses: number;

  @Column({
    type: DataType.INTEGER,
  })
  totalSites: number;

  @ForeignKey(() => User)
  @Column
  userId: string;

  @ForeignKey(() => Plan)
  @Column
  planId: string;

  @HasMany(() => User, 'companyId')
  users: User[];

  @HasMany(() => WareHouse, 'companyId')
  warehouse: WareHouse[];

  @HasMany(() => Site, 'companyId')
  sites: Site[];

  @HasMany(() => HardwareOrder, 'companyId')
  HardwareOrders: HardwareOrder[];

  @HasMany(() => Rfid, 'companyId')
  Rfids: Rfid[];

  @HasOne(() => Subscription, 'companyId')
  subscription: Subscription;

  @HasMany(() => Asset, 'companyId')
  assets: Asset[];

  @HasMany(() => Survey, 'companyId')
  surveys: Survey[];

  @HasMany(() => Project, 'companyId')
  project: Project;

  @BelongsToMany(() => AppModule, () => CompanyAppModule)
  appModules: AppModule[];
}

export default Company;
