import AppModule from 'api/appmodule/models/appmodule.model';
import { UUIDV4 } from 'sequelize';
import {
  Column,
  ForeignKey,
  IsUUID,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';
import Company from './company.model';

@Table
class CompanyAppModule extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @ForeignKey(() => Company)
  @Column
  companyId: string;

  @ForeignKey(() => AppModule)
  @Column
  appModuleId: string;
}

export default CompanyAppModule;
