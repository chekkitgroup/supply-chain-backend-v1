import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  IsEmail,
  DataType,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';

import { UUIDV4 } from 'sequelize';
import User from './user.model';
import Company from './company.model';

@Table
class UserInvite extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @IsEmail
  @Column({
    type: DataType.STRING(50),
    unique: {
      name: 'email',
      msg: 'An account already exists with this email',
    },
  })
  email: string;

  @Column({
    type: DataType.STRING(100),
  })
  companyRole: string;

  @Column({
    type: DataType.STRING(10),
    allowNull: true
  })
  invitationCode: string;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  accepted: boolean;

  @ForeignKey(() => User)
  inviterId: string;

  @ForeignKey(() => Company)
  companyId: string;

  @BelongsTo(() => User, 'inviterId')
  invitedBy: User;

  @BelongsTo(() => Company, 'companyId')
  company: Company;
}
export default UserInvite;
