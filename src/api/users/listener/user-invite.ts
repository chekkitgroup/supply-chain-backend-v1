import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import { UserInviteEmailTemplate } from 'utils/constant';
import sendEmail from 'utils/email';
import logger from 'utils/logger';
import { UserInviteEvent } from '../event/user-invite';

@Injectable()
export class UserEvent {
  @OnEvent('user.invite')
  handleUserInviteCreatedEvent(event: UserInviteEvent) {
    this.sendUserInviteEmail(event.email, event.token);
  }

  async sendUserInviteEmail(email: string, token: string): Promise<boolean> {
    const link = `${process.env.SIGNUP_INVITATION_URL}/${token}`;

    const emailContent = UserInviteEmailTemplate(link);

    logger.info(
      `About to send invite link sent for user with details ${JSON.stringify(
        email,
      )}`,
    );

    const emailSent = await sendEmail(
      email,
      'Invitation to Signup on Chekkit Supply Chain',
      emailContent,
    );

    logger.info(`Result from email sending ${JSON.stringify(emailSent)}`);

    return emailSent;
  }
}
