import { Module } from '@nestjs/common';

import { UsersModule } from '../users.module';
import { IsUserAlreadyExistConstraint } from './user.validation';

@Module({
  imports: [UsersModule],
  providers: [IsUserAlreadyExistConstraint],
})
export class ValidationModule {}
