import {
  ConflictException,
  Injectable,
  UnprocessableEntityException,
} from '@nestjs/common';

import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

import { UsersService } from '../users.service';

@ValidatorConstraint({ async: true })
@Injectable()
export class IsUserAlreadyExistConstraint
  implements ValidatorConstraintInterface
{
  constructor(private readonly userService: UsersService) {}
  async validate(email: string) {
    const user = await this.userService.findOne({ email });

    if (user) throw new ConflictException('User already exist');

    return true;
  }
}

export function IsUserAlreadyExist(validationOptions?: ValidationOptions) {
  return function (object: unknown, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsUserAlreadyExistConstraint,
    });
  };
}

@ValidatorConstraint({ async: true })
@Injectable()
export class EmailValidator implements ValidatorConstraintInterface {
  async validate(value: any, args: ValidationArguments) {
    const [relatedPropertyName] = args.constraints;
    const relatedValue: string = (args.object as any)[relatedPropertyName];

    const proposedSuffix = relatedValue.toLocaleLowerCase().split(' ');

    const suffix = value.split('@')[1].split('.')[0].toLowerCase();

    if (
      !(
        proposedSuffix.includes(suffix) ||
        proposedSuffix.some((companyNameSuffix) =>
          suffix.includes(companyNameSuffix),
        )
      )
    )
      throw new UnprocessableEntityException('Invalid company email');

    return true;
  }
}

export function EmailValidation(
  property: string,
  validationOptions?: ValidationOptions,
) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'EmailValidator',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [property],
      options: validationOptions,
      validator: EmailValidator,
    });
  };
}
