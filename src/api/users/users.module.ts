import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import Company from './models/company.model';
import Role from './models/role.model';
import User from './models/user.model';
import { UsersService } from './users.service';
import { CompanyService } from './company/company.service';
import { UsersController } from './users.controller';
import { RoleService } from './role/role.service';
import Permission from './models/permission.model';
import RolePermission from './models/rolePermission.model';

import { PlansModule } from 'api/plans/plans.module';
import { UserEvent } from './listener/user-invite';
import { RoleController } from './role/role.controller';
import { AdminModule } from 'api/admin/admin.module';
import UserInvite from './models/user-invite.model';
import { AuthModule } from 'api/auth/auth.module';
import { SurveyModule } from 'api/survey/survey.module';
import { AssetModule } from 'api/asset/asset.module';
import { HardwareModule } from 'api/hardware/hardware.module';
import { SubscriptionModule } from 'api/subscription/subscription.module';
import CompanyAppModule from './models/companyAppModule.model';

@Module({
  imports: [
    SequelizeModule.forFeature([
      User,
      UserInvite,
      Company,
      Role,
      Permission,
      RolePermission,
      CompanyAppModule,
    ]),
    forwardRef(() => AuthModule),
    forwardRef(() => PlansModule),
    forwardRef(() => AdminModule),
    forwardRef(() => SurveyModule),

    AssetModule,
    HardwareModule,
    SubscriptionModule,
  ],

  providers: [UsersService, CompanyService, RoleService, UserEvent],
  exports: [UsersService, CompanyService, RoleService],
  controllers: [UsersController, RoleController],
})
export class UsersModule {}
