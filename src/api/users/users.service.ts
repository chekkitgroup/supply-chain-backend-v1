import {
  ConflictException,
  ForbiddenException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { EventEmitter2 } from '@nestjs/event-emitter';

import { userAuthDetails } from 'api/auth/types';

import logger from 'utils/logger';
import { objectResponse } from 'utils/response';

import {
  AssignPermissionsDto,
  CreateInvitedUserDto,
  CreateUserDto,
  CreateUserInviteDto,
  CreateAgentInviteDto,
} from './dtos/user.dto';
import Company from './models/company.model';

import User from './models/user.model';
import { UserExtension } from './types';

import { PlansService } from 'api/plans/plans.service';
import { UserInviteEvent } from './event/user-invite';
import { RoleService } from './role/role.service';
import { AdminService } from 'api/admin/admin.service';
import { AccountLoginOutputDto } from 'api/auth/dto/response.dto';
import UserInvite from './models/user-invite.model';
import { AuthService } from 'api/auth/auth.service';
import { getOffSet, getPaginationMetaData } from 'utils/pagination';
import { PaginationMeta } from 'utils/types/pagination';

import { UserWithAdminField } from './dtos/response.dto';
import { SubscriptionService } from 'api/subscription/subscription.service';
import AppModule from 'api/appmodule/models/appmodule.model';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User) private readonly userRepository: typeof User,
    @InjectModel(UserInvite)
    private readonly userInviteRepository: typeof UserInvite,
    @InjectModel(Company) private readonly companyRepository: typeof Company,

    private readonly roleService: RoleService,
    private readonly planService: PlansService,
    private readonly adminService: AdminService,
    @Inject(forwardRef(() => AuthService))
    private readonly authService: AuthService,

    private readonly subscriptionService: SubscriptionService,

    private eventEmitter: EventEmitter2,
  ) {}

  async createUser(userData: CreateUserDto): Promise<User> {
    logger.info(
      `About to create User with user data,
          ${JSON.stringify(userData)}`,
    );

    const userDetail: Partial<User> = {
      name: userData.name,
      email: userData.email,
      password: userData.password,
      companyRole: userData.companyRole,
      roleId: userData.roleId,
    };

    const user = this.userRepository.create(userDetail);

    return user;
  }

  async findOne(details: objectResponse): Promise<User> {
    logger.info(
      `About to find User with details,
        ${JSON.stringify(details)}`,
    );

    const user = await this.userRepository.findOne({
      where: {
        ...details,
      },
      include: ['company'],
    });

    return user;
  }

  async findOneUserInvite(details: objectResponse): Promise<UserInvite> {
    logger.info(
      `About to find User in User Invite with details,
          ${JSON.stringify(details)}`,
    );

    const user = await this.userInviteRepository.findOne({
      where: {
        ...details,
      },
    });

    return user;
  }

  async update(
    updateConditions: objectResponse,
    updateValues: objectResponse,
  ): Promise<void> {
    logger.info(
      `About to Update User with Details,
        ${JSON.stringify(updateConditions)} ${JSON.stringify(updateValues)}`,
    );

    await this.userRepository.update(
      {
        ...updateValues,
      },
      {
        where: {
          ...updateConditions,
        },
      },
    );
  }

  async InviteUser(
    userInviteDto: CreateUserInviteDto,
    authDetails: userAuthDetails,
  ) {
    logger.info(`About to check whether user is the admin of a company `);

    const role = await this.roleService.getAdminRole();

    if (role.id !== authDetails.roleId)
      throw new ForbiddenException('You are allowed to perform this operation');

    logger.info(
      `About to get user details with id: ${JSON.stringify(authDetails.id)}`,
    );

    const user = await this.userRepository.findOne({
      where: { id: authDetails.id },
      include: Company,
    });

    if (!user) throw new NotFoundException('user does not exist');

    logger.info(
      `Checking if user company matches using id : ${JSON.stringify(
        authDetails.companyId,
      )}`,
    );

    if (user.company.id !== authDetails.companyId)
      throw new UnauthorizedException('user company details do not match');

    logger.info(
      `About to check whether invitee email is in sync with company's admin email`,
    );

    const userEmailSuffix = user.email.split('@')[1].toLowerCase();

    const invitedUserEmailSuffix = userInviteDto.email
      .split('@')[1]
      .toLowerCase();

    if (userEmailSuffix !== invitedUserEmailSuffix)
      throw new UnauthorizedException('Invalid company email');

    const invitedUser = await this.userInviteRepository.findOne({
      where: { email: userInviteDto.email },
    });

    let newInvitedUser: any;

    if (!invitedUser) {
      const invitedUserDetails: Partial<UserInvite> = {
        email: userInviteDto.email,
        companyRole: userInviteDto.companyRole,
        inviterId: authDetails.id,
        companyId: authDetails.companyId,
      };

      newInvitedUser = await this.userInviteRepository.create(
        invitedUserDetails,
      );
    }

    if (invitedUser && invitedUser.accepted) {
      throw new ConflictException('User has been invited already');
    }

    const invitedUserId =
      invitedUser && !invitedUser?.accepted
        ? invitedUser.id
        : newInvitedUser.dataValues.id;

    const token = await this.authService.generateToken(
      {
        id: invitedUserId,
        inviteeId: user.id,
      },
      '7d',
    );

    const userInvite: UserInviteEvent = {
      email: userInviteDto.email,
      token,
    };

    this.eventEmitter.emit('user.invite', userInvite);
  }

  async InviteAgent(agentInviteDto: CreateAgentInviteDto) {
    logger.info(`About to check whether user is the admin of a company `);
    // to do: check if user account already exists
    // check if email is in users table
    // check if user has already been invited
    // if invited and not accepted, retrieve and send code
    // if invited and accepted, inform that user already invited
    // if not invited , create invitation and send email

    // const existingUser = await this.findOne({ email: agentInviteDto.email });

    // if (existingUser) throw new ConflictException('User already exists');

    const invitedUser = await this.userInviteRepository.findOne({
      where: { email: agentInviteDto.email },
    });
    if (!invitedUser) throw new NotFoundException('User has not been invited');

    if (invitedUser && invitedUser.accepted) {
      throw new ConflictException('User has been invited already');
    }

    const user = await this.userRepository.findOne({
      where: { email: invitedUser.email },
      include: Company,
    });

    if (!user) throw new NotFoundException('user does not exist');

    const invitedUserId =
      invitedUser && !invitedUser?.accepted ? invitedUser.id : null;

    const token = await this.authService.generateToken(
      {
        id: invitedUserId,
        inviteeId: user.id,
      },
      '7d',
    );

    const userInvite: UserInviteEvent = {
      email: agentInviteDto.email,
      token,
    };

    this.eventEmitter.emit('user.invite', userInvite);

    logger.info('>>>>');
    logger.info(token);
    logger.info('>>>>');
    // const role = await this.roleService.getAdminRole();

    // if (role.id !== authDetails.roleId)
    //   throw new ForbiddenException('You are allowed to perform this operation');

    // logger.info(
    //   `About to get user details with id: ${JSON.stringify(authDetails.id)}`,
    // );

    // return true;

    // const user = await this.userRepository.findOne({
    //   where: { email: agentInviteDto.email },
    //   include: Company,
    // });

    // if (!user) throw new NotFoundException('user does not exist');

    // logger.info(
    //   `Checking if user company matches using id : ${JSON.stringify(
    //     authDetails.companyId,
    //   )}`,
    // );

    // if (user.company.id !== authDetails.companyId)
    //   throw new UnauthorizedException('user company details do not match');

    // logger.info(
    //   `About to check whether invitee email is in sync with company's admin email`,
    // );

    // const userEmailSuffix = user.email.split('@')[1].toLowerCase();

    // const invitedUserEmailSuffix = agentInviteDto.email
    //   .split('@')[1]
    //   .toLowerCase();

    // if (userEmailSuffix !== invitedUserEmailSuffix)
    //   throw new UnauthorizedException('Invalid company email');

    //   const invitedUser = await this.userInviteRepository.findOne({
    //     where: { email: agentInviteDto.email },
    //   });

    // const userDetails = await this.userRepository.findOne({
    //   where: { email: agentInviteDto.email },
    // });

    // const company = await this.companyRepository.findOne({
    //   where: { id: user.company.id },
    // });

    // let newInvitedUser: any;
    // if (!invitedUser) {
    //   const invitedUserDetails: Partial<UserInvite> = {
    //     email: agentInviteDto.email,
    //     companyRole: 'agent',
    //     inviterId: company.userId,
    //     companyId: invitedUser?invitedUser.companyId:company.id,
    //   };

    //   newInvitedUser = await this.userInviteRepository.create(
    //     invitedUserDetails,
    //   );
    // }

    // logger.info(`----------`);

    // logger.info(user.email);
    // logger.info(company.id);

    // logger.info(`----------`);
    // return true;

    // if (invitedUser && invitedUser.accepted) {
    //   throw new ConflictException('User has been invited already');
    // }

    // const invitedUserId =
    //   invitedUser && !invitedUser?.accepted
    //     ? invitedUser.id
    //     : newInvitedUser.dataValues.id;

    // const token = await this.authService.generateToken(
    //   {
    //     id: invitedUserId,
    //     inviteeId: user.id,
    //   },
    //   '7d',
    // );

    // const userInvite: UserInviteEvent = {
    //   email: agentInviteDto.email,
    //   token,
    // };

    // this.eventEmitter.emit('user.invite', userInvite);
  }

  async createInvitedUser(
    createUserInviteDto: CreateInvitedUserDto,
    user: userAuthDetails,
  ) {
    logger.info(
      `About to get company Admin User details with id : ${user.inviteeId}`,
    );

    const companyAdminUser = await this.userRepository.findOne({
      where: { id: user.inviteeId },
      include: ['company'],
    });

    const invitedUser = await this.userInviteRepository.findOne({
      where: { id: user.id },
    });

    if (!invitedUser) throw new NotFoundException('User has not been invited');

    if (!companyAdminUser)
      throw new NotFoundException('No admin user exist for this company');

    const existingUser = await this.findOne({ email: invitedUser.email });

    if (existingUser) throw new ConflictException('User already exists');

    const userData: Partial<CreateUserDto> = {
      ...createUserInviteDto,
      email: invitedUser.email,
      companyRole: invitedUser.companyRole,
    };

    logger.info(`About to create user for invited user`);

    const createdUser = (await this.userRepository.create(
      userData,
    )) as UserExtension;

    createdUser.setCompany(companyAdminUser.company);

    createdUser.set('isVerified', true);
    await createdUser.save();

    invitedUser.set('accepted', true);
    await invitedUser.save();
  }

  async getUserAuthDetails(
    user: userAuthDetails,
  ): Promise<AccountLoginOutputDto> {
    logger.info('Retrieving user details for login');

    const details: any = await this.userRepository.findOne({
      where: { id: user.id },
      attributes: {
        exclude: [
          'created_at',
          'deleted_at',
          'updated_at',
          'password',
          'isVerified',
          'email',
          'companyId',
        ],
      },

      include: [
        {
          model: Company,
          include: [
            {
              model: AppModule,
              attributes: {
                exclude: ['created_at', 'deleted_at', 'updated_at'],
              },
              through: {
                attributes: [],
              },
            },
          ],
          attributes: {
            exclude: [
              'created_at',
              'deleted_at',
              'updated_at',
              'userId',
              'planId',
            ],
          },
        },
      ],
    });

    const subscription = await this.subscriptionService.getActiveSubDetails(
      user.companyId,
    );

    const role = await this.roleService.getRoleWithPermission(details.roleId);

    const features = await this.planService.getPlansFeatures();

    const featuresName = features.map((feature) => feature.name);

    details.roleId = undefined;

    let permissions = [];

    if (details.company) details.company.dataValues.subscription = subscription;

    if (role && !role.system) {
      permissions =
        role.permissions.length > 0
          ? role.permissions.map((permission) => permission.identifier)
          : [];

      if (subscription) {
        const planId = subscription?.planId;

        const planDetails = await this.planService.findOne(planId);

        if (permissions.length > 0) {
          const planFeatures = planDetails.features.map(
            (feature) => feature.name,
          );

          const planPermissions = permissions.filter((permission) =>
            planFeatures.includes(permission),
          );

          permissions = permissions.filter(
            (permission) => !featuresName.includes(permission),
          );

          permissions = [...permissions, ...planPermissions];
        }
      } else {
        details.company.dataValues.subscription = { status: false };

        permissions = permissions.filter(
          (permission) => !featuresName.includes(permission),
        );
      }
    }

    const temporaryPermissions = this.getTemporaryPermissions(
      details.temporaryPermissions,
    );

    permissions = [...new Set([...permissions, ...temporaryPermissions])];

    details.temporaryPermissions = undefined;

    if (details.company && details.company.dataValues.subscription)
      details.company.dataValues.subscription.planId = undefined;

    const companyDetails: Company = details.company;

    if (details.company?.id) {
      companyDetails.totalWarehouses = companyDetails.totalWarehouses ?? 0;

      companyDetails.totalSites = companyDetails.totalSites ?? 0;

      companyDetails.totalAssets = companyDetails.totalAssets ?? 0;

      companyDetails.totalSurveys = companyDetails.totalSurveys ?? 0;

      companyDetails.totalHardwares = companyDetails.totalHardwares ?? 0;
    }

    const userData = {
      permissions,
      isAdminUser: role?.name === 'Admin',
      ...details.dataValues,
    };

    return userData;
  }

  async getCompanyUsersByPage(companyId: string): Promise<User[]> {
    logger.info('About to get users for a particular company');

    return await this.userRepository.findAll({
      where: { companyId },
      attributes: [
        'id',
        'name',
        'email',
        'companyRole',
        'roleId',
        'temporary_permissions',
      ],
    });
  }

  async getCompanyUsers(
    companyId: string,
    page: number,
    perPage: number,
  ): Promise<[Partial<UserWithAdminField>[], PaginationMeta]> {
    const offset: number = getOffSet(+page, +perPage);

    logger.info('About to get users for a particular company');

    const { count: totalCount, rows: users } =
      await this.userRepository.findAndCountAll({
        distinct: true,
        where: { companyId },
        attributes: [
          'id',
          'name',
          'email',
          'companyRole',
          'roleId',
          'temporary_permissions',
        ],
        ...(perPage && { limit: perPage }),
        ...(offset && { limit: offset }),
      });

    const usersWithAdminField = await Promise.all(
      users.map(async (user) => {
        const role = await this.roleService.getOne(user.roleId);

        const response: Partial<UserWithAdminField> = {
          id: user.id,
          name: user.name,
          isAdmin: role?.name === 'Admin',
          email: user.email,
          companyRole: user.companyRole,
          roleId: user.roleId,
          temporaryPermissions: user.temporaryPermissions,
        };

        return response;
      }),
    );

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      page,
      perPage,
      usersWithAdminField.length,
    );

    return [usersWithAdminField, paginationMetaData];
  }

  async getCompanyRoles(companyId) {
    const companyRoles = await this.adminService.getCompanyRoles(companyId);

    return companyRoles;
  }

  async assignRoleToUser(userId: string, roleId: string) {
    logger.info(`About to get role with id: ${roleId} `);

    const role = await this.roleService.getOne(roleId);

    if (!role) throw new NotFoundException('Role does not exist');

    logger.info(`About to get userId with id: ${userId} `);

    const user = await this.userRepository.findOne({ where: { id: userId } });

    if (!user) throw new NotFoundException('User does not exist');

    logger.info(
      `About to set roleId with id : ${roleId} for  userId with id: ${userId} `,
    );

    await this.update({ id: userId }, { roleId });
  }

  async getCompanyPermissions(companyId: string) {
    return await this.adminService.getCompanyPermissions(companyId);
  }

  async getUserPermission(user: userAuthDetails, userId: string) {
    const userDetail = await this.findOne({
      id: userId,

      companyId: user.companyId,
    });

    if (!userDetail) throw new NotFoundException('User does not exist');

    const role = await this.roleService.getRoleWithPermission(
      userDetail.roleId,
    );

    const temporaryPermissions = this.getTemporaryPermissions(
      userDetail.temporaryPermissions,
    );

    const permissions = [
      ...new Set([
        ...role.permissions?.map((permission) => permission.identifier),
        ...temporaryPermissions,
      ]),
    ];

    return permissions;
  }

  async assignTemporaryPermissionsToUser(
    userId: string,
    permissionsDetails: AssignPermissionsDto,
  ) {
    logger.info(`About to get userId with id: ${userId} `);

    const user = await this.findOne({ id: userId });

    if (!user) throw new NotFoundException('User does not exist');

    let staledTemporaryPermissions = [];

    if (user.temporaryPermissions) {
      staledTemporaryPermissions = JSON.parse(user.temporaryPermissions);
    }

    logger.info(
      `About to get company permissions for company  with id: ${user.company.id} `,
    );

    const companyPermissions = await this.adminService.getCompanyPermissions(
      user.company.id,
    );

    permissionsDetails.permissions = permissionsDetails.permissions.filter(
      (permission) => companyPermissions.includes(permission),
    );

    const permissions = [
      ...staledTemporaryPermissions,
      ...permissionsDetails.permissions,
    ];

    logger.info(
      `About to set temporary permissions for company with id: ${user.company.id} `,
    );

    await this.update(
      { id: userId },
      { temporaryPermissions: JSON.stringify(permissions) },
    );
  }

  async removeTemporaryPermissionsToUser(
    userId: string,
    permissionsDetails: AssignPermissionsDto,
  ) {
    logger.info(`About to get userId with id: ${userId}`);

    const user = await this.findOne({ id: userId });

    if (!user) throw new NotFoundException('User does not exist');

    let staledTemporaryPermissions = [];

    if (user.temporaryPermissions) {
      staledTemporaryPermissions = JSON.parse(user.temporaryPermissions);
    }

    logger.info(
      `About to get company permissions for company  with id: ${user.company.id} `,
    );

    const companyPermissions = await this.adminService.getCompanyPermissions(
      user.company.id,
    );

    permissionsDetails.permissions = permissionsDetails.permissions.filter(
      (permission) => companyPermissions.includes(permission),
    );

    const permissions = staledTemporaryPermissions.filter(
      (permission) => !permissionsDetails.permissions.includes(permission),
    );

    logger.info(
      `About to remove temporary permissions for company with id: ${user.company.id} `,
    );

    await this.update(
      { id: userId },
      {
        temporaryPermissions:
          permissions.length === 0 ? null : JSON.stringify(permissions),
      },
    );
  }

  getTemporaryPermissions(temporaryPermissions: null | string): string[] {
    return temporaryPermissions ? JSON.parse(temporaryPermissions) : [];
  }
}
