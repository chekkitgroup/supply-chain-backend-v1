export const roles = [
  {
    name: 'Admin',
    system: true,
  },
  {
    name: 'Admin',
    system: false,
    permissions: [
      'dashboard.view',
      'assets.view',
      'assets.manage',
      'warehouses.view',
      'warehouses.manage',
      'surveys.view',
      'surveys.manage',
      'rewards.view',
      'rewards.manage',
      'reports.view',
      'insights.view',
      'rfids.view',
      'rfids.manage',
      'settings.view',
      'settings.manage',
      'users.view',
      'users.manage',
    ],
  },
  {
    name: 'Warehouse Manager',
    permissions: [
      'dashboard.view',
      'assets.view',
      'assets.manage',
      'warehouses.view',
      'warehouses.manage',
      'reports.view',
    ],
  },
  {
    name: 'Asset Manager',
    permissions: [
      'dashboard.view',
      'assets.view',
      'assets.manage',
      'insights.view',
      'reports.view',
    ],
  },
];

export const permissions = [
  {
    group: 'dashboard',
    identifiers: ['dashboard.view'],
  },
  {
    group: 'assets',
    identifiers: ['assets.view', 'assets.manage'],
  },
  {
    group: 'warehouses',
    identifiers: ['warehouses.view', 'warehouses.manage'],
  },
  {
    group: 'surveys',
    identifiers: ['surveys.view', 'surveys.manage'],
  },
  {
    group: 'rewards',
    identifiers: ['rewards.view', 'rewards.manage'],
  },
  {
    group: 'reports',
    identifiers: ['reports.view'],
  },
  {
    group: 'insights',
    identifiers: ['insights.view'],
  },
  {
    group: 'rfids',
    identifiers: ['rfids.view', 'rfids.manage'],
  },
  {
    group: 'settings',
    identifiers: ['settings.view', 'settings.manage'],
  },
  {
    group: 'users',
    identifiers: ['users.view', 'users.manage'],
  },
];
