import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CompanyOutputDto } from 'api/admin/dtos/response.dto';
import { asyncForEach } from 'utils/helpers';
import logger from 'utils/logger';
import { getOffSet, getPaginationMetaData } from 'utils/pagination';
import { objectResponse } from 'utils/response';
import { PaginationMeta } from 'utils/types/pagination';
import { CreateCompanyDto } from '../dtos/company.dto';

import Company from '../models/company.model';
import CompanyAppModule from '../models/companyAppModule.model';

@Injectable()
export class CompanyService {
  constructor(
    @InjectModel(Company) private readonly companyRepository: typeof Company,
    @InjectModel(CompanyAppModule)
    private readonly companyAppModuleRepo: typeof CompanyAppModule,
  ) {}

  async create(companyData: CreateCompanyDto): Promise<any> {
    logger.info(
      `About to create company with companyDetails data,
              ${JSON.stringify(companyData)}`,
    );

    const companyDetail: Partial<Company> = {
      name: companyData.name,

      address: companyData.address,

      country: companyData.country,

      userId: companyData.userId,

      identifier: companyData.identifier,
    };

    const company = await this.companyRepository.create(companyDetail);

    if (companyData.moduleIds.length >= 1) {
      await asyncForEach(companyData.moduleIds, async (moduleId) => {
        await this.companyAppModuleRepo.create({
          companyId: company.id,
          appModuleId: moduleId,
        });
      });
    }

    logger.info(
      `Result returned from company creation `,
      JSON.stringify(company),
    );

    return company;
  }

  async findOne(details: objectResponse): Promise<Company> {
    logger.info(
      `About to find company with details,
        ${JSON.stringify(details)}`,
    );

    const company = await this.companyRepository.findOne({
      where: {
        ...details,
      },
    });

    return company;
  }

  async getAll(): Promise<[CompanyOutputDto[]]> {
    const { count: totalCount, rows: companies } =
      await this.companyRepository.findAndCountAll();

    const companyOutput =
      companies.length > 0
        ? companies.map((company) =>
            CompanyOutputDto.FromCompanyOutput(company),
          )
        : [];

    return [companyOutput];
  }

  async update(updateValues: objectResponse, updateConditions: objectResponse) {
    await this.companyRepository.update(
      {
        ...updateValues,
      },
      {
        where: {
          ...updateConditions,
        },
      },
    );
  }
}
