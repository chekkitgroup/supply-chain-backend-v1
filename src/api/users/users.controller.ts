import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AdminAuth } from 'api/auth/authority.decorator';
import { AccountLoginOutputDtoSchema } from 'api/auth/dto/response.dto';
import { JwtAuthGuard } from 'api/auth/jwt-auth.guard';
import { JwtInviteAuthGuard } from 'api/auth/jwt-invite-auth-guard';
import { RequestWithUser } from 'api/auth/types';
import { PaginationFilter } from 'utils/pagination';
import { successResponse, successResponseSpec } from 'utils/response';
import { ResponseSchemaHelper } from 'utils/types/output-model';
import {
  AssignPermissionsDto,
  CreateInvitedUserDto,
  CreateAgentInviteDto,
  CreateUserInviteDto,
} from './dtos/user.dto';

import { UsersService } from './users.service';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private readonly _userService: UsersService) {}

  @ApiOperation({ summary: 'Invite user successfully' })
  @ApiResponse({
    status: 200,
    description: 'user invited successfully',
  })
  @ApiBearerAuth()
  @AdminAuth(false)
  @Post('invite')
  async userInvite(
    @Body() userInviteDto: CreateUserInviteDto,
    @Request() req: RequestWithUser,
  ): Promise<successResponseSpec> {
    await this._userService.InviteUser(userInviteDto, req.user);

    return successResponse('user invited successfully');
  }


  @ApiOperation({ summary: 'Send agent invitation code' })
  @ApiResponse({
    status: 200,
    description: 'agent invited successfully',
  })
  @ApiBearerAuth()
  @AdminAuth(false)
  @Post('agent-invite')
  async sendAgentCode(
    @Body() userInviteDto: CreateAgentInviteDto,
  ): Promise<successResponseSpec> {
    await this._userService.InviteAgent(userInviteDto);

    return successResponse('agent invited successfully');
  }

  @ApiOperation({ summary: 'Create invited user account via invite' })
  @ApiResponse({
    status: 200,
    description: 'Account created successfully',
  })
  @ApiBearerAuth()
  @UseGuards(JwtInviteAuthGuard)
  @Post('accept-invite')
  async acceptInvite(
    @Body() createUserInviteDto: CreateInvitedUserDto,
    @Request() req: RequestWithUser,
  ): Promise<successResponseSpec> {
    await this._userService.createInvitedUser(createUserInviteDto, req.user);

    return successResponse('User account created successfully', 201);
  }

  @ApiOperation({ summary: 'Get users' })
  @ApiResponse({
    status: 200,
    description: 'Get users',
  })
  @ApiBearerAuth()
  @AdminAuth(false)
  @Get('')
  async getUsersForACompany(
    @Request() req: RequestWithUser,
    @Query() filter?: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [users, paginationMetaData] = await this._userService.getCompanyUsers(
      req.user.companyId,
      +filter.page,
      +filter.perPage,
    );

    return successResponse(
      'Company users retrieved',
      200,
      users,
      true,
      paginationMetaData,
    );
  }

  @ApiOperation({ summary: 'Get authenticated User' })
  @ApiResponse({
    status: 200,
    description: 'Get authenticated User',
    schema: ResponseSchemaHelper(AccountLoginOutputDtoSchema),
  })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('authenticated')
  async getAuthenticatedUser(
    @Request() req: RequestWithUser,
  ): Promise<successResponseSpec> {
    const userDetails = await this._userService.getUserAuthDetails(req.user);

    return successResponse('user Details retrieved', 200, userDetails);
  }

  @ApiBearerAuth()
  @AdminAuth(false)
  @ApiOperation({ summary: 'Get Company Roles' })
  @ApiResponse({
    status: 200,
    description: 'Roles retrieved successfully',
  })
  @Get('roles')
  async getCompanyRoles(
    @Request() req: RequestWithUser,
  ): Promise<successResponseSpec> {
    const roles = await this._userService.getCompanyRoles(req.user.companyId);

    return successResponse('Roles retrieved successfully', 200, roles);
  }

  @ApiOperation({ summary: 'Assign role a user' })
  @ApiResponse({
    status: 200,
    description: 'Role assigned successfully',
  })
  @ApiBearerAuth()
  @AdminAuth(false)
  @Put(':id/roles/:roleId')
  async assignRoleToUser(
    @Param('id') userId: string,
    @Param('roleId') assignedRoleId: string,
  ): Promise<successResponseSpec> {
    await this._userService.assignRoleToUser(userId, assignedRoleId);

    return successResponse('Roles assigned succesfully');
  }

  @ApiBearerAuth()
  @AdminAuth(false)
  @ApiOperation({ summary: 'Get a company permissions based on their plan' })
  @ApiResponse({
    status: 200,
    description: 'Permissions retrieved successfully',
  })
  @Get('company/permissions')
  async getCompanyPermissions(
    @Request() req: RequestWithUser,
  ): Promise<successResponseSpec> {
    const permissions = await this._userService.getCompanyPermissions(
      req.user.companyId,
    );

    return successResponse('Permissions retrieved successfully', 200, {
      permissions,
    });
  }

  @ApiBearerAuth()
  @AdminAuth(false)
  @ApiOperation({ summary: 'Get User Permissions' })
  @ApiResponse({
    status: 200,

    description: 'Get User Permissions',
  })
  @Get(':userId/permissions')
  async getUserPermission(
    @Request() req: RequestWithUser,

    @Param('userId') userId: string,
  ): Promise<successResponseSpec> {
    const permissions = await this._userService.getUserPermission(
      req.user,
      userId,
    );

    return successResponse('Permissions retrieved successfully', 200, {
      permissions,
    });
  }

  @ApiOperation({ summary: 'Assign temporary permissions to a user' })
  @ApiResponse({
    status: 200,
    description: 'assign temporary permissions to users',
  })
  @ApiBearerAuth()
  @AdminAuth(false)
  @Put(':id/assign-permissions')
  async assignPermissionToUser(
    @Param('id') userId: string,
    @Body() permissions: AssignPermissionsDto,
  ): Promise<successResponseSpec> {
    await this._userService.assignTemporaryPermissionsToUser(
      userId,
      permissions,
    );

    return successResponse('Permissions assigned succesfully');
  }

  @ApiOperation({ summary: 'Remove temporary permissions for a user' })
  @ApiResponse({
    status: 200,
    description: 'Permissions removed successfully',
  })
  @ApiBearerAuth()
  @AdminAuth(false)
  @Put(':id/remove-permissions')
  async removePermissionsFromUsers(
    @Param('id') userId: string,
    @Body() permissions: AssignPermissionsDto,
  ): Promise<successResponseSpec> {
    await this._userService.removeTemporaryPermissionsToUser(
      userId,
      permissions,
    );

    return successResponse('Permissions removed succesfully');
  }
}
