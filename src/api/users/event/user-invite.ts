export class UserInviteEvent {
  email: string;
  token: string;
}
