export class AccountEvent {
  name: string;
  email: string;
  token: string;
}
