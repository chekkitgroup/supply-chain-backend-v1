import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Op } from 'sequelize';
import logger from 'utils/logger';
import { objectResponse } from 'utils/response';
import { CreateRoleDto } from '../dtos/role.dto';
import Permission from '../models/permission.model';
import Role from '../models/role.model';
import RolePermission from '../models/rolePermission.model';

@Injectable()
export class RoleService {
  constructor(
    @InjectModel(Role) private readonly roleRepository: typeof Role,

    @InjectModel(Permission)
    private readonly permissionRepository: typeof Permission,

    @InjectModel(RolePermission)
    private readonly rolePermissionRepository: typeof RolePermission,
  ) {}

  async getAdminRole(): Promise<Role> {
    const role = await this.roleRepository.findOne({
      where: { system: false, name: 'Admin' },
      include: ['permissions'],
    });

    return role;
  }

  async getSystemAdminRole(): Promise<Role> {
    const role = await this.roleRepository.findOne({
      where: { system: true, name: 'Admin' },
    });

    return role;
  }

  async getOne(id: string): Promise<Role> {
    logger.info(`about to get role with id ${JSON.stringify(id)}`);

    const role = await this.roleRepository.findOne({
      where: { id },
    });

    return role;
  }

  async getRoleAndPermissions(id: string): Promise<Role> {
    const role = await this.roleRepository.findOne({
      where: { id },
      include: ['permissions'],
    });

    return role;
  }

  async getOneByCompany(
    companyId: string,
    criterion: objectResponse,
  ): Promise<Role> {
    return await this.roleRepository.findOne({
      where: { companyId, ...criterion },
    });
  }

  async create(roleData: CreateRoleDto): Promise<Role> {
    const roleDetail: Partial<Role> = {
      name: roleData.name,
    };

    return await this.roleRepository.create(roleDetail);
  }

  async getRolesforCompany(companyId: string) {
    return await this.roleRepository.findAll({ where: { companyId } });
  }

  async update(
    updateConditions: objectResponse,
    updateValues: objectResponse,
  ): Promise<void> {
    await this.roleRepository.update(
      {
        ...updateValues,
      },
      {
        where: {
          ...updateConditions,
        },
      },
    );
  }

  async getDefaultRoles(): Promise<Role[]> {
    return await this.roleRepository.findAll({
      where: {
        name: {
          [Op.not]: 'Admin',
        },
        companyId: null,
      },
    });
  }

  async getPermissions(identifier: string): Promise<Permission> {
    return await this.permissionRepository.findOne({
      where: {
        identifier,
      },
      attributes: ['id'],
    });
  }

  async createRolePermission(details: objectResponse): Promise<RolePermission> {
    const permissionDetail: Partial<RolePermission> = {
      roleId: details.roleId,
      permissionId: details.permissionId,
    };

    return await this.rolePermissionRepository.create(permissionDetail);
  }

  async getRoleWithPermission(id: string) {
    return await this.roleRepository.findOne({
      where: { id },
      include: ['permissions'],
    });
  }
}
