import User from '../models/user.model';

export interface UserWithAdminField extends User {
  isAdmin: boolean;
}
