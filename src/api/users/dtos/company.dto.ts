import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Identifiers } from '../types';

export class CreateCompanyDto {
  @ApiProperty({ example: 'string' })
  @IsString()
  name: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  address?: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  country: string;

  userId: string;

  identifier: Identifiers;

  moduleIds: string[];
}
