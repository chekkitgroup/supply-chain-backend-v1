import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateRoleDto {
  @ApiProperty({ example: 'Operations' })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ example: ['insights.manage'] })
  @IsString({ each: true })
  permissions: string[];

  @ApiProperty({ example: '' })
  @IsString()
  companyId: string;
}
