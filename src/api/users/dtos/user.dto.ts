import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { Identifiers } from '../types';
import {
  EmailValidation,
  IsUserAlreadyExist,
} from '../validation/user.validation';

export class CreateUserDto {
  @ApiProperty({ example: 'Blessing Michael' })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ example: 'michael@gmail.com' })
  @IsEmail(
    {},
    {
      message: 'Invalid company email',
    },
  )
  @IsNotEmpty()
  @IsUserAlreadyExist()
  @EmailValidation('companyName', {
    message: 'Invalid company email',
  })
  email: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  password: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  companyName: string;

  @ApiProperty({ example: '19 Sapele Warri, Delta, Lagos State' })
  @IsString()
  @IsNotEmpty()
  address: string;

  @ApiProperty({ example: 'Nigeria' })
  @IsString()
  @IsNotEmpty()
  country: string;

  @ApiProperty({ example: 'Account Officer' })
  @IsString()
  companyRole?: string;

  @IsEnum(Identifiers)
  @IsOptional()
  companyIdentifier?: Identifiers;

  @ApiProperty({ example: '' })
  roleId?: string;

  @ApiProperty({
    example: [
      '0a026a17-180d-48af-bf9d-9533dd90f0d6',
      '324324s2-42d7-we24-48af-bf9d324234ds',
    ],
  })
  moduleIds: string[];
}

export class LoginDto {
  @ApiProperty({ example: 'blessingmakaraba@gmail.com' })
  @IsEmail(
    {},
    {
      message: 'Invalid Email',
    },
  )
  @IsNotEmpty()
  email: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  password: string;
}

export class resetPasswordDto {
  @ApiProperty({ example: 'blessingmakaraba@gmail.com' })
  @IsEmail(
    {},
    {
      message: 'Invalid Email',
    },
  )
  @IsNotEmpty()
  email: string;
}

export class updatePasswordDto {
  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  newPassword: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  confirmPassword: string;
}

export class CreateUserInviteDto {
  @ApiProperty({ example: 'michael@gmail.com' })
  @IsEmail(
    {},
    {
      message: 'Invalid Email',
    },
  )
  @IsNotEmpty()
  email: string;

  @ApiProperty({ example: 'Special Advisor' })
  @IsString()
  companyRole: string;
}

export class CreateAgentInviteDto {
  @ApiProperty({ example: 'michael@gmail.com' })
  @IsEmail(
    {},
    {
      message: 'Invalid Email',
    },
  )
  @IsNotEmpty()
  email: string;
}

export class CreateInvitedUserDto {
  @ApiProperty({ example: 'Blessing Michael' })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({ example: 'string' })
  @IsString()
  @IsNotEmpty()
  password: string;
}

export class AssignPermissionsDto {
  @ApiProperty({ example: ['users.manage'] })
  @IsString({ each: true })
  permissions: string[];
}
