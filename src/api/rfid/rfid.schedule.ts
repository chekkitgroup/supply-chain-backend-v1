import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import logger from 'utils/logger';
import { RedisService } from 'utils/redis';
import { RfidService } from './rfid.service';

@Injectable()
export class RfidScheduleService {
  constructor(
    private readonly rfidService: RfidService,
    private readonly redisService: RedisService,
  ) {}

  // every 15 minutes
  @Cron('0 */20 * * * *')
  async syncRfidDevice() {
    const lock = await this.redisService.acquireResource('sync_rfid_device');

    try {
      logger.info('starts syncing rfid from tag and trac');

      await this.rfidService.syncRfidFromTagAndTrac();

      logger.info('done syncing rfid from tag and trac');
    } catch (error) {
      logger.error(
        `An error occurred while syncing rfid device from tag and trac error: ${error.message} `,
      );
    } finally {
      try {
        if (lock) {
          await lock.unlock();

          logger.info('lock released for sync_rfid_device');
        }
      } catch (err) {}
    }
  }  // every 10 minutes
  // @Cron('0 */10 * * * *')
  @Cron('*/1 * * * *')
  async checkOnlineDevices() {
    const lock = await this.redisService.acquireResource('sync_online_rfid_device');
    // var d = new Date().toISOString();
    // var y = new Date(new Date().getFullYear(), 0, 1, 1).toISOString();
    // // logger.info('date:>> ', d);
    // // logger.info('date:>>> ', new Date().toISOString());
    // console.log(d)
    // console.log(y)

    try {
      logger.info('starts syncing online rfid from tag and trac');

      await this.rfidService.getDevicesOfflineFromTagAndTrac();

      logger.info('done syncing online rfid from tag and trac');
    } catch (error) {
      logger.error(
        `An error occurred while getting offline rfid device from tag and trac error: ${error.message} `,
      );
    } finally {
      try {
        if (lock) {
          await lock.unlock();

          logger.info('lock released for sync_rfid_device');
        }
      } catch (err) {}
    }
  }
}
