import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { AssetModule } from 'api/asset/asset.module';
import { UsersModule } from 'api/users/users.module';
import { UtilsModule } from 'utils/utils.module';
import Rfid from './models/rfid.model';
import { RfidController } from './rfid.controller';
import { RfidService } from './rfid.service';
import { RfidScheduleService } from './rfid.schedule';
import { WareHouseModule } from 'api/warehouse/warehouse.module';
import { RfidEventListener } from './listener/rfid-device';
import { OnesignalService } from 'utils/onesignal';
import Asset from 'api/asset/models/asset.model';
import AlarmLog from './models/alarm-log.model';
import DeviceLocationLog from './models/device-location-log.model';

@Module({
  imports: [
    SequelizeModule.forFeature([DeviceLocationLog,Asset, Rfid, AlarmLog, ]),
    forwardRef(() => UsersModule),
    forwardRef(() => AssetModule),
    forwardRef(() => UtilsModule),

    WareHouseModule,
  ],
  controllers: [RfidController],
  providers: [
    RfidService,
    RfidScheduleService,
    OnesignalService,
    RfidEventListener,
  ],
  exports: [RfidService],
})
export class RfidModule {}
