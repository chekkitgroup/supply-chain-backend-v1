export enum RFIDTYPE {
  ACTIVE = 'active',
  PASSIVE = 'passive',
}

export interface IProviderDevice {
  id: string;
  deviceType: string;
  manufacturerId: string;
  deviceManagerId: string;
  attributes: string;
  isActive: boolean;
  createdBy: string;
}

export interface IProviderDeviceDetails {

  vbat: number;
  time: string;
  rssi: number;
  gwType: string;
  blegw: string;
  tm: number;
  temp_alert: number;
  torState: number;
  dataType: string;
  type: number;
  src: string;
  torDevice: number;
  ts: string;
  lat: number;
  lng: number;
  name: string;
  data: string;
  addr: number;
  y: number;
  tempAlert: number;
  x: number;
  Phone: string;
  unc: number
}

export interface IDeviceResponse {
  status: string;
  devices: IProviderDevice[];
}

export interface IDeviceDetailsResponse {
  status: string;
  response: IProviderDeviceDetails;
}
