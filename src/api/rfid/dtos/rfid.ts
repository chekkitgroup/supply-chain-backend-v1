import { ApiProperty } from '@nestjs/swagger';

import { Type } from 'class-transformer';
import {
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsString,
  Length,
  ValidateNested,
} from 'class-validator';

import { RFIDTYPE } from '../types';

export class CreateRFIDDto {
  @ApiProperty({ type: () => [RFIDDetailDto] })
  @ValidateNested({ each: true })
  @IsArray()
  @IsNotEmpty()
  @Type(() => RFIDDetailDto)
  rfidDetails: [RFIDDetailDto];
}

export class RFIDDetailDto {
  @ApiProperty({ example: '679809009099' })
  @IsString()
  @Length(12)
  macAddress: string;

  @ApiProperty({ example: 'CHE-RFID-SERIAL-01' })
  @IsString()
  @IsNotEmpty()
  serialNumber: string;

  @ApiProperty({ example: RFIDTYPE.ACTIVE })
  @IsEnum(RFIDTYPE)
  type: RFIDTYPE;
}

export class AssignRFIDsDto {
  @ApiProperty()
  @IsString({ each: true })
  rfidIds: string[];
}
