export class RfidEvent {
  user: Record<string, unknown>;
  device: Record<string, unknown>;
  asset;
  site : string;
  warehouse : string;
}
