import {
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/sequelize';
import { ProvisionGWDTO } from 'api/admin/dtos';
import { TagAndTracService } from 'api/asset/apps/tagAndTrac.service';
import { HTTPREQUESTMETHOD } from 'api/asset/apps/types';
import { AssetService } from 'api/asset/asset.service';
import { IPackageLevelCatgory } from 'api/asset/types';
import { userAuthDetails } from 'api/auth/types';
import { WareHouseService } from 'api/warehouse/warehouse.service';
import { Op } from 'sequelize';
import logger from 'utils/logger';
import { RfidEvent } from '../rfid/event/rfid';

import {
  getOffSet,
  getPaginationMetaData,
  PaginationFilter,
} from 'utils/pagination';
import { objectResponse } from 'utils/response';
import { PaginationMeta } from 'utils/types/pagination';
import { UtilsService } from 'utils/utils.service';
import { AssignRFIDsDto, CreateRFIDDto } from './dtos/rfid';
import Rfid from './models/rfid.model';
import { IDeviceResponse, IDeviceDetailsResponse, RFIDTYPE } from './types';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { CreatedAt, Sequelize } from 'sequelize-typescript';
import sequelize from 'sequelize';
import Asset from 'api/asset/models/asset.model';
import { OnesignalService } from 'utils/onesignal';
import AlarmLog from './models/alarm-log.model';
import DeviceLocationLog from './models/device-location-log.model';

@Injectable()
export class RfidService {
  constructor(
    @InjectModel(Rfid) private readonly rfidRepository: typeof Rfid,
    @InjectModel(DeviceLocationLog) private readonly deviceLocationLogRepository: typeof DeviceLocationLog,
    @InjectModel(AlarmLog) private readonly alarmRepository: typeof AlarmLog,
    private sequelize: Sequelize,

    private readonly tagAndTracService: TagAndTracService,
    // @InjectConnection() private readonly connection: Connection,
    private readonly utilsService: UtilsService,
    private onesignalService: OnesignalService,
    @InjectModel(Asset) private readonly assetRepository: typeof Asset,
    @Inject(forwardRef(() => AssetService))
    private readonly assetService: AssetService,
    private eventEmitter: EventEmitter2,

    private readonly warehouseService: WareHouseService,
  ) {}

  async register(data: CreateRFIDDto) {
    const rfids = data.rfidDetails.map((rfidDetail) => {
      const rfid: Partial<Rfid> = {
        macAddress: rfidDetail.macAddress,
        serialNumber: rfidDetail.serialNumber,
        type: rfidDetail.type,
      };

      return rfid;
    });

    await this.rfidRepository.bulkCreate(rfids);
  }

  async assign(input: AssignRFIDsDto, companyId: string) {
    const rfids = await this.rfidRepository.findAll({
      where: {
        id: {
          [Op.in]: input.rfidIds,
        },
      },
    });

    const updateRFIDs = rfids.map((rfid) => {
      return this.rfidRepository.update(
        {
          companyId,
          isAssigned: true,
        },
        {
          where: {
            id: rfid.id,
          },
        },
      );
    });

    await Promise.all(updateRFIDs);
  }

  async findAsset(id: string): Promise<Asset> {
    return await this.assetRepository.findOne({
      where: { id },
      include: ['warehouse', 'site'],
    });
  }

  async getDeviceLocationLogs(macAddress: string): Promise<DeviceLocationLog[]> {
    return await this.deviceLocationLogRepository.findAll({
      where: { macAddress:macAddress },
      order: [
        ['created_at', 'DESC'],
      ],
    });
  }

  async find(details: objectResponse): Promise<Rfid> {
    return await this.rfidRepository.findOne({ where: { ...details } });
  }
  async find2(details: objectResponse): Promise<Rfid> {
    return await this.rfidRepository.findOne({
      where: { ...details },
      include: ['company', 'asset'],
    });
  }

  async updateOne(updateProp: Partial<Rfid>, id: string) {
    return this.rfidRepository.update(updateProp, { where: { id } });
  }
  async getuser(id: string) {
    console.log('geting user: ', id);
    // return this.connection.query(`SELECT * FROM users WHERE id= ${id}`);
    // return await this.connection.query(`SELECT * FROM users id= '${id}'`);
    try {
      const records = (await this.sequelize.query(
        `
      SELECT *
      FROM users 
      WHERE id = '${id}'
    `,
        {
          type: sequelize.QueryTypes.SELECT,
        },
      )) as any[];
      // console.log('records>', records)

      return records[0];
      // return await this.connection.query(`SELECT * FROM 'supply_chain.users where id= 2f4442f5-f25b-4657-818d-205527a25d0d' LIMIT 0, 1000`);
    } catch (error) {
      console.log('query error: ', error);
    }
  }

  async findOneWithCompany(id: string): Promise<Rfid> {
    return this.rfidRepository.findOne({ where: { id }, include: ['company'] });
  }

  async getAllAlarmLogs(
    user: userAuthDetails,
    filter: PaginationFilter,
  ): Promise<[AlarmLog[], PaginationMeta]> {
    const { page, perPage } = filter;
    const offset: number =
      page && perPage ? getOffSet(+page, +perPage) : undefined;

    const { rows: logs, count: totalCount } =
      await this.alarmRepository.findAndCountAll({
        distinct: true,
        where: {
          companyId: user.companyId,
        },
        include: ['warehouse', 'company', 'asset'],
        // include: ['warehouse, company, asset'],
        // ...(perPage && { limit: perPage }),
        // ...(offset && { limit: offset }),
      });

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      +filter.page,
      +filter.perPage,
      logs.length,
    );

    return [logs, paginationMetaData];
  }

  async getAllRFIDTags(
    user: userAuthDetails,
    filter: PaginationFilter,
  ): Promise<[Rfid[], PaginationMeta]> {
    const { page, perPage } = filter;
    const offset: number =
      page && perPage ? getOffSet(+page, +perPage) : undefined;

    const { rows: rfids, count: totalCount } =
      await this.rfidRepository.findAndCountAll({
        distinct: true,
        where: {
          companyId: user.companyId,
        },
        // ...(perPage && { limit: perPage }),
        // ...(offset && { limit: offset }),
      });

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      +filter.page,
      +filter.perPage,
      rfids.length,
    );

    return [rfids, paginationMetaData];
  }
  async returnAllRFIDTags(): Promise<Rfid[]> {
    return await this.rfidRepository.findAll({  where: { isAssigned:true }, include: ['company', 'asset'] });

  }

  async getRfids(filter: PaginationFilter): Promise<[Rfid[], PaginationMeta]> {
    const { page, perPage } = filter;
    const offset: number =
      page && perPage ? getOffSet(+page, +perPage) : undefined;

    const { rows: rfids, count: totalCount } =
      await this.rfidRepository.findAndCountAll({
       
        distinct: true,

        // ...(perPage && { limit: perPage }),
        // ...(offset && { limit: offset }),
      });

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      +filter.page,
      +filter.perPage,
      rfids.length,
    );

    return [rfids, paginationMetaData];
  }

  async getGateways(
    filter: PaginationFilter,
  ): Promise<[Rfid[], PaginationMeta]> {
    const { page, perPage } = filter;
    const offset: number =
      page && perPage ? getOffSet(+page, +perPage) : undefined;

    const { rows: rfids, count: totalCount } =
      await this.rfidRepository.findAndCountAll({
        where: {
          deviceType: { [Op.endsWith]: '_GW' },
        },
        distinct: true,
        ...(perPage && { limit: perPage }),
        ...(offset && { limit: offset }),
      });

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      +filter.page,
      +filter.perPage,
      rfids.length,
    );

    return [rfids, paginationMetaData];
  }

  async disableRFIDTag(
    user: userAuthDetails,
    oldRfidId: string,
    newRfidId: string,
  ) {
    const rfid = await this.rfidRepository.findOne({
      where: {
        id: oldRfidId,
        companyId: user.companyId,
      },
    });

    if (!rfid) throw new NotFoundException('Rfid does not exist');

    const asset = await this.assetService.findOne({ rfidId: rfid.id });

    if (!asset)
      throw new NotFoundException('This rfid is not attach to any asset');

    const project = await this.warehouseService.getProjectByCompanyId(
      user.companyId,
    );

    logger.info(
      'About to call tag and trac to deprovision asset for the specific asset',
    );

    await this.tagAndTracService.call(
      {},
      HTTPREQUESTMETHOD.DELETE,
      `device/${asset.macAddress}/asset`,
    );

    logger.info('About to call disable rfid tag from tag and trac');

    await this.rfidRepository.update(
      { disabled: true, status: false },
      { where: { id: oldRfidId, companyId: user.companyId } },
    );

    if (newRfidId) {
      const rfid = await this.find({
        id: newRfidId,
        disabled: false,
        status: false,
        isAssigned: true,
        companyId: user.companyId,
      });

      if (!rfid)
        throw new NotFoundException('Rfid to provision does not exist');

      const provisionAssetData = {
        trackedId: asset.providerTrackingId,
        tu_type: IPackageLevelCatgory[asset.packageLevel],
        projectId: project.provider_project_id,
      };

      logger.info(
        'About calling tag and trac service to provision asset with a specific device',
      );

      await this.tagAndTracService.call(
        provisionAssetData,
        HTTPREQUESTMETHOD.POST,
        `device/${rfid.macAddress}/asset`,
      );
    }
  }

  async count() {
    return this.rfidRepository.count();
  }

  async create(rfidData: Partial<Rfid>) {
    return this.rfidRepository.create(rfidData);
  }

  async addLog(alarmData: Partial<AlarmLog>) {
    // console.log('adding alarm log', alarmData)
    return this.alarmRepository.create(alarmData);
  }

  async addDeviceLocationLog(deviceData: Partial<DeviceLocationLog>) {
   const logs = await this.getDeviceLocationLogs(deviceData.macAddress)
   let lastLog = logs[0];

   if(!lastLog){
    // console.log("adding to log - new no record")
      return this.deviceLocationLogRepository.create(deviceData);
   }
   let distance =  Math.round(this.returnDistance(lastLog.latitude, lastLog.longitude,deviceData.latitude, deviceData.longitude) * 1000);

  //  console.log('-------------------')
  //  console.log('adding Device Location last', lastLog)
  //  console.log('adding Device Location log', distance)

   if(logs && logs.length > 0 && distance > 10){
    // console.log("adding to log - existing ")
    return this.deviceLocationLogRepository.create(deviceData);
   }else if(logs.length == 0 && distance > 10){
    // console.log("adding to log - new ")
    return this.deviceLocationLogRepository.create(deviceData);
   }else{
    // console.log("not adding to log  ", deviceData.macAddress)
   }
     

  }

  async syncRfidFromTagAndTrac() {
    let chekkitDetail = await this.utilsService.getChekkitDetailFromCache();

    if (!chekkitDetail) {
      await this.tagAndTracService.login();

      chekkitDetail = await this.utilsService.getChekkitDetailFromCache();
    }
    logger.info('About to sync rfid tags with Tag and Trac');

    const deviceData = (await this.tagAndTracService.call(
      {},
      HTTPREQUESTMETHOD.GET,
      `organization/${chekkitDetail.organization.id}/devices`,
    )) as unknown as IDeviceResponse;

    let count = await this.count();

    const rfidsSync = deviceData?.devices?.map(async (device) => {
      const rfid = await this.find({
        macAddress: device.id,
      });

      if (!rfid) {
        const rfidData: Partial<Rfid> = {
          macAddress: device.id,
          serialNumber: `CHE-${device?.deviceType || 'RFID'}-SERIAL-${
            count + 1
          }`,
          type: RFIDTYPE.ACTIVE,
          deviceType: device?.deviceType,
          status: device.isActive,
        };

        count++;

        return this.create(rfidData);
      }
    });

    await Promise.all(rfidsSync);

    logger.info('Done Syncing RFID');
  }

  async createDeviceLog(rfid: Rfid){
    try {
      let chekkitDetail = await this.utilsService.getChekkitDetailFromCache();
      if (!chekkitDetail) {
        await this.tagAndTracService.login();
  
        chekkitDetail = await this.utilsService.getChekkitDetailFromCache();
      }
  
  
      const deviceData = (await this.tagAndTracService.call(
        {},
        HTTPREQUESTMETHOD.GET,
        `device/${rfid.macAddress}/recentData`,
      )) as unknown as IDeviceDetailsResponse;
      let deviceDetails = deviceData.response;
  
      let logData: Partial<DeviceLocationLog> = {
        macAddress: rfid.macAddress,
        latitude: deviceDetails.lat,
        longitude: deviceDetails.lng,
      }
      this.addDeviceLocationLog(logData);
      
    } catch (error) {
      console.log('createDeviceLog> ',error)
      
    }
  }
  
  async getDevicesOfflineFromTagAndTrac() {
    logger.info('Loading all RFID');
    try {
      const rfids = await this.returnAllRFIDTags();
      // console.log('rfids>>', rfids)
  
    
      const rfidsSync = rfids.map(async (rfid) => {
        // console.log('rfidsSync>> ',rfid)

        // return false;
  
        // const asset = await this.findAsset(rfid?.asset?.id);
        // console.log('rfid>> ', rfid);

        // console.log('asset>> ', asset);
        // check if rfid is assigned
 
        // console.log('<<------------->>>> ');
        // console.log('<rfid>>> ', rfid);
        // console.log('<rfid>isAssigned>> ', rfid.isAssigned);
        // console.log('<rfid>company>> ', rfid.company);
        // console.log('<rfid>asset>> ', rfid.asset);
        if (rfid && rfid.isAssigned && rfid.company && rfid.asset) {
          // let env = this;
          // console.log('<rfid>>> ', rfid);
          
        const asset = await this.findAsset(rfid?.asset?.id);
        this.createDeviceLog(rfid);
        if(asset && !asset.site){
          // console.log('<macAddress>',asset)
          // console.log('<macAddress>',asset.warehouse)

          return false;
        }else{
          // console.log('macAddress site>',asset.site)
        }
        // console.log('<asset>>> ', asset);
        let siteCoords = {
          latitude: asset.site.latitude,
          longitude: asset.site.longitude
        }
        // let warehouseCoords ={ latitude: 6.6267446, longitude: 3.3418415 }
        let chekkitDetail = await this.utilsService.getChekkitDetailFromCache();
        if (!chekkitDetail) {
          await this.tagAndTracService.login();
    
          chekkitDetail = await this.utilsService.getChekkitDetailFromCache();
        }

      const deviceData = (await this.tagAndTracService.call(
        {},
        HTTPREQUESTMETHOD.GET,
        `device/${rfid.macAddress}/recentData`,
      )) as unknown as IDeviceDetailsResponse;
      let deviceDetails = deviceData.response;
      // console.log('deviceDetails>> ', deviceDetails);


      let deviceCoords = {
        latitude: deviceDetails.lat,
        longitude: deviceDetails.lng
      }

      let distance =  Math.round(this.returnDistance(siteCoords.latitude, siteCoords.longitude,deviceCoords.latitude, deviceCoords.longitude) * 1000);


      // console.log('<asset warehouse siteCoords>>> ', siteCoords);
      // console.log('<asset warehouse deviceCoords>>> ', deviceCoords);
      // console.log(`<asset warehouse distance<${rfid.macAddress}>>> `, distance);
      // return false;
        let env =  this;
          // if assigned 6yget company user email
          if(distance> 20){
            this.getuser(rfid.company.userId).then(function (result) {
              // console.log('rfid>> ', rfid);
  
              // console.log('(asset)>>', asset);
              // console.log('<<(asset)>>');
              // env.sendEmailAlert(
              //   deviceData,
              //   result,
              //   rfid.asset,
              //   asset.site?.name,
              //   asset.site?.name,
              // );
              // console.log(`distance exceeded. mac: ${rfid.macAddress} dist: ${distance}`)
              // env.onesignalService.sendNotification(
              //   deviceData,
              //   asset,
              //   rfid.company.id,
              //   asset.site?.name,
              //   'Engie',
              // );

              const alarmData: Partial<AlarmLog> = {
                macAddress: rfid.macAddress,
                companyId: rfid.company.id,
                warehouseId: asset.warehouse?.id,
                assetId: asset.id,
              };
          
              // env.addLog(alarmData);
            });

          }else{
            // console.log(`distance in line. mac: ${rfid.macAddress} dist: ${distance}`)

          }
        }
      });
      // console.log('getDevicesOfflineFromTagAndTrac length>> ',rfids.length)

    } catch (error) {
      console.log('getDevicesOfflineFromTagAndTrac>> ',error)
    }

  }


  returnDistanced(lat1, lon1, lat2, lon2) {
    let distance = 0;

    var p1 = { lat: lat1, lng: lon1 };

    var p2 = { lat: lat2, lng: lon2 };

    var R = 6378137; // Earth’s mean radius in meter
    var dLat = this.rad(p2.lat - p1.lat);
    var dLong = this.rad(p2.lng - p1.lng);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.rad(p1.lat)) *
        Math.cos(this.rad(p2.lat)) *
        Math.sin(dLong / 2) *
        Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    distance = R * c;
    return distance;

    // return Math.round(distance /); // returns the distance in meter
  }

  returnDistance(lat1, lon1, lat2, lon2) 
    {
      var R = 6371; // km
      var dLat = this.rad(lat2-lat1);
      var dLon = this.rad(lon2-lon1);
      lat1 = this.rad(lat1);
      lat2 = this.rad(lat2);

      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c;
      return d;
    }

  rad(x) {
    return (x * Math.PI) / 180;
  }

  async sendEmailAlert(device, user, asset, site, warehouse) {



    const rfidEvent: RfidEvent = {
      user,
      device,
      asset,
      site,
      warehouse,
    };

    this.eventEmitter.emit('rfid.unavailable', rfidEvent);


    logger.info('About to get device details from Tag and Trac');

    // try {
    //   const deviceData = (await this.tagAndTracService.call(
    //     {},
    //     HTTPREQUESTMETHOD.GET,
    //     `device/${device}/recentData`,
    //   )) as unknown as IDeviceDetailsResponse;
    //   let deviceDetails = deviceData.response;
    //   // console.log('deviceDetails>> ', deviceDetails);

    //   const rfidEvent: RfidEvent = {
    //     user,
    //     device,
    //     deviceDetails,
    //     asset,
    //     site,
    //     warehouse,
    //   };

    //   this.eventEmitter.emit('rfid.unavailable', rfidEvent);
    // } catch (error) {
    //   console.log(error);
    // }
  }

  async deprovisionGateway(macAddress: string) {
    const resp = await this.tagAndTracService.call(
      {},
      HTTPREQUESTMETHOD.DELETE,
      `device/${macAddress}/gateway`,
    );
    if (resp) {
      return this.rfidRepository.update(
        {
          companyId: null,
          isAssigned: null,
        },
        {
          where: {
            macAddress,
          },
        },
      );
    }
  }
  async deprovisionAsset(macAddress: string) {
    try {
      const resp = await this.tagAndTracService.call(
        {},
        HTTPREQUESTMETHOD.DELETE,
        `device/${macAddress}/asset`,
      );
      console.log('im in deprovisioning>> ', resp);

       this.rfidRepository.update(
        {
          companyId: null,
          isAssigned: null,
          status: false
        },
        {
          where: {
            macAddress,
          },
        },
      );

      return this.assetRepository.update(
        {
          macAddress: null,
        },
        {
          where: {
            macAddress,
          },
        },
      );
    } catch (error) {
      console.log(error);
    }

    // if (resp) {
    //   try {
    //     return this.rfidRepository.update(
    //       {
    //         companyId: null,
    //         isAssigned: null,
    //       },
    //       {
    //         where: {
    //           macAddress,
    //         },
    //       },
    //     );
    //   } catch (error) {
    //     console.log('error deprovisioning>> ', error);
    //   }

    //   return this.assetRepository.update(
    //     {
    //       macAddress: null,
    //     },
    //     {
    //       where: {
    //         macAddress,
    //       },
    //     },
    //   );
    // }
  }

  async provisionGateway(data: ProvisionGWDTO) {
    const gatewayData = {
      location: data.location.replace(' ', '-'),
      gatewayType: data.deviceType,
      longitude: '',
      latitude: '',
      projectId: '',
      lastReading: { '': '' },
      attributes: { '': '' },
    };

    const resp = await this.tagAndTracService.call(
      gatewayData,
      HTTPREQUESTMETHOD.POST,
      `device/${data.macAddress}/gateway`,
    );

    if (resp) {
      return this.rfidRepository.update(
        {
          companyId: data.companyId,
          isAssigned: true,
        },
        {
          where: {
            macAddress: data.macAddress,
          },
        },
      );
    }
  }
}

