import { Controller, Request, Query,Delete, Get, Param, Put } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Authorize } from 'api/auth/authority.decorator';

import { RequestWithUser } from 'api/auth/types';
import { PaginationFilter } from 'utils/pagination';
import { successResponse, successResponseSpec } from 'utils/response';
import { RfidService } from './rfid.service';

@ApiBearerAuth()
@ApiTags('RFID')
@Controller('rfid')
export class RfidController {
  constructor(private readonly _rfidService: RfidService) {}

  @ApiOperation({ summary: 'Retrieves RFID Tags for a company' })
  @ApiResponse({
    status: 200,
    description: 'Retrieves RFID Tags for a company successfully',
  })
  @Authorize('rfids.view')
  @Get()
  async getAllRFIDTags(
    @Request() req: RequestWithUser,
    @Query() filter?: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [response, paginationMetaData] =
      await this._rfidService.getAllRFIDTags(req.user, filter);

    return successResponse(
      'RFID Tags retrieved successfully',
      200,
      response,
      true,
      paginationMetaData,
    );
  }

  
  @ApiOperation({
    summary: 'Get device location logs',
  })
  @Authorize('assets.view')
  @Get('detail/:macAddress/')
  async getAsset(
    @Request() req: RequestWithUser,
    @Param('macAddress') macAddress: string,
  ) {
    const asset = await this._rfidService.getDeviceLocationLogs(
      macAddress
    );

    return successResponse(
      'Location retrieved successfully',
      200,
      asset,
      true
    );
  }

  @ApiOperation({ summary: 'Retrieves Alarm Logs for a company' })
  @ApiResponse({
    status: 200,
    description: 'Retrieves  Alarm Logs for a company successfully',
  })
  @Authorize('rfids.view')
  @Get('alarm-logs')
  async getAllAlarmLogs(
    @Request() req: RequestWithUser,
    @Query() filter?: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [response, paginationMetaData] =
      await this._rfidService.getAllAlarmLogs(req.user, filter);

    return successResponse(
      'Alarm Logs retrieved successfully',
      200,
      response,
      true,
      paginationMetaData,
    );
  }


  @ApiOperation({ summary: 'Disable RFID Tags for a company' })
  @ApiResponse({
    status: 200,
    description: 'Disable RFID Tags for a company successfully',
  })
  @Authorize('rfids.manage')
  @Put(':oldRfidId/disable')
  async disableRfidTag(
    @Request() req: RequestWithUser,
    @Param('oldRfidId') oldRfidId: string,
    @Query() newRfidId?: string,
  ): Promise<successResponseSpec> {
    await this._rfidService.disableRFIDTag(req.user, oldRfidId, newRfidId);

    return successResponse('RFID Tag disable successfully', 200);
  }

  @ApiBearerAuth()
  @ApiOperation({ summary: 'deprovision an asset and unassign from company' })
  @Delete('assets/de-provision/:macAddress')
  async deprovisionAsset(@Param('macAddress') macAddress: string){
    await this._rfidService.deprovisionAsset(macAddress)
    return successResponse('Asset deprovisioned successfully', 200);
  }

}
