import { OnEvent } from '@nestjs/event-emitter';
import { Injectable } from '@nestjs/common';
import { UserInviteEmailTemplate, DeviceUndetectedEmailTemplate } from 'utils/constant';
import sendEmail from 'utils/email';
import logger from 'utils/logger';
import { RfidEvent } from '../event/rfid';
import { OnesignalService } from 'utils/onesignal';

@Injectable()
export class RfidEventListener {

  @OnEvent('rfid.unavailable')
  handleUnavailableRfidEvent(event: RfidEvent) {
    this.sendRfidNotificationEmail(
      event.user,
      event.device,
      event.asset,
      event.site, 
      event.warehouse
    );
  }

  async sendRfidNotificationEmail(
    user,
    device,
    asset,
    site, 
    warehouse
  ): Promise<boolean> {
    console.log('deviceDetailslll>>', device);
    const emailContent = DeviceUndetectedEmailTemplate(  user,
      device,
      asset,
      site, 
      warehouse
      );
      
    const emailCovntent =
      `
      Hello ${user.name},` +
      '\r' +
      '\r' +

      `We suspected a security threat with the asset with the below information.  %0D%0D
      `
      +
      `Asset Name:  ${asset.name} %0D%0D`
      +  
      `Asset Tag:  ${device.macAddress} %0D%0D`
      +
      `Last Event:  ${device.time} %0D%0D`
      +
      `We are unable to track or read the activity on the asset, please take action. ` +
      '%0D' +
      `Treat as URGENT ⚠️.  ` +
      '%0D' +
      `Chekkit Anti-theft.` +
      '%0D' +
      '%0D' +
      '%0D' +
      `Cheers.` +
      `
    At the following coordinates Lat: {${device.lat}}, Lng: {${device.lng}}` +
      '%0D' +
      "<a href='https://www.google.com/maps/search/?api=1&query=" +
      device.lat +
      ' ,' +
      device.lng +
      "'>Click to view on Map</a>";
    // console.log('>>>> ', emailContent);
    logger.info(
      `About to send invite link sent for user with details ${JSON.stringify(
        user,
      )}`,
    );

    const emailSent = await sendEmail(
      user.email,
      '[Urgent] missing device: ' + asset.name,
      emailContent,
    );

    logger.info(`Subject: Critical security alert 🚨`);
    return emailSent;
  }
}
