import Asset from 'api/asset/models/asset.model';
import { UUIDV4 } from 'sequelize';
import {
  Column,
  Model,
  DataType,
  HasOne,
  PrimaryKey,
  IsUUID,
  ForeignKey,
  Table,
  BelongsTo
} from 'sequelize-typescript';
import Rfid from 'api/rfid/models/rfid.model';

@Table
class DeviceLocationLog extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING })
  macAddress: string;
  

  @Column({
    type: DataType.FLOAT,
  })
  latitude: number;

  @Column({
    type: DataType.FLOAT,
  })
  longitude: number;


  // @BelongsTo(() => Rfid, 'deviceId')
  // device: Rfid;

  // virtuals
//   deviceId: string;
}

export default DeviceLocationLog;
