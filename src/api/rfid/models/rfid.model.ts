import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
  HasOne,
  BelongsTo,
} from 'sequelize-typescript';

import { UUIDV4 } from 'sequelize';
import Asset from 'api/asset/models/asset.model';
import { RFIDTYPE } from '../types';
import Company from 'api/users/models/company.model';

@Table
class Rfid extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING, unique: true })
  serialNumber: string;

  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  macAddress: string;

  @Column({ type: DataType.STRING, allowNull: true })
  deviceType: string;

  @Column({
    type: DataType.ENUM('active', 'passive'),
    defaultValue: RFIDTYPE.ACTIVE,
  })
  type: RFIDTYPE;

  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  isAssigned: boolean;

  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  status: boolean;

  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  disabled: boolean;

  @HasOne(() => Asset, 'rfidId')
  asset: Asset;

  @BelongsTo(() => Company, 'companyId')
  company: Company;
}

export default Rfid;
