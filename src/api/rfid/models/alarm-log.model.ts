import Asset from 'api/asset/models/asset.model';
import { UUIDV4 } from 'sequelize';
import Company from 'api/users/models/company.model';
import {
  Column,
  Model,
  DataType,
  HasOne,
  PrimaryKey,
  IsUUID,
  ForeignKey,
  Table,
  BelongsTo
} from 'sequelize-typescript';
import Warehouse from 'api/warehouse/models/warehouse.model';

@Table
class AlarmLog extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  // @Column({
  //   type: DataType.STRING(16),
  //   allowNull: false,
  //   unique: true,
  // })
  // value: string;

  // @Column({
  //   type: DataType.BOOLEAN,
  //   defaultValue: true,
  // })
  // isAssigned: boolean;


  @Column({ type: DataType.STRING })
  macAddress: string;
  
  // @ForeignKey(() => Asset)
  // @Column
  // assetId: string;

  @BelongsTo(() => Company, 'companyId')
  company: Company;

  @BelongsTo(() => Warehouse, 'warehouseId')
  warehouse: Warehouse;

  @BelongsTo(() => Asset, 'assetId')
  asset: Asset;

  // virtuals
  companyId: string;
  warehouseId: string;
  assetId: string;
}

export default AlarmLog;
