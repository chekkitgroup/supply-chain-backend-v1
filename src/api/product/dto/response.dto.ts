import { getSchemaPath } from '@nestjs/swagger';
import Product from '../models/product.model';
import { productDisplay } from '../types';

export class ProductOutputDto {
  id: string;
  name: string;
  description: string;
  code: productDisplay;
  created_at: Date;
  deleted_at: Date;
  updated_at: Date;

  static FromProductOutput(product: Product) {
    const output = new ProductOutputDto();

    const exposedKeys = ['id', 'name', 'description', 'code'];

    exposedKeys.forEach((exposedKey) => {
      output[exposedKey] = product[exposedKey];
    });

    return output;
  }
}

export const ProductOutputDtoSchema = getSchemaPath(ProductOutputDto);
