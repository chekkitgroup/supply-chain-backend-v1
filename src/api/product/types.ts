export enum productDisplay {
  Asset_Management = 'AS',
  Inventory_Management = 'IM',
  Connect = 'CO',
  Retail_POS = 'RP',
}
