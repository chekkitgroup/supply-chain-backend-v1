import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { successResponse, successResponseSpec } from 'utils/response';
import { ResponseSchemaHelper } from 'utils/types/output-model';
import { ProductOutputDto, ProductOutputDtoSchema } from './dto/response.dto';
import { ProductService } from './product.service';

@ApiTags('Products')
@Controller('products')
export class ProductController {
  constructor(private readonly _productService: ProductService) {}

  @ApiOperation({ summary: 'Retrieves Supply Chain Products' })
  @ApiResponse({
    status: 200,
    description: 'Product retrieved successfully',
    schema: ResponseSchemaHelper(ProductOutputDtoSchema, 'array'),
  })
  @Get('')
  async getProducts(): Promise<successResponseSpec> {
    const products = await this._productService.getAll();

    const productsOutput = products?.map((product) =>
      ProductOutputDto.FromProductOutput(product),
    );

    return successResponse(
      'Products retrieved successfully',
      200,
      productsOutput,
    );
  }
}
