import { UUIDV4 } from 'sequelize';
import {
  Column,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  DataType,
} from 'sequelize-typescript';
import { productDisplay } from '../types';

@Table
class Product extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING(50), allowNull: false })
  name: string;

  @Column({ type: DataType.TEXT })
  description: string;

  @Column({ type: DataType.ENUM('AS', 'IM', 'CO', 'RP') })
  code: productDisplay;
}

export default Product;
