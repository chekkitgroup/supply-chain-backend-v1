import { productDisplay } from './types';

export const products = [
  {
    name: 'Asset Management',
    code: productDisplay.Asset_Management,
    description: 'For Asset Management',
  },

  {
    name: 'Inventory Management',
    code: productDisplay.Inventory_Management,
    description: 'For Inventory Management',
  },
  {
    name: 'Connect+',
    code: productDisplay.Connect,
    description: 'Meet manufacturers, retailers, distributors like you',
  },
  {
    name: 'Retail and POS',
    code: productDisplay.Retail_POS,
    description: 'Retail and POS',
  },
];
