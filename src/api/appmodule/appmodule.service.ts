import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import logger from 'utils/logger';
import AppModule from './models/appmodule.model';
import { CreateModule } from './dto/createModule';

@Injectable()
export class AppmoduleService {
  constructor(
    @InjectModel(AppModule)
    private readonly moduleRepository: typeof AppModule,
  ) {}

  async getAllAppModule(): Promise<AppModule[]> {
    logger.info('About to get all modules');

    const modules = await this.moduleRepository.findAll();

    return modules;
  }

  async createModule(data: CreateModule): Promise<void> {
    logger.info('Creating new module');
    await this.moduleRepository.create({
      name: data.name,
    });
  }
}
