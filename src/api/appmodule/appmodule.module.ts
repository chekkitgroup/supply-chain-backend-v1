import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { AppmoduleController } from './appmodule.controller';
import { AppmoduleService } from './appmodule.service';
import AppModule from 'api/appmodule/models/appmodule.model';

@Module({
  imports: [SequelizeModule.forFeature([AppModule])],
  controllers: [AppmoduleController],
  providers: [AppmoduleService],
  exports: [AppmoduleService],
})
export class AppmoduleModule {}
