import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { successResponse, successResponseSpec } from 'utils/response';
import { AppmoduleService } from './appmodule.service';
import { CreateModule } from './dto/createModule';

@Controller('appmodule')
@ApiTags('AppModule')
export class AppmoduleController {
  constructor(private readonly moduleService: AppmoduleService) {}

  @ApiOperation({ summary: 'Get all modules' })
  @ApiResponse({
    status: 200,
    description: 'Get all modules successfully',
  })
  @Get()
  async getAllModule(): Promise<successResponseSpec> {
    const modules = await this.moduleService.getAllAppModule();

    return successResponse(
      'All module returned successfully',
      200,
      modules,
      false,
    );
  }
}
