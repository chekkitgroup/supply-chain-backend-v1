import Company from 'api/users/models/company.model';
import CompanyAppModule from 'api/users/models/companyAppModule.model';
import { NOW } from 'sequelize';
import { UUIDV4 } from 'sequelize';
import {
  BelongsToMany,
  Column,
  DataType,
  IsUUID,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';

@Table
class AppModule extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.STRING, allowNull: false })
  name: string;

  @BelongsToMany(() => Company, () => CompanyAppModule)
  appModules: Company[];
}

export default AppModule;
