export const appModules = [
  {
    name: 'asset-management',
  },
  {
    name: 'connect-plus',
  },
  {
    name: 'inventory-management',
  },
  {
    name: 'retail-pos',
  },
];
