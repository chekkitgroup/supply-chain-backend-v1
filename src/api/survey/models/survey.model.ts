import Asset from 'api/asset/models/asset.model';
import Company from 'api/users/models/company.model';
import {
  Column,
  DataType,
  HasMany,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  BelongsTo,
} from 'sequelize-typescript';
import { UUIDV4 } from 'sequelize';
import SurveyQuestion from './survey-question.model';

@Table
class Survey extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.TEXT })
  title: string;

  @Column({ type: DataType.TEXT })
  content: string;

  @Column({ type: DataType.INTEGER })
  numberOfQuestions: number;

  @Column({ type: DataType.BOOLEAN, defaultValue: false })
  isActive: boolean;

  @HasMany(() => SurveyQuestion, 'surveyId')
  questions: SurveyQuestion[];

  @HasMany(() => Asset, 'surveyId')
  assets: Asset[];

  @BelongsTo(() => Company, 'companyId')
  company: Company;
}

export default Survey;
