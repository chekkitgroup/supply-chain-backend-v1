import {
  Column,
  DataType,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
  BelongsTo,
} from 'sequelize-typescript';
import { UUIDV4 } from 'sequelize';
import SurveyQuestion from './survey-question.model';
import Survey from './survey.model';
import User from 'api/users/models/user.model';

@Table
class SurveyResponse extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.TEXT })
  response: string;

  @BelongsTo(() => SurveyQuestion, 'surveyQuestionId')
  question: SurveyQuestion;

  @BelongsTo(() => Survey, 'surveyId')
  survey: Survey;

  @BelongsTo(() => User, 'userId')
  user: User;
}

export default SurveyResponse;
