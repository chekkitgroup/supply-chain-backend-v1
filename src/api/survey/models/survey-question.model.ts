import { UUIDV4 } from 'sequelize';
import {
  BelongsTo,
  Column,
  DataType,
  IsUUID,
  PrimaryKey,
  Table,
  Model,
} from 'sequelize-typescript';
import Survey from './survey.model';

@Table
class SurveyQuestion extends Model {
  @IsUUID(4)
  @PrimaryKey
  @Column({ defaultValue: UUIDV4 })
  id: string;

  @Column({ type: DataType.TEXT })
  question: string;

  @Column({ type: DataType.TEXT })
  choices: string;

  @BelongsTo(() => Survey, 'surveyId')
  survey: Survey;
}

export default SurveyQuestion;
