import { Body, Get, Param, Post, Put, Query, Request } from '@nestjs/common';

import { Controller } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CreateSurveyDto } from 'api/asset/dto/index.dto';

import { Authorize } from 'api/auth/authority.decorator';
import { RequestWithUser } from 'api/auth/types';
import { PaginationFilter } from 'utils/pagination';
import { successResponse, successResponseSpec } from 'utils/response';
import { AttachSurveyDto, CreateSurveyResponseDto } from './dto/index.dto';
import { SurveyOutputDto } from './dto/response.dto';
import { SurveyService } from './survey.service';

@ApiBearerAuth()
@Controller('survey')
@ApiTags('Surveys')
export class SurveyController {
  constructor(private readonly surveyService: SurveyService) {}

  @ApiOperation({
    summary: 'Create Survey',
  })
  @ApiResponse({
    status: 201,
    description: 'Create survey successfully',
  })
  @Authorize('surveys.manage')
  @Post()
  async createSurvey(
    @Body() input: CreateSurveyDto,
    @Request() req: RequestWithUser,
  ): Promise<successResponseSpec> {
    await this.surveyService.create(input, req.user);

    return successResponse('Survey created successfully', 201);
  }

  @ApiOperation({ summary: 'Get surveys for all company' })
  @ApiResponse({
    status: 200,
    description: 'Get surveys for a company',
  })
  @Authorize('surveys.view')
  @Get()
  async getSurveys(
    @Request() req: RequestWithUser,
    @Query() filter?: PaginationFilter,
  ): Promise<successResponseSpec> {
    const [surveys, paginationMetaData] = await this.surveyService.getSurveys(
      req.user,
      +filter.page,
      +filter.perPage,
    );

    const output = surveys?.map((survey) =>
      SurveyOutputDto.FromSurveyOutput(survey),
    );

    return successResponse(
      'Surveys retrieved successfully',
      200,
      output,
      true,
      paginationMetaData,
    );
  }

  @ApiOperation({ summary: 'Get survey for asset' })
  @ApiResponse({
    status: 200,
    description: 'Get surveys for a asset',
  })
  @Authorize('surveys.view')
  @Get(':id')
  async getAssetSurvey(
    @Param('id') assetId: string,
  ): Promise<successResponseSpec> {
    const survey = await this.surveyService.getAssetSurvey(
      assetId
    );

    const output = SurveyOutputDto.FromSurveyOutput(survey);
    

    return successResponse(
      'Surveys retrieved successfully',
      200,
      output,
      true,
    );
  }

  @ApiOperation({ summary: 'Disable survey' })
  @ApiResponse({
    status: 200,
    description: 'Survey disabled successfully',
  })
  @Authorize('surveys.manage')
  @Put(':id/disable')
  async disableSurvey(
    @Request() req: RequestWithUser,
    @Param('id') surveyId: string,
  ): Promise<successResponseSpec> {
    await this.surveyService.disableSurvey(req.user, surveyId);

    return successResponse('Surveys disabled successfully', 200);
  }

  @ApiOperation({ summary: 'Enable survey' })
  @ApiResponse({
    status: 200,
    description: 'Survey enabled successfully',
  })
  @Authorize('surveys.manage')
  @Put(':id/enable')
  async enableSurvey(
    @Request() req: RequestWithUser,
    @Param('id') surveyId: string,
  ): Promise<successResponseSpec> {
    await this.surveyService.enableSurvey(req.user, surveyId);

    return successResponse('Survey enabled successfully', 200);
  }

  @ApiOperation({ summary: 'Attach survey to an asset' })
  @ApiResponse({
    status: 200,
    description: 'Survey attached to asset successfully',
  })
  @Authorize('surveys.manage')
  @Put(':id/attach-to-asset')
  async AttachSurveyToAsset(
    @Request() req: RequestWithUser,
    @Body() { assetIds }: AttachSurveyDto,
    @Param('id') surveyId: string,
  ): Promise<successResponseSpec> {
    await this.surveyService.attachSurveysToAsset(req.user, assetIds, surveyId);

    return successResponse('Surveys attached to asset successfully', 200);
  }

  @ApiOperation({ summary: 'Record survey response' })
  @ApiResponse({
    status: 200,
    description: 'Record survey response',
  })
  @Authorize()
  @Post('/record-survey-response')
  async surveyResponse(
    @Request() req: RequestWithUser,
    @Body() surveyResponseDetails: CreateSurveyResponseDto,
  ): Promise<successResponseSpec> {
    await this.surveyService.recordResponse(surveyResponseDetails, req.user);

    return successResponse('response received successfully', 200);
  }
}
