import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { SurveyService } from './survey.service';
import { SurveyController } from './survey.controller';
import SurveyQuestion from './models/survey-question.model';
import Survey from './models/survey.model';
import { AssetModule } from 'api/asset/asset.module';
import { UsersModule } from 'api/users/users.module';
import SurveyResponse from './models/survey-response.model';

@Module({
  imports: [
    SequelizeModule.forFeature([Survey, SurveyQuestion, SurveyResponse]),
    forwardRef(() => AssetModule),

    forwardRef(() => UsersModule),
  ],
  providers: [SurveyService],
  exports: [SurveyService],
  controllers: [SurveyController],
})
export class SurveyModule {}
