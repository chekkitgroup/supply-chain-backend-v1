import {
  ConflictException,
  ForbiddenException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { AssetService } from 'api/asset/asset.service';
import {
  CreateSurveyDto,
  CreateSurveyQuestionDto,
} from 'api/asset/dto/index.dto';

import { userAuthDetails } from 'api/auth/types';
import { CompanyService } from 'api/users/company/company.service';
import logger from 'utils/logger';
import { getOffSet, getPaginationMetaData } from 'utils/pagination';
import { PaginationMeta } from 'utils/types/pagination';
import { CreateSurveyResponseDto } from './dto/index.dto';
import SurveyQuestion from './models/survey-question.model';
import SurveyResponse from './models/survey-response.model';
import Survey from './models/survey.model';

@Injectable()
export class SurveyService {
  constructor(
    @InjectModel(SurveyQuestion)
    private readonly surveyQuestionRepository: typeof SurveyQuestion,

    @InjectModel(Survey) private readonly surveyRepository: typeof Survey,

    @InjectModel(SurveyResponse)
    private readonly surveyResponseRepository: typeof SurveyResponse,

    @Inject(forwardRef(() => AssetService))
    private readonly assetService: AssetService,

    @Inject(forwardRef(() => CompanyService))
    private readonly companyService: CompanyService,
  ) {}

  async createSurveyQuestion(
    data: CreateSurveyQuestionDto,
  ): Promise<SurveyQuestion> {
    const surveyData: Partial<SurveyQuestion> = {
      question: data.question,
      choices: JSON.stringify(data.choices),
    };

    const surveyQuestions = await this.surveyQuestionRepository.create(
      surveyData,
    );

    return surveyQuestions;
  }

  async create(data: CreateSurveyDto, user: userAuthDetails): Promise<Survey> {
    const surveyQuestions = data.surveyQuestions.map((surveyQuestion) => ({
      question: surveyQuestion.question,
      choices: JSON.stringify(surveyQuestion.choices),
    }));

    logger.info('About creating survey questions');

    const newSurveyQuestions = await this.surveyQuestionRepository.bulkCreate(
      surveyQuestions,
    );

    logger.info('About creating survey');

    const surveyDetails: Partial<Survey> = {
      title: data.title,
      content: data.content,
      numberOfQuestions: surveyQuestions.length,
      isActive: true,
    };

    const survey = await this.surveyRepository.create(surveyDetails);

    await Promise.all(
      newSurveyQuestions.map((newSurveyQuestion) => {
        newSurveyQuestion.set('surveyId', survey.id);

        return newSurveyQuestion.save();
      }),
    );

    logger.info('About setting company for created survey');

    survey.set('companyId', user.companyId);

    await survey.save();

    logger.info('About to update count of survey for company');

    const company = await this.companyService.findOne({ id: user.companyId });

    company.set('totalSurveys', company.totalSurveys + 1);

    await company.save();

    return survey;
  }

  async getSurveys(
    user: userAuthDetails,
    page: number,
    perPage: number,
  ): Promise<[Survey[], PaginationMeta]> {
    const offset: number = getOffSet(+page, +perPage);

    logger.info(
      `About to get surveys for a particular company with companyId: ${user.companyId}`,
    );

    const { count: totalCount, rows: surveys } =
      await this.surveyRepository.findAndCountAll({
        distinct: true,
        where: { companyId: user.companyId },
        include: {
          model: SurveyQuestion,
          attributes: ['id', 'question', 'choices'],
        },
        ...(perPage && { limit: perPage }),
        ...(offset && { limit: offset }),
      });

    logger.info(
      `Back from getting surveys for with companyId: ${
        user.companyId
      } with warehouse details ${JSON.stringify(surveys)}`,
    );

    const paginationMetaData = getPaginationMetaData(
      totalCount,
      page,
      perPage,
      surveys.length,
    );

    return [surveys, paginationMetaData];
  }

  async getAssetSurvey(id: string): Promise<Survey> {
    return this.surveyRepository.findOne({
      where: { id },

      include: {
        model: SurveyQuestion,
        attributes: ['id', 'question', 'choices'],
      },
    });
  }

  async getSurvey(id: string): Promise<Survey> {
    return this.surveyRepository.findOne({
      where: { id },

      include: {
        model: SurveyQuestion,
        attributes: ['id', 'question', 'choices'],
      },
    });
  }

  async disableSurvey(user: userAuthDetails, surveyId: string): Promise<void> {
    const survey = await this.surveyRepository.findOne({
      where: { id: surveyId, companyId: user.companyId },
    });

    if (!survey) throw new NotFoundException('Survey does not exist');

    await this.surveyRepository.update(
      {
        isActive: false,
      },
      {
        where: { id: surveyId },
      },
    );
  }

  async enableSurvey(user: userAuthDetails, surveyId: string): Promise<void> {
    const survey = await this.surveyRepository.findOne({
      where: { id: surveyId, companyId: user.companyId },
    });

    if (!survey) throw new NotFoundException('Survey does not exist');

    await this.surveyRepository.update(
      {
        isActive: true,
      },
      {
        where: { id: surveyId },
      },
    );
  }

  async attachSurveysToAsset(
    user: userAuthDetails,
    assetIds: string[],
    surveyId: string,
  ) {
    logger.info('About to check whether survey exist');

    const survey = await this.surveyRepository.findOne({
      where: {
        id: surveyId,
        companyId: user.companyId,
      },
    });

    if (!survey) throw new NotFoundException('survey does not exist');

    logger.info('About to get assets');

    const assets = await this.assetService.findAll(assetIds, user.companyId);

    logger.info('About to set surveyId for assets ');

    await Promise.all(
      assets.map((asset) => {
        asset.set('surveyId', survey.id);

        return asset.save();
      }),
    );
  }

  async findSurveyById(surveyId: string) {
    return this.surveyRepository.findOne({
      where: {
        id: surveyId,
      },
    });
  }

  async countAll(companyId: string) {
    const totalSurveys = await this.surveyRepository.count({
      where: {
        companyId,
      },
    });

    return totalSurveys;
  }

  async recordResponse(
    surveyResponseDetails: CreateSurveyResponseDto,
    user: userAuthDetails,
  ) {
    const survey = await this.findSurveyById(surveyResponseDetails.surveyId);

    if (!survey) throw new NotFoundException('survey does not exist');

    if (!survey.isActive)
      throw new ForbiddenException('This survey is disabled at this moment');

    const surveyResponses = surveyResponseDetails.responseDetails.map(
      async (details) => {
        const surveyQuestion = await this.surveyQuestionRepository.findOne({
          where: {
            id: details.surveyQuestionId,
            surveyId: survey.id,
          },
        });

        if (surveyQuestion) {
          const isAnswered = await this.surveyResponseRepository.findOne({
            where: {
              surveyQuestionId: surveyQuestion.id,
              userId: user.id,
            },
          });

          if (isAnswered)
            throw new ConflictException(
              'You have answered this question already',
            );

          const surveyResponse: Partial<SurveyResponse> = {
            response: details.response,
          };

          const createdSurveyResponse =
            await this.surveyResponseRepository.create(surveyResponse);

          await this.surveyResponseRepository.update(
            {
              surveyId: survey.id,
              surveyQuestionId: surveyQuestion.id,
              userId: user.id,
            },
            {
              where: { id: createdSurveyResponse.id },
            },
          );
        }
      },
    );

    await Promise.all(surveyResponses);
  }
}
