import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, IsNotEmpty, IsString, ValidateNested } from 'class-validator';

export class AttachSurveyDto {
  @ApiProperty()
  @IsString({ each: true })
  assetIds: string[];
}

export class SurveyResponseDto {
  @ApiProperty()
  surveyQuestionId: string;

  @ApiProperty()
  response: string;
}

export class CreateSurveyResponseDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  surveyId: string;

  @ApiProperty()
  @ValidateNested({ each: true })
  @IsArray()
  @Type(() => SurveyResponseDto)
  responseDetails: SurveyResponseDto[];
}
