import { getSchemaPath } from '@nestjs/swagger';
import SurveyQuestion from '../models/survey-question.model';
import Survey from '../models/survey.model';

export class SurveyOutputDto {
  id: string;
  title: string;
  content: string;
  numberOfQuestions: number;
  isActive: boolean;
  questions: SurveyQuestion[];
  created_at: Date;
  deleted_at: Date;
  updated_at: Date;

  static FromSurveyOutput(survey: Survey) {
    const output = new SurveyOutputDto();

    const exposedKeys = [
      'id',
      'title',
      'content',
      'numberOfQuestions',
      'isActive',
      'questions',
      'created_at',
    ];

    exposedKeys.forEach((exposedKey) => {
      output[exposedKey] = survey[exposedKey];
    });

    return output;
  }
}

export const SurveyOutputDtoSchema = getSchemaPath(SurveyOutputDto);
