import { Injectable } from '@nestjs/common';
import { configuration } from '../config';
import Redis from 'ioredis';
import * as Redlock from 'redlock';

@Injectable()
export class RedisService {
  private client: Redis.Redis;
  private redlock: Redlock;

  constructor() {
    const host = configuration().redis.host;
    const port = configuration().redis.port;

    this.client = new Redis(port, host);

    this.redlock = new Redlock([this.client], {
      retryCount: 0,
    });
  }

  async acquireResource(name: string, ttl = 5000): Promise<Redlock.Lock> {
    return await this.redlock.acquire([name], ttl);
  }
}
