import { CacheModule, Module } from '@nestjs/common';

import { ConfigService } from '@nestjs/config';
import { NotificationModule } from 'api/notification/notification.module';

import * as redisStore from 'cache-manager-redis-store';

import { RedisService } from './redis';
import { AwsUploadService } from './uploader';
import { UtilsService } from './utils.service';

@Module({
  imports: [
    CacheModule.registerAsync({
      useFactory: async (configService: ConfigService) => ({
        store: redisStore,
        host: configService.get('REDIS_HOST'),
        port: +configService.get('REDIS_PORT'),
      }),

      inject: [ConfigService],
    }),
    NotificationModule,
  ],
  providers: [UtilsService, AwsUploadService, RedisService],
  exports: [UtilsService, AwsUploadService, RedisService],
})
export class UtilsModule {}
