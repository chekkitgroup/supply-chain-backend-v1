import {
  CACHE_MANAGER,
  Inject,
  Injectable,
  ValidationError,
} from '@nestjs/common';
import { configuration } from '../config/index';

import { Cache } from 'cache-manager';

import * as moment from 'moment';
import { IProviderAuthPayload } from 'api/asset/apps/types';
import { NotificationService } from 'api/notification/notification.service';
import { CreateNotificationDTO } from 'api/notification/dto';

@Injectable()
export class UtilsService {
  constructor(
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    @Inject(NotificationService)
    private notificationService: NotificationService,
  ) {}

  transformToSentenceCase(word: string): string {
    return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
  }

  myUniqueID(range: number): number {
    let text = '';

    const possible = '1234567890';

    for (let i = 0; i < 4; i++) {
      const sup = Math.floor(Math.random() * possible.length);

      text += i > 0 && sup == i ? '0' : possible.charAt(sup);
    }

    const m = moment();

    const ms =
      m.milliseconds() +
      1000 * (m.seconds() + 60 * (m.minutes() + 60 * m.hours()));

    return Math.floor(
      Math.pow(10, range - 1) +
        Number(text) +
        Math.random() * 9 * Math.pow(10, range - 1) +
        ms,
    );
  }

  convertToPercentage(firstNumber: number, secondNumber: number): number {
    return Math.floor((firstNumber / secondNumber) * 100);
  }

  transformErrors(errors: ValidationError[] | ValidationError[][]) {
    const clientError = errors.map((error: any) => {
      if (!error.children.length) {
        const [prop] = Object.getOwnPropertyNames(error.constraints);

        return {
          [error.property]: error.constraints[prop],
        };
      } else {
        const mappedErrors = error.children.map((err: any) => {
          const errs = err.children.reduce(
            (accumulatedErrors: any, validator: any) => {
              if (validator.constraints) {
                const [prop] = Object.getOwnPropertyNames(
                  validator.constraints,
                );

                return {
                  ...accumulatedErrors,
                  [validator.property]: validator.constraints[prop],
                };
              }
            },
            {},
          );

          return errs;
        });

        return mappedErrors;
      }
    });

    return clientError;
  }

  generateTrackerId() {
    return `${configuration().tagAndTrac.trackerPrefix}_${this.myUniqueID(14)}`;
  }

  async getChekkitDetailFromCache() {
    return (await this.cacheManager.get(
      'providerAuthPayload',
    )) as IProviderAuthPayload;
  }

  async setChekkitDetailFromCache(key: string, data: any) {
    await this.cacheManager.set(key, data, { ttl: 7200 });
  }

  async createNotification(data: CreateNotificationDTO) {
    await this.notificationService.create(data);
  }
}
