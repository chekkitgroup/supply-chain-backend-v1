import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AwsUploadService {
  constructor(private configService: ConfigService) {}
  public async profilePhotoUpload(file: any): Promise<any> {
    const s3 = this.getS3();
    const fileName = Date.now();
    const originalName = file.originalname;
    const extension = originalName.slice(originalName.lastIndexOf('.'));
    const newFileName = `ProfileImage/${fileName}${extension}`;

    const urlKey = newFileName;
    const params = {
      Body: file.buffer,
      Bucket: this.configService.get<string>('S3_BUCKET'),
      Key: urlKey,
      ACL: 'public-read',
    };

    return new Promise((resolve, reject) => {
      s3.upload(params, (err: any, data: any) => {
        if (err) {
          reject(err.message);
        }
        resolve(data);
      });
    });
  }

  public async mediaUpload(file: any): Promise<any> {
    const s3 = this.getS3();
    const fileName = Date.now();
    const originalName = file.originalname;
    const extension = originalName.slice(originalName.lastIndexOf('.'));
    const newFileName = `audio/${fileName}${extension}`;

    const urlKey = newFileName;
    const params = {
      Body: file.buffer,
      Bucket: this.configService.get<string>('S3_BUCKET'),
      Key: urlKey,
      ACL: 'public-read',
    };

    return new Promise((resolve, reject) => {
      s3.upload(params, (err: any, data: any) => {
        if (err) {
          reject(err.message);
        }
        resolve(data);
      });
    });
  }

  public async base64Upload(file: any, name: string): Promise<any> {
    const s3 = this.getS3();

    let imageExtension = file.split(';')[0].split('/');
    imageExtension = imageExtension[imageExtension.length - 1];
    const buf = Buffer.alloc(
      1000000,
      file.replace(/^data:image\/\w+;base64,/, ''),
      'base64',
    );

    const params = {
      Bucket: this.configService.get<string>('S3_BUCKET'),
      Key: `${name}-${Date.now()}.${imageExtension}`,
      Body: buf,
      ContentType: `image/${imageExtension}`,
      ACL: 'public-read',
    };
    return new Promise((resolve, reject) => {
      s3.upload(params, (err: any, data: any) => {
        if (err) {
          reject(err.message);
        }
        resolve(data);
      });
    });
  }

  private getS3() {
    return new AWS.S3({
      accessKeyId: this.configService.get<string>('AWS_ACCESS_KEY_ID'),
      secretAccessKey: this.configService.get<string>('AWS_ACCESS_KEY'),
      region: this.configService.get<string>('S3_REGION'),
    });
  }
}
