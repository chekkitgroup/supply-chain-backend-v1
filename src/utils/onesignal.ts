import {
  OneSignalAppClient,
  NotificationByDeviceBuilder,
  NotificationBySegmentBuilder,
} from 'onesignal-api-client-core';
import { Injectable } from '@nestjs/common';

@Injectable()
export class OnesignalService {
  //   constructor() {}

  async sendNotification(device, asset, company, site, warehouse) {
    const client = new OneSignalAppClient(
      process.env.ONESIGNAL_APP_ID,
      process.env.ONESIGNAL_REST_API_KEY,
    );
    console.log('company>> ', asset);
    // const input = new NotificationBySegmentBuilder()
    //   .setIncludeExternalUserIds([company])

    //   .notification() // .email()
    //   .setIncludedSegments(['Active Users', 'Inactive Users'])
    //   .setHeadings({ en: 'Critical security alert' })
    //   .setSubtitle({ en: ' We are unable to track or read the activity on the asset, please take action.' })
    //   .setContents({ en: `Asset Name: ${asset.name}` })
    //   .build();
    const input = new NotificationBySegmentBuilder()
      .setIncludedSegments(['Active Users', 'Inactive Users'])
      .notification() // .email()
      .setHeadings({ en: 'Critical security alert' })
      .setSubtitle({
        en: ' We are unable to track or read the activity on the asset, please take action.',
      })
      .setContents({ en: `Asset Name: ${asset.name}` })
      .build();
    try {
      const result = await client.createNotification(input);
      console.log('<onesignal success>', result);
    } catch (error) {
      console.log('<onesignal error>', error);
    }
  }
  //   async viewNotifications() {
  // //    return await this.oneSignalService.viewNotifications();
  //   }

  //   async createNotification(message: string) {
  //   const input = new NotificationBySegmentBuilder()
  //        .setIncludedSegments(['Active Users', 'Inactive Users'])
  //        .notification() // .email()
  //        .setContents({ en: message })
  //        .build();

  //     await this.oneSignalService.createNotification(input);
  //   }
}
