// import { configuration } from '../config/index';
import * as nodemailer from 'nodemailer';

import logger from 'utils/logger';

const { google } = require('googleapis');

const OAuth2 = google.auth.OAuth2;

const sendEmail = async (
  receiver: string,
  subject: string,
  content: string,
): Promise<boolean> => {
  // if (configuration().IsTest()) {
  //   logger.info(`sending ${subject} email for ${receiver}`);
  //   return true;
  // }

  const myOAuth2Client = new OAuth2(
    process.env.OAUTH_CLIENT_ID,
    process.env.OAUTH_CLIENT_SECRET,
    'https://developers.google.com/oauthplayground',
  );

  myOAuth2Client.setCredentials({
    refresh_token: process.env.OAUTH_REFRESH_TOKEN,
  });

  // console.log('process.env', process.env)

  const myAccessToken = myOAuth2Client.getAccessToken();

  const transporter = nodemailer.createTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
      type: 'OAuth2',
      user: 'noreply@chekkitapp.com',
      clientId: process.env.OAUTH_CLIENT_ID,
      clientSecret: process.env.OAUTH_CLIENT_SECRET,
      refreshToken: process.env.OAUTH_REFRESH_TOKEN,
      accessToken: myAccessToken,
    },
  });

  const mailOptions = {
    from: '"Chekkit Supply Chain Management " <noreply@supplychaain.com>',
    to: receiver,
    subject: subject,
    html: content,
  };

  try {
    const report = await transporter.sendMail(mailOptions);

    if (report.accepted) return true;
  } catch (error) {
    logger.error(`email-error: ${error.message}`);

    return false;
  }
  return true;
};

export default sendEmail;
