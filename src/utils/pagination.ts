import { ApiProperty } from '@nestjs/swagger';
import { IsNumberString, IsOptional } from 'class-validator';
import { PaginationMeta } from './types/pagination';

export class PaginationFilter {
  @ApiProperty({ example: 1, required: false })
  @IsNumberString()
  @IsOptional()
  page?: string;

  @ApiProperty({ example: 15, required: false })
  @IsNumberString()
  @IsOptional()
  perPage?: string;
}

export const getOffSet = (page: number, perPage: number): number =>
  perPage * page - perPage;

export const getPaginationMetaData = (
  totalCount: number,
  page: number,
  perPage: number,
  numberOfItems: number,
): PaginationMeta => {
  const numberOfPages = Math.ceil(totalCount / perPage);

  return {
    totalCount,
    currentPage: page,
    numberOfPages,
    numberOfItems,
    firstPage: page === 1,
    lastPage: page === numberOfPages,
  };
};
