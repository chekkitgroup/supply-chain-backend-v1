import { getSchemaPath } from '@nestjs/swagger';
import { successResponseSpec } from 'utils/response';

import { PaginationMeta } from './pagination';

export const ResponseSchemaHelper = (
  schemaPath: string,
  type: 'object' | 'array' = 'object',
) => ({
  allOf: [
    { $ref: getSchemaPath(successResponseSpec) },
    {
      properties: {
        paginationMeta: {
          $ref: getSchemaPath(PaginationMeta),
        },
        data: {
          ...(type === 'object'
            ? { allOf: [{ $ref: schemaPath }] }
            : { type, items: { $ref: schemaPath } }),
        },
      },
    },
  ],
});
