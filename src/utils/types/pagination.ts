import { ApiProperty } from '@nestjs/swagger';

export class PaginationMeta {
  @ApiProperty()
  totalCount: number;

  @ApiProperty()
  currentPage: number;

  @ApiProperty()
  numberOfPages: number;

  @ApiProperty()
  numberOfItems: number;

  @ApiProperty()
  firstPage: boolean;

  @ApiProperty()
  lastPage: boolean;
}
