import { TestingModule } from '@nestjs/testing';
import { SeedersService } from 'database/seeders/seeders.service';

class TestUtils {
  static async syncRoles(module: TestingModule): Promise<void> {
    const seeders = module.get(SeedersService);

    await seeders.insertRoles();
  }

  static async syncPermissions(module: TestingModule): Promise<void> {
    const seeders = module.get(SeedersService);

    await seeders.insertPermissions();
  }

  static async syncModules(module: TestingModule): Promise<void> {
    const seeders = module.get(SeedersService);

    await seeders.insertAppModules();
  }

  static async syncProducts(module: TestingModule): Promise<void> {
    const seeders = module.get(SeedersService);

    await seeders.insertProducts();
  }

  static async syncFeatures(module: TestingModule): Promise<void> {
    const seeders = module.get(SeedersService);

    await seeders.insertFeatureCategories();

    await seeders.insertFeatures();
  }

  static async syncPlans(module: TestingModule): Promise<void> {
    const seeders = module.get(SeedersService);

    await seeders.insertPlans();
  }

  static async syncHardwares(module: TestingModule): Promise<void> {
    const seeders = module.get(SeedersService);

    await seeders.insertHardwares();
  }
}

export default TestUtils;
