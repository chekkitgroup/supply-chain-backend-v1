import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import IndexFixture from './fixtures';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let fixture: IndexFixture;

  // beforeAll(async () => {
  //   const moduleFixture: TestingModule = await Test.createTestingModule({
  //     imports: [AppModule],
  //   }).compile();

  //   app = moduleFixture.createNestApplication();

  //   fixture = new IndexFixture(moduleFixture);

  //   await fixture.destroyAll();

  //   await app.init();
  // });

  it('Should return welcome to Chekkit Supply Chain System API', async () => {
    await request(app.getHttpServer()).get('').expect(200).expect({
      status: 'Success',
      statusCode: 200,
      message: 'Welcome to Chekkit Supply Chain System API',
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
