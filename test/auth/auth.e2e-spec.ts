import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../../src/app.module';
import IndexFixture from '../fixtures';
import TestUtils from '../test.utils';
import { CreateUserDto, LoginDto } from 'api/users/dtos/user.dto';
import { Sequelize } from 'sequelize-typescript';

describe('AuthController (e2e)', () => {
  let app: INestApplication;
  let fixture: IndexFixture;
  let sequelize: Sequelize;

  // beforeAll(async () => {
  //   const moduleFixture: TestingModule = await Test.createTestingModule({
  //     imports: [AppModule],
  //   }).compile();

  //   app = moduleFixture.createNestApplication();

  //   sequelize = moduleFixture.get(Sequelize);

  //   fixture = new IndexFixture(moduleFixture);

  //   await fixture.destroyAll();

  //   await TestUtils.syncProducts(moduleFixture);

  //   await TestUtils.syncFeatures(moduleFixture);
  //   await TestUtils.syncPlans(moduleFixture);

  //   await TestUtils.syncPermissions(moduleFixture);
  //   await TestUtils.syncRoles(moduleFixture);

  //   await TestUtils.syncModules(moduleFixture);

  //   await TestUtils.syncHardwares(moduleFixture);

  //   await fixture.loadAll();

  //   await app.init();
  // });

  describe('/auth/signup', () => {
    it('Should return welcome to Chekkit Supply Chain System API', async () => {
      const input: CreateUserDto = {
        name: 'Blessing Makaraba',
        email: 'system@gmail.com',
        password: 'Ilovetocode',
        address: '10 Oladi',
        country: 'Nigeria',
        companyRole: 'Software Engineer',
        companyName: 'Chekkit Technologies',
        moduleIds: [''],
      };

      await request(app.getHttpServer())
        .post('/auth/signup')
        .send(input)
        .expect(201);

      expect({
        status: 'Success',
        statusCode: 200,
        message:
          'Account Created. Please check your email and to verify your account',
      });
    });
  });

  describe('/auth/signin', () => {
    it('Should return success if user is verified', async () => {
      const input: LoginDto = {
        email: 'test@gmail.com',
        password: 'welovetocode',
      };
      const res = await request(app.getHttpServer())
        .post('/auth/signin')
        .send(input)
        .expect(201);

      expect({
        status: 'Success',
        statusCode: 200,
        message: 'Login Successful',
      });

      expect(res.body).toHaveProperty('data');
    });
  });

  afterAll(async () => {
    await sequelize.close();

    await app.close();
  });
});
