import { Sequelize } from 'sequelize-typescript';

export interface Fixture {
  load(sequelize: Sequelize): Promise<void>;
  destroy(sequelize: Sequelize): Promise<void>;
}
