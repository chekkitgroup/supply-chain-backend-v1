import AppModule from 'api/appmodule/models/appmodule.model';
import Company from 'api/users/models/company.model';
import User from 'api/users/models/user.model';
import { Sequelize } from 'sequelize-typescript';
import { Fixture } from './types';

class UserFixture implements Fixture {
  async load(sequelize: Sequelize): Promise<void> {
    await sequelize.transaction(async () => {
      const company = await Company.create({
        name: 'Liberty Powers',
        address: '10 Oladipupo Oduwole',
        country: 'Nigeria',
        identifier: 'distributor',
      });

      const appModule = await AppModule.create({
        name: 'asset-management',
      });

      await User.create({
        name: 'Abraham Lincoln',
        password: 'welovetocode',
        email: 'test@gmail.com',
        isVerified: true,
        address: '10 Oladi',
        country: 'Nigeria',
        companyRole: 'Software Engineer',
        companyId: company.id,
      });
    });
  }

  async destroy(sequelize: Sequelize): Promise<void> {
    await sequelize.truncate({ cascade: true, force: true, truncate: true });
  }
}

export default UserFixture;
