import { TestingModule } from '@nestjs/testing';
import { Sequelize } from 'sequelize-typescript';
import RoleFixture from './role';
import { Fixture } from './types';
import UserFixture from './users';

class IndexFixture {
  private fixtures: Fixture[] = [];
  private sequelize: Sequelize;

  constructor(private module: TestingModule) {
    this.fixtures.push(new RoleFixture());
    this.fixtures.push(new UserFixture());

    this.sequelize = this.module.get(Sequelize);
  }

  async loadAll() {
    for (const fixture of this.fixtures) {
      await fixture.load(this.sequelize);
    }
  }

  async destroyAll() {
    await this.sequelize.query('SET FOREIGN_KEY_CHECKS = 0', { raw: true });

    for (const fixture of this.fixtures) {
      await fixture.destroy(this.sequelize);
    }
  }
}

export default IndexFixture;
