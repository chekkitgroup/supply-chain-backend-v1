import { Sequelize } from 'sequelize-typescript';
import { Fixture } from './types';

class RoleFixture implements Fixture {
  async load(_sequelize: Sequelize): Promise<void> {
    return;
  }

  async destroy(sequelize: Sequelize): Promise<void> {
    await sequelize.truncate({ cascade: true, force: true, truncate: true });
  }
}

export default RoleFixture;
