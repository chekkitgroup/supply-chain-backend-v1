FROM node

WORKDIR /supply_chain_api

COPY package.json .
COPY yarn.lock .

RUN yarn install



COPY . .


RUN yarn run build

EXPOSE 3000

CMD [ "node", "dist/src/main.js" ]
